package org.nrg.ccf.ndatransfer.components.conf;

import lombok.extern.slf4j.Slf4j;
import org.nrg.framework.annotations.XnatPlugin;
import org.springframework.context.annotation.ComponentScan;

@XnatPlugin(
			value = "ccfNdaTransferComponentsPlugin",
			name = "CCF NDA Transfer Components Plugin"
		)
@ComponentScan({ 
		"org.nrg.ccf.ndatransfer.components.conf",
		"org.nrg.ccf.ndatransfer.components.utils",
		"org.nrg.ccf.ndatransfer.components.xapi"
	})
@Slf4j
public class CcfNdaTransferComponentsPlugin {
	
	public CcfNdaTransferComponentsPlugin() {
		log.info("Configuring the Intradb NDA Transfer Components Plugin.");
	}
	
}
