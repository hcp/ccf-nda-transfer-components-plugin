package org.nrg.ccf.ndatransfer.components.abst;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.nrg.action.ClientException;
import org.nrg.action.ServerException;
import org.nrg.ccf.common.utilities.constants.CommonConstants;
import org.nrg.ccf.common.utilities.utils.ResourceUtils;
import org.nrg.ccf.ndatransfer.abst.RegexMatcher;
import org.nrg.ccf.ndatransfer.abst.ValueInfo;
import org.nrg.ccf.ndatransfer.constants.MatchOperator;
import org.nrg.ccf.ndatransfer.constants.ResourceType;
import org.nrg.ccf.ndatransfer.interfce.FileMatcherI;
import org.nrg.ccf.ndatransfer.interfce.FileSelectorI;
import org.nrg.xdat.XDAT;
import org.nrg.xdat.model.XnatAbstractresourceI;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.model.XnatImagesessiondataI;
import org.nrg.xdat.om.XnatResourcecatalog;
import org.nrg.xft.security.UserI;
import org.nrg.xnat.services.archive.CatalogService;
import org.nrg.xnat.services.archive.CatalogService.Operation;

import com.google.common.collect.ImmutableList;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import lombok.AccessLevel;

@Slf4j
@Getter
@Setter
public abstract class AbstractRegexResourceFileSelector extends RegexMatcher implements FileSelectorI {
	
	@Setter(AccessLevel.NONE)
	protected final List<FileMatcherI> fileMatchers = new ArrayList<>(); 
	protected ResourceType resourceType; 
	@Setter(AccessLevel.NONE)
	private String canonicalClassname =this.getClass().getCanonicalName();
	private final CatalogService _catalogService = XDAT.getContextService().getBean(CatalogService.class);
	
	public AbstractRegexResourceFileSelector() {
		super();
	}
	
	public AbstractRegexResourceFileSelector(ResourceType resourceType, String matchRegex, String excludeRegex, MatchOperator matchOperator, Integer matchCount) {
		super(matchRegex, excludeRegex, matchOperator, matchCount);
		this.resourceType = resourceType;
	}

	@Override
	public List<String> configurationYaml() {
		
		final StringBuilder sb = new StringBuilder();
		sb.append("ResourceType:\n")
		.append("    id: resourceType\n")
		.append("    name: resourceType\n")
		.append("    kind: panel.select.single\n")
		.append("    label: Resource Type:\n")
		.append("    validation: required\n");
		if (resourceType != null) {
			sb.append("    value: ")
			.append(resourceType)
			.append("\n")
			.append("    element:\n")
			.append("        disabled: true:\n")
			;
		}
		sb.append("    options:\n");
		for (final ResourceType value : ResourceType.values()) {
			final ValueInfo valueInfo = value.getValueInfo();
			sb.append("        \"");
			sb.append(valueInfo.getValue());
			sb.append("\": \"");
			sb.append(valueInfo.getDisplayName());
			sb.append("\"\n");
		}
		sb.append(regexConfigurationYaml())
		.append("FileMatchers:\n")
		.append("    id: fileMatchers\n")
		.append("    name: fileMatchers\n")
		.append("    kind: panel.display\n")
		.append("    label: FileMatchers:\n");
		
		return ImmutableList.of(sb.toString());
		
	}

	@Override
	public boolean readyForTransfer(XnatImagesessiondataI exp, UserI user) {
		switch (resourceType) {
			case SESSION:
				final List<XnatAbstractresourceI> sessionResources = exp.getResources_resource();
				return evaluateSessionFileSelector(sessionResources, user);
			case SCAN:
				final List<XnatImagescandataI> sessionScans = getRelevantScans(exp); 
				return evaluateScanFileSelector(sessionScans, user);
		}
		return false;
	}
	
	// Subclasses may want to override this method
	protected List<XnatImagescandataI> getRelevantScans(XnatImagesessiondataI exp) {
		return exp.getScans_scan();
	}

	// Method must be public for JSON deserialization
	public void addFileMatcher(FileMatcherI fileMatcher) {
		if (!this.fileMatchers.contains(fileMatcher)) {
			this.fileMatchers.add(fileMatcher);
		}
	}

	protected List<XnatAbstractresourceI> findMatchingResources(List<XnatAbstractresourceI> sessionResources) {
		final List<XnatAbstractresourceI> returnList = new ArrayList<>();
		for (final XnatAbstractresourceI resource : sessionResources) {
			if (!(resource instanceof XnatResourcecatalog)) {
				continue;
			}
			final String resLabel = resource.getLabel();
			if (evaluateString(resLabel)) {
				returnList.add(resource);
			}
		}
		return returnList;
	}

	protected boolean evaluateSessionFileSelector(List<XnatAbstractresourceI> sessionResources, UserI user) {
		final List<XnatAbstractresourceI> matchingResources = findMatchingResources(sessionResources);
		final int matches = matchingResources.size();
		final MatchOperator operator = getMatchOperator();
		boolean isOkay = false;
		switch (operator) {
			case EQUALS:
				if (matches == getMatchCount()) {
					isOkay = true;
				}
				break;
			case GREATER_THAN:
				if (matches > getMatchCount()) {
					isOkay = true;
				}
				break;
			case GREATER_THAN_EQUALS:
				if (matches >= getMatchCount()) {
					isOkay = true;
				}
				break;
			case LESS_THAN_EQUALS:
				if (matches <= getMatchCount()) {
					isOkay = true;
				}
				break;
			case LESS_THAN:
				if (matches <= getMatchCount()) {
					isOkay = true;
				}
				break;
		}
		if (!isOkay) {
			return false;
		}
		for (final FileMatcherI fileMatcher : getFileMatchers()) {
			if (!evaluateFileMatches(toResourceCatalogList(matchingResources),fileMatcher, user)) {
				return false;
			}
		}
		return true;
	}
	
	protected List<XnatResourcecatalog> toResourceCatalogList(List<XnatAbstractresourceI> matchingResources) {
		final List<XnatResourcecatalog> returnList = new ArrayList<>();
		for (final XnatAbstractresourceI resource : matchingResources) {
			if (resource instanceof XnatResourcecatalog) {
				returnList.add((XnatResourcecatalog)resource);
			}
		}
		return returnList;
	}

	@SuppressWarnings("unchecked")
	protected Map<String,List<File>> findFileMatches(XnatResourcecatalog resource, FileMatcherI fileMatcher, UserI user) {
		List<File> files = resource.getCorrespondingFiles(CommonConstants.ROOT_PATH);
		File catalogFile = resource.getCatalogFile(CommonConstants.ROOT_PATH);
		if (files.size()<1) {
			final String resourceSB = ResourceUtils.getResourceSb(resource);
			log.info("Refreshing catalog:  " + resourceSB);
			if (resourceSB != null) {
				refreshCatalog(user, resourceSB);
			}
			// Re-pull the resource so the getCorrespondingFiles list gets updated.  Otherwise it'll still be empty.
			final Integer resourceID = resource.getXnatAbstractresourceId();
			resource = XnatResourcecatalog.getXnatResourcecatalogsByXnatAbstractresourceId(resourceID, user, false); 
			files = resource.getCorrespondingFiles(CommonConstants.ROOT_PATH);
			catalogFile = resource.getCatalogFile(CommonConstants.ROOT_PATH);
		}
		final File resourceRoot = catalogFile.getParentFile();
		final Map<String,List<File>> returnMap = new LinkedHashMap<>();
		for (final File file : files) {
			final String relativePath = file.getPath().replace(resourceRoot.getPath() + File.separator, "");
			if (!returnMap.containsKey(relativePath)) {
				returnMap.put(relativePath, new ArrayList<File>());
			}
			if (fileMatcher.evaluateFile(resourceRoot.getPath(), file)) {
				returnMap.get(relativePath).add(file);
			}
		}
		return returnMap;
	}

	private synchronized void refreshCatalog(UserI user, String resourceSB) {
		try {
			_catalogService.refreshResourceCatalog(user, resourceSB, Operation.ALL);
		} catch (ServerException | ClientException e) {
			log.error("ERROR refreshing catalog:  " + resourceSB);
		}
	}

	protected boolean evaluateFileMatches(List<XnatResourcecatalog> catList, FileMatcherI fileMatcher, UserI user) {
		final Collection<File> files = new HashSet<>(); 
		for (final XnatResourcecatalog catalog : catList) {
				Collection<List<File>> matches = findFileMatches(catalog, fileMatcher, user).values();
				for (final List<File> fl : matches) {
					for (final File f : fl) {
						files.add(f);
					}
				}
		}
		return fileMatcher.validateFileMatches(files);
	}

	protected List<XnatAbstractresourceI> findMatchingScanResources(List<XnatImagescandataI> relevantScans) {
		final List<XnatAbstractresourceI> returnList = new ArrayList<>();
		for (final XnatImagescandataI scan : relevantScans) {
			final List<XnatAbstractresourceI> resources = scan.getFile();
			returnList.addAll(findMatchingResources(resources));
		}
		return returnList;
	}

	protected boolean evaluateScanFileSelector(List<XnatImagescandataI> relevantScans, UserI user) {
		if (!resourceType.equals(ResourceType.SCAN)) {
			return false;
		}
		final List<XnatAbstractresourceI> matchedResources = findMatchingScanResources(relevantScans);
		final int matches = matchedResources.size();
		boolean isOkay = false;
		switch (getMatchOperator()) {
			case EQUALS:
				if (matches == getMatchCount()) {
					isOkay = true;
				}
				break;
			case GREATER_THAN:
				if (matches > getMatchCount()) {
					isOkay = true;
				}
				break;
			case GREATER_THAN_EQUALS:
				if (matches >= getMatchCount()) {
					isOkay = true;
				}
				break;
		case LESS_THAN:
				if (matches < getMatchCount()) {
					isOkay = true;
				}
				break;
		case LESS_THAN_EQUALS:
				if (matches <= getMatchCount()) {
					isOkay = true;
				}
				break;
		default:
			break;
		}
		if (!isOkay) {
			return false;
		}
		for (final FileMatcherI fileMatcher : getFileMatchers()) {
			if (!evaluateFileMatches(toResourceCatalogList(matchedResources),fileMatcher,user)) {
				return false;
			}
		}
		return true;
	}

	@Override
	public String toString() {
		return canonicalClassname + " [ resourceType=" + resourceType
				+ ", matchRegex=" + ((matchRegex.length()<40) ? matchRegex : matchRegex.substring(0,40) + "....") 
				+ ", fileMatchers=" + fileMatchers +" ]";
	}
	
}
