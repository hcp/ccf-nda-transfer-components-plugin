package org.nrg.ccf.ndatransfer.components.matchers;

import java.io.File;
import java.util.Collection;
import java.util.List;

import org.nrg.ccf.ndatransfer.abst.RegexMatcher;
import org.nrg.ccf.ndatransfer.anno.FileMatcher;
import org.nrg.ccf.ndatransfer.constants.MatchOperator;
import org.nrg.ccf.ndatransfer.interfce.FileMatcherI;

import com.google.common.collect.ImmutableList;

@FileMatcher
public class RegexFileMatcher extends RegexMatcher implements FileMatcherI {

	public RegexFileMatcher() {
		super();
	}

	public RegexFileMatcher(String matchRegex, String excludeRegex, MatchOperator matchOperator, Integer matchCount) {
		super(matchRegex, excludeRegex, matchOperator, matchCount);
	}

	@Override
	public List<String> configurationYaml() {
		return ImmutableList.of(regexConfigurationYaml());
	}

	@Override
	public boolean evaluateFile(final String resourceRootPath, File file) {
		final String relativePath = file.getPath().replace(resourceRootPath + File.separator, "");
		return evaluateString(relativePath);
	}

	@Override
	public boolean validateFileMatches(Collection<File> files) {
		final int matches = files.size();
		switch (matchOperator) {
			case EQUALS:
				if (matches == matchCount) {
					return true;
				}
				break;
			case GREATER_THAN:
				if (matches > matchCount) {
					return true;
				}
				break;
			case GREATER_THAN_EQUALS:
				if (matches >= matchCount) {
					return true;
				}
				break;
			case LESS_THAN:
				if (matches < matchCount) {
					return true;
				}
				break;
			case LESS_THAN_EQUALS:
				if (matches <= matchCount) {
					return true;
				}
				break;
			default:
				break;
			}
		return false;
	}

}
