package org.nrg.ccf.ndatransfer.components.simple;

import java.util.Date;

import org.nrg.ccf.ndaguid.constants.NdaGuidConstants;
import org.nrg.ccf.ndatransfer.abst.DataPackageDefinition;
import org.nrg.ccf.ndatransfer.abst.ManifestPackageDefinition;
import org.nrg.ccf.ndatransfer.abst.NdaPackagePrereqCheckerI;
import org.nrg.ccf.ndatransfer.anno.NdaPackagePrereqChecker;
import org.nrg.ccf.ndatransfer.interfce.FileSelectorI;
import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.xdat.om.XnatImagesessiondata;
import org.nrg.xdat.om.XnatMegsessiondata;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xdat.om.XnatPetsessiondata;
import org.nrg.xdat.om.XnatSubjectdata;
import org.nrg.xft.security.UserI;

//import lombok.extern.slf4j.Slf4j;

//@Slf4j
@NdaPackagePrereqChecker
public class SimplePackagePrereqChecker implements NdaPackagePrereqCheckerI {
	
	@Override
	public void checkPrereqs(PcpStatusEntity statusEntity, UserI user, DataPackageDefinition packageInfo) {
		final String entityType = statusEntity.getEntityType();
		XnatImagesessiondata imageSession = null;
		boolean sessionOk = true;
		final StringBuilder infoSB = new StringBuilder();
		final ManifestPackageDefinition mpInfo;
		if (!(packageInfo instanceof ManifestPackageDefinition)) {
			sessionOk = false;
			infoSB.append("<li>This prereq checker is only applicable to ManifestPackageInfo types.</li>");
			updateEntity(statusEntity,infoSB,false);
			return;
		}
		mpInfo = (ManifestPackageDefinition)packageInfo;
		if (entityType.equals(XnatMrsessiondata.SCHEMA_ELEMENT_NAME) ||
			entityType.equals(XnatMegsessiondata.SCHEMA_ELEMENT_NAME) ||
			entityType.equals(XnatPetsessiondata.SCHEMA_ELEMENT_NAME)) {
			imageSession = XnatImagesessiondata.getXnatImagesessiondatasById(statusEntity.getEntityId(), user, false); 
			final Object session_age_obj = imageSession.getFieldByName(NdaGuidConstants.INTERVIEW_AGE_FIELD);
			final String session_age = (session_age_obj != null) ? session_age_obj.toString() : "";
			if (session_age.isEmpty() || !session_age.matches(NdaGuidConstants.INTERVIEW_AGE_REGEX)) {
				sessionOk = false;
				infoSB.append("<li>Session NDA Interview Age has not been defined</li>");
				updateEntity(statusEntity,infoSB,false);
			} 
			final Object session_date_obj = imageSession.getFieldByName(NdaGuidConstants.INTERVIEW_DATE_FIELD);
			final String session_date = (session_date_obj != null) ? session_date_obj.toString() : "";
			if (session_date.isEmpty() || !session_date.matches(NdaGuidConstants.INTERVIEW_DATE_REGEX)) {
				sessionOk = false;
				infoSB.append("<li>Session NDA Interview Date has not been defined</li>");
				updateEntity(statusEntity,infoSB,false);
			} 
		} 
		if (imageSession == null) {
			sessionOk = false;
			infoSB.append("<li>ImageSession not found (" + statusEntity.getEntityId() + ")");
			updateEntity(statusEntity,infoSB,false);
			return;
		}
		final XnatSubjectdata subject = imageSession.getSubjectData();
		final Object subject_guid_obj = subject.getFieldByName(NdaGuidConstants.GUID_FIELD);
		final String subject_guid = (subject_guid_obj != null) ? subject_guid_obj.toString() : "";
		if (subject_guid.isEmpty() || !subject_guid.matches(NdaGuidConstants.GUID_REGEX)) {
			sessionOk = false;
			infoSB.append("<li>Subject NDA GUID has not been defined</li>");
			updateEntity(statusEntity,infoSB,false);
		} 
		final Object subject_gender_obj = subject.getFieldByName(NdaGuidConstants.GENDER_FIELD);
		final String subject_gender = (subject_gender_obj != null) ? subject_gender_obj.toString() : "";
		if (subject_gender.isEmpty() || !subject_gender.matches(NdaGuidConstants.GENDER_REGEX)) {
			sessionOk = false;
			infoSB.append("<li>Subject NDA Gender has not been defined</li>");
			updateEntity(statusEntity,infoSB,false);
		} 
		for (final FileSelectorI rfSelector : mpInfo.getFileSelectors()) {
			if (!rfSelector.readyForTransfer(imageSession,user)) {
				sessionOk = false;
				infoSB.append("<li>Criteria for one or more resourceFileSelectors not met:  <ol><li>Selector=" + 
						rfSelector.toString() + "</li></ol></li>");
				updateEntity(statusEntity,infoSB,false);
				
			}
		}
		updateEntity(statusEntity,infoSB,sessionOk);
	}

	private void updateEntity(PcpStatusEntity statusEntity, StringBuilder infoSB, boolean sessionOk) {
		if (sessionOk) {
			if (!statusEntity.getPrereqs()) {
				statusEntity.setPrereqs(true);
				statusEntity.setPrereqsTime(new Date());
				statusEntity.setPrereqsInfo("");
			} 
		} else {
			if (statusEntity.getPrereqs() || !statusEntity.getPrereqsInfo().equals(infoSB.toString())) {
				statusEntity.setPrereqs(false);
				statusEntity.setPrereqsTime(new Date());
				statusEntity.setPrereqsInfo(infoSB.toString());
			} 
		}
	}

}
