package org.nrg.ccf.ndatransfer.components.entities;

import org.nrg.ccf.ndatransfer.anno.NdaPackageDefinition;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
//import lombok.experimental.Accessors;

@Getter
@Setter
@NdaPackageDefinition(description="Standard fmriresults01 package definition (gender renamed)")
public class FmriResults01PackageDefinition_sx extends FmriResults01PackageDefinition {
	
	@Setter(AccessLevel.NONE)
	@Getter(AccessLevel.NONE)
	private final String _className = this.getClass().getSimpleName();
	
	@Override
	public String generateMetadataFileHeader() {
		return "fmriresults,01,,,,,,,,,,,,,,,,,,,,,,,," + System.lineSeparator() +
		"subjectkey,src_subject_id,origin_dataset_id,interview_date,interview_age,sex,experiment_id,inputs,img03_id," + 
		"file_source,job_name,proc_types,metric_files,pipeline,pipeline_script,pipeline_tools,pipeline_type,pipeline_version," +
		"qc_fail_quest_reason,qc_outcome,derived_files,scan_type,img03_id2,file_source2,session_det,image_history" + System.lineSeparator()		
		;
	}

}
