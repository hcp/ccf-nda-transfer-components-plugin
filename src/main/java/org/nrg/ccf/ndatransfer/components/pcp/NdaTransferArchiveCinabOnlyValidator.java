package org.nrg.ccf.ndatransfer.components.pcp;

import java.io.File;
import java.io.InvalidClassException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.TrueFileFilter;
import org.nrg.ccf.common.utilities.utils.ResourceUtils;
import org.nrg.ccf.ndatransfer.abst.DataPackageDefinition;
import org.nrg.ccf.ndatransfer.abst.NdaPackageValidatorI;
import org.nrg.ccf.ndatransfer.utils.NdaTransferConfigUtils;
import org.nrg.ccf.pcp.anno.PipelineValidator;
import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.ccf.pcp.inter.PipelineValidatorI;
import org.nrg.framework.exceptions.NotFoundException;
import org.nrg.xdat.XDAT;
import org.nrg.xdat.model.XnatAbstractresourceI;
import org.nrg.xdat.om.XnatImagesessiondata;
import org.nrg.xft.security.UserI;
import org.nrg.xnat.turbine.utils.ArcSpecManager;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@PipelineValidator
public class NdaTransferArchiveCinabOnlyValidator implements PipelineValidatorI {
	
	final NdaTransferConfigUtils _configUtils = XDAT.getContextService().getBean(NdaTransferConfigUtils.class);
	final static Map<String,NdaPackageValidatorI> _cache = new HashMap<>();

	@Override
	public void validate(PcpStatusEntity statusEntity, UserI user) {
		try {
			// 1) Is there a directory for the entity in the CinaB directory?
			final String entityProject = statusEntity.getProject();
			final String entityLabel = statusEntity.getEntityLabel();
			final String archivePath = ArcSpecManager.GetInstance().getGlobalArchivePath();
			final File archiveDir = new File(archivePath);
			final File cinabDir = new File(archivePath + File.separator + "CinaB", entityProject);
			if (!archiveDir.exists() || !archiveDir.isDirectory()) {
				statusEntity.setValidated(false);
				statusEntity.setValidatedTime(new Date());
				statusEntity.setValidatedInfo(
						"ERROR:  Archive directory does not exist!!! (DIRECTORY=" + 
						archiveDir.getPath() + ")"
				);
				return;
			}
			if (!cinabDir.exists() || !cinabDir.isDirectory()) {
				statusEntity.setValidated(false);
				statusEntity.setValidatedTime(new Date());
				statusEntity.setValidatedInfo(
						"CinaB archive directory either doesn't exist or isn't a directory (DIRECTORY=" + 
						cinabDir.getPath() + ")"
				);
				return;
			}
			final File entityDir = new File(cinabDir, entityLabel);
			if (!entityDir.exists() || !entityDir.isDirectory()) {
				statusEntity.setValidated(false);
				statusEntity.setValidatedTime(new Date());
				statusEntity.setValidatedInfo(
						"Session directory does not exist in CinaB directory (DIRECTORY=" + 
						entityDir.getPath() + ")"
				);
				return;
			}
			// 2) Is there a directory for the entity in the CinaB directory?
			if (!statusEntity.getSubGroup().toLowerCase().contains("unproc")) {
				XnatImagesessiondata session = XnatImagesessiondata.getXnatImagesessiondatasById(statusEntity.getEntityId(), user, false);
				final List<String> resourceList = new ArrayList<>();
				final Set<String> matchedSet = new HashSet<>();
				for (final XnatAbstractresourceI resource : session.getResources_resource()) {
					final String resourceLabel = resource.getLabel();
					if (!resourceLabel.toLowerCase().matches("^.*_(pre)*proc")) {
						continue;
					}
					if (!resourceList.contains(resourceLabel)) {
						resourceList.add(resourceLabel);
					}
				}
				outerLoop:
				for (File f : FileUtils.listFilesAndDirs(entityDir, TrueFileFilter.INSTANCE, TrueFileFilter.INSTANCE)) {
					final Path path = f.toPath().toRealPath();
					for (final String resource : resourceList) {
						if (path.toString().contains(File.separator + resource + File.separator)) {
							matchedSet.add(resource);
							if (matchedSet.size()==resourceList.size()) {
								break outerLoop;
							}
						}
					}
				}
				if (matchedSet.size()<resourceList.size()) {
					statusEntity.setValidated(false);
					statusEntity.setValidatedTime(new Date());
					statusEntity.setValidatedInfo(
							"Not all _preproc and _proc resources are represented in the directory for this session (SESSION=" + 
							entityLabel + ")"
					);
					return;
				}
			} else {
				// Let's just make sure we have some unprocessed data
				boolean foundUnproc = false;
				for (File f : FileUtils.listFilesAndDirs(entityDir, TrueFileFilter.INSTANCE, TrueFileFilter.INSTANCE)) {
					final Path path = f.toPath().toRealPath();
					final String pathStr = path.toString();
					if (pathStr.contains("_unproc/") || pathStr.toLowerCase().contains("/scans/")) {
						foundUnproc = true;
						break;
					}
				}
				if (!foundUnproc) {
					statusEntity.setValidated(false);
					statusEntity.setValidatedTime(new Date());
					statusEntity.setValidatedInfo(
							"No unprocessed data was found in the directory for this session (SESSION=" + 
							entityLabel + ")"
					);
					return;
				}
			}
			statusEntity.setValidated(true);
			statusEntity.setValidatedTime(new Date());
			statusEntity.setValidatedInfo(
					"CinaB output found (DIRECTORY=" + 
					cinabDir.getPath() + ")"
			);
			
		} catch (Exception e) {
			log.error(e.toString());
		}
	}
	
}
