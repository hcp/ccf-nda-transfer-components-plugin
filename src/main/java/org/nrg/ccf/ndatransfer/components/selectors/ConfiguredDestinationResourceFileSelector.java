package org.nrg.ccf.ndatransfer.components.selectors;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.nrg.ccf.ndatransfer.abst.DataPackageDefinition;
import org.nrg.ccf.ndatransfer.abst.ValueInfo;
import org.nrg.ccf.ndatransfer.anno.FileSelector;
import org.nrg.ccf.ndatransfer.components.selectors.ResourceFileSelector;
import org.nrg.ccf.ndatransfer.constants.MatchOperator;
import org.nrg.ccf.ndatransfer.constants.ResourceType;
import org.nrg.ccf.ndatransfer.interfce.FileMatcherI;
import org.nrg.xdat.model.XnatAbstractresourceI;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.model.XnatImagesessiondataI;
import org.nrg.xdat.om.XnatImagesessiondata;
import org.nrg.xdat.om.XnatResourcecatalog;
import org.nrg.xft.security.UserI;

import com.google.common.collect.ImmutableList;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Getter
@Setter
@FileSelector
public class ConfiguredDestinationResourceFileSelector extends ResourceFileSelector {
	
	protected String newLocation;
	public static final String SESSION_LABEL_SUBST = "$SessionLabel";
	public static final String SUBJECT_LABEL_SUBST = "$SubjectLabel";
	
	@Override
	public List<String> configurationYaml() {
		final StringBuilder sb = new StringBuilder();
		sb.append("ResourceType:\n")
		.append("    id: resourceType\n")
		.append("    name: resourceType\n")
		.append("    kind: panel.select.single\n")
		.append("    label: Resource Type:\n")
		.append("    validation: required\n");
		if (resourceType != null) {
			sb.append("    value: ")
			.append(resourceType)
			.append("\n")
			.append("    element:\n")
			.append("        disabled: true:\n")
			;
		}
		sb.append("    options:\n");
		for (final ResourceType value : ResourceType.values()) {
			final ValueInfo valueInfo = value.getValueInfo();
			sb.append("        \"");
			sb.append(valueInfo.getValue());
			sb.append("\": \"");
			sb.append(valueInfo.getDisplayName());
			sb.append("\"\n");
		}
		sb.append("NewLocation:\n")
		.append("    id: newLocation\n")
		.append("    name: newLocation\n")
		.append("    kind: panel.input.text\n")
		.append("    label: New Location:\n")
		.append("    description: (OPTIONAL)  Path inside the package for locating file ($SessionLabel will be replaced " +
				"the session label, $SubjectLabel will be replaced with the subject label).\n");
		sb.append(regexConfigurationYaml());
		sb.append("FileMatchers:\n")
		.append("    id: fileMatchers\n")
		.append("    name: fileMatchers\n")
		.append("    kind: panel.display\n")
		.append("    label: FileMatchers:\n");
		return ImmutableList.of(sb.toString());
	}

	public ConfiguredDestinationResourceFileSelector() {
		super();
	}

	public ConfiguredDestinationResourceFileSelector(ResourceType resourceType, String matchRegex, String excludeRegex, String newLocation, MatchOperator matchOperator, Integer matchCount) {
		super(resourceType, matchRegex, excludeRegex, matchOperator, matchCount);
		this.newLocation = newLocation;
	}
	
	@Override
	public Map<String,List<File>> generateFileMap(DataPackageDefinition packageInfo, XnatImagesessiondataI exp, UserI user) {
		final Map<String, List<File>> returnMap = new LinkedHashMap<>();
		switch (resourceType) {
			case SESSION:
				final List<XnatAbstractresourceI> sessionResources = exp.getResources_resource();
				for (final XnatAbstractresourceI resource : findMatchingResources(sessionResources)) {
					processResource(resource, exp, user, returnMap);
				}
				return returnMap;
			case SCAN:
				final List<XnatImagescandataI> sessionScans = getRelevantScans(exp); 
				for (final XnatAbstractresourceI resource : findMatchingScanResources(sessionScans)) {
					processResource(resource, exp, user, returnMap);
				}
				return returnMap;
		}
		return returnMap;
	}

	private void processResource(XnatAbstractresourceI resource, XnatImagesessiondataI exp, UserI user, Map<String, List<File>> returnMap) {
		if (!(resource instanceof XnatResourcecatalog)) {
			return;
		}
		for (final FileMatcherI fileMatcher : getFileMatchers()) {
			final Map<String, List<File>> matches = findFileMatches((XnatResourcecatalog)resource,fileMatcher,user);
			for (Entry<String, List<File>> entry : matches.entrySet()) {
				// First, we handle unproc resources differently.
				String newKey = entry.getKey();
				if (newLocation != null && newLocation.trim().length()>0) {
					newLocation = newLocation.replace(File.separator + "$", "");
					if (newKey.contains(File.separator)) {
						newKey = newKey.replaceFirst("^.*[/]", newLocation);
					} else {
						newKey = newLocation + File.separator + newKey;
					}
					if (newKey.contains(SESSION_LABEL_SUBST)) {
						newKey = newKey.replace(SESSION_LABEL_SUBST, exp.getLabel());
					}
					if (newKey.contains(SUBJECT_LABEL_SUBST) && exp instanceof XnatImagesessiondata) {
						newKey = newKey.replace(SUBJECT_LABEL_SUBST, ((XnatImagesessiondata)exp).getSubjectData().getLabel());
					}
				}
				if (!returnMap.containsKey(newKey)) {
					returnMap.put(newKey, new ArrayList<File>());
				}
				returnMap.get(newKey).addAll(entry.getValue());
			}
		}
	}

}
