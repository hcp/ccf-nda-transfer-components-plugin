package org.nrg.ccf.ndatransfer.components.pcp;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.nrg.ccf.common.utilities.components.NodeUtils;
import org.nrg.ccf.common.utilities.components.ScriptResult;
import org.nrg.ccf.common.utilities.utils.CcfFileUtils;
import org.nrg.ccf.common.utilities.utils.FileSizeUtils;
import org.nrg.ccf.ndatransfer.abst.DataPackageDefinition;
import org.nrg.ccf.ndatransfer.abst.ManifestPackageDefinition;
import org.nrg.ccf.ndatransfer.client.SubmissionServiceClient;
import org.nrg.ccf.ndatransfer.client.VtcmdClient;
import org.nrg.ccf.ndatransfer.client.entities.ProjectTransferSettings;
import org.nrg.ccf.ndatransfer.client.entities.SubmissionResource;
import org.nrg.ccf.ndatransfer.components.simple.SimplePackageValidator;
import org.nrg.ccf.ndatransfer.constants.ProcessingType;
import org.nrg.ccf.ndatransfer.constants.SubmissionConstants;
import org.nrg.ccf.ndatransfer.constants.SubmissionGrouping;
import org.nrg.ccf.ndatransfer.entities.NdaSubmissionEntity;
import org.nrg.ccf.ndatransfer.entities.NdaSubmissionItem;
import org.nrg.ccf.ndatransfer.entities.UploadBundle;
import org.nrg.ccf.ndatransfer.exception.ResumeSubmissionException;
import org.nrg.ccf.ndatransfer.exception.SubmissionException;
import org.nrg.ccf.ndatransfer.exception.ValidateAndUploadException;
import org.nrg.ccf.ndatransfer.pojo.PackageStats;
import org.nrg.ccf.ndatransfer.services.NdaSubmissionEntityService;
import org.nrg.ccf.ndatransfer.services.NdaSubmissionItemService;
import org.nrg.ccf.ndatransfer.services.NdaSubmissionStatementService;
import org.nrg.ccf.ndatransfer.utils.NdaTransferConfigUtils;
import org.nrg.ccf.pcp.abst.PipelineExecManagerA;
import org.nrg.ccf.pcp.anno.PipelineExecManager;
import org.nrg.ccf.pcp.constants.PcpConstants;
import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.ccf.pcp.inter.PcpCondensedStatusI;
import org.nrg.ccf.pcp.inter.PipelineSubmitterI;
import org.nrg.ccf.pcp.inter.PipelineValidatorI;
import org.nrg.ccf.pcp.services.PcpStatusEntityService;
import org.nrg.xdat.XDAT;
import org.nrg.xft.security.UserI;
import org.nrg.xnat.turbine.utils.ArcSpecManager;

import java.nio.file.Files;
import java.nio.file.Paths;

import com.google.gson.Gson;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@PipelineExecManager(resetStatusOnStartup = true)
public class NdaTransferExecManager extends PipelineExecManagerA {
	
	private final SimpleDateFormat _formatter = new SimpleDateFormat("yyyyMMddHHmm");
	final NdaTransferConfigUtils _configUtils = XDAT.getContextService().getBean(NdaTransferConfigUtils.class);
	final VtcmdClient _vtcmdClient = XDAT.getContextService().getBean(VtcmdClient.class);
	final NdaSubmissionEntityService _submissionEntityService = XDAT.getContextService().getBean(NdaSubmissionEntityService.class);
	final NdaSubmissionItemService _submissionItemService = XDAT.getContextService().getBean(NdaSubmissionItemService.class);
	final NdaSubmissionStatementService _submissionStatementService = XDAT.getContextService().getBean(NdaSubmissionStatementService.class);
	final SubmissionServiceClient _submissionServiceClient = XDAT.getContextService().getBean(SubmissionServiceClient.class);
	final ProjectTransferSettings _projectTransferSettings = XDAT.getContextService().getBean(ProjectTransferSettings.class);
	final PcpStatusEntityService _statusEntityService = XDAT.getContextService().getBean(PcpStatusEntityService.class);
	final NodeUtils _nodeUtils = XDAT.getContextService().getBean(NodeUtils.class);
	final SimpleDateFormat _ignoreDatef = new SimpleDateFormat(SimplePackageValidator.IGNORE_DATE_FMT_STR);
	final Gson _gson = new Gson();
	private final String TEMPDIR_PREFIX = "ndatransfer-";
	Integer _numThreads;

	@Override
	public void doSubmit(PipelineSubmitterI submitter, PipelineValidatorI validator, List<PcpCondensedStatusI> statusList, Map<String,String> parameters, UserI user) {
		
		List<PcpStatusEntity> entitiesForProcessing = new ArrayList<>();
		for (final PcpCondensedStatusI entity : statusList) {
			final PcpStatusEntity statusEntity = _statusEntityService.getStatusEntity(entity);
			if (statusEntity == null) {
				continue;
			}
			statusEntity.setStatus(PcpConstants.PcpStatus.QUEUED.toString());
			final Date statusTime = new Date();
			statusEntity.setStatusTime(statusTime);
			statusEntity.setStatusInfo("Processing queued at " + statusTime + " (NODE=" + _nodeUtils.getXnatNode() + ")");
			_statusEntityService.update(statusEntity);
			entitiesForProcessing.add(statusEntity);
		}
		final String submitByStr = parameters.get("NdaTransferSubmitter-submit-by");
		final String collectionIdStr = parameters.get("NdaTransferSubmitter-collection-id");
		final Integer collectionId = Integer.valueOf(collectionIdStr);
		SubmissionGrouping submitBy; 
		try {
			submitBy = SubmissionGrouping.valueOf(submitByStr);
		} catch (Exception e) {
			submitBy = SubmissionGrouping.SESSION;
		}
		final String resumeSubmitStr = parameters.get("NdaTransferSubmitter-resume-submission");
		Boolean resumeSubmission = false;
		try {
			resumeSubmission = Boolean.valueOf(resumeSubmitStr);
		} catch (Exception e) { 
			// Do nothing 
		}
		final String numThreadsStr = parameters.get("NdaTransferSubmitter-num-threads");
		_numThreads = 6;
		try {
			_numThreads = Integer.valueOf(numThreadsStr);
		} catch (Exception e) { 
			// Do nothing 
		}
		String additionalParametersStr = parameters.get("NdaTransferSubmitter-additional-parameters");
		if (additionalParametersStr == null) {
			additionalParametersStr="";
		} else {
			additionalParametersStr=additionalParametersStr.trim().replaceAll("  *", " ");
		}
		if (resumeSubmission) {
			resumePreviousSubmission(entitiesForProcessing, collectionId, additionalParametersStr, user);
			return;
		}
		final ProcessingType processingType = ProcessingType.valueOf(parameters.get("NdaTransferSubmitter-processing-type"));
		final Map<String,List<PcpStatusEntity>> submissionGroups = createSubmissionGroups(entitiesForProcessing, submitBy, parameters, processingType);
		final String removeTempDirStr = parameters.get("NdaTransferSubmitter-remove-tempdir");
		Boolean removeTempDir = false;
		try {
			removeTempDir = Boolean.valueOf(removeTempDirStr);
		} catch (Exception e) { 
			// Do nothing 
		}
		/*
		final String validateOnlyStr = parameters.get("NdaTransferSubmitter-validate-only");
		Boolean validateOnly = false;
		try {
			validateOnly = Boolean.valueOf(validateOnlyStr);
		} catch (Exception e) { 
			// Do nothing 
		}
		*/
		for (final Entry<String, List<PcpStatusEntity>> submission : submissionGroups.entrySet()) {
			try {
				doSubmission(submission, user, collectionId, removeTempDir, processingType, submissionGroups.entrySet().size(), 
						validator, parameters, additionalParametersStr);
			} catch (SubmissionException e) {
				final String exceptionInfoStr = "SubmissionException:" + e.toString() + "<br>NODE=" + _nodeUtils.getXnatNode();
				for (final PcpStatusEntity statusEntity : submission.getValue()) {
					if (statusEntity == null) {
						continue;
					}
					if (statusEntity.getStatus().equals(PcpConstants.PcpStatus.QUEUED.toString()) ||
							statusEntity.getStatus().equals(PcpConstants.PcpStatus.RUNNING.toString()) ||
							statusEntity.getStatus().equals(PcpConstants.PcpStatus.SUBMITTED.toString())) {
						statusEntity.setStatus(PcpConstants.PcpStatus.ERROR.toString());
						final Date statusTime = new Date();
						statusEntity.setStatusTime(statusTime);
						statusEntity.setStatusInfo(exceptionInfoStr);
						_statusEntityService.update(statusEntity);
					}
				}
				log.error("SubmissionException:  ", e);
			}
		}
	}

	private Map<String, List<PcpStatusEntity>> createSubmissionGroups(List<PcpStatusEntity> entitiesForProcessing,
			SubmissionGrouping submitBy, Map<String, String> parameters, ProcessingType processingType) {
		final Map<String,List<PcpStatusEntity>> submissionGroups = new LinkedHashMap<>();
		String descriptiveSegmentStr = parameters.get("NdaTransferSubmitter-descriptive-segment");
		descriptiveSegmentStr = (descriptiveSegmentStr != null) ? descriptiveSegmentStr.trim().replaceAll("[^0-9a-zA-Z]","") : "";
		String numGroupsStr = parameters.get("NdaTransferSubmitter-num-groups");
		Integer numGroups;
		try {
			numGroups = (numGroupsStr != null && numGroupsStr.trim().matches("^[0-9]+$")) ? Integer.valueOf(numGroupsStr.trim()) : 1;
		} catch (NumberFormatException e) {
			numGroups = 1;
		}
		if (descriptiveSegmentStr.length()>0) {
			descriptiveSegmentStr = ((!descriptiveSegmentStr.startsWith("_")) ? "_" : "") + descriptiveSegmentStr;
		}
		final String dt = _formatter.format(new Date());
		if (submitBy.equals(SubmissionGrouping.COMBINED)) {
			final Set<String> sessions = new HashSet<>();
			final Set<String> packages = new HashSet<>();
			final Map<String,List<PcpStatusEntity>> typeGroups = new LinkedHashMap<>();
			for (final PcpStatusEntity entity : entitiesForProcessing) {
				sessions.add(entity.getEntityLabel());
				packages.add(entity.getSubGroup());
				final String keyStr = getDataTypeForSubgroup(entity.getProject(), entity.getSubGroup(), processingType);
				if (!typeGroups.containsKey(keyStr)) {
					typeGroups.put(keyStr, new ArrayList<PcpStatusEntity>());
				}
				typeGroups.get(keyStr).add(entity);
			}
			for (final String keyStr : typeGroups.keySet()) {
				final List<PcpStatusEntity> entityList = typeGroups.get(keyStr);
				if (entityList.size()==1) {
					final PcpStatusEntity entity = entityList.get(0);
					submissionGroups.put(entity.getEntityLabel() + "_" + keyStr + "_" + entity.getSubGroup() + descriptiveSegmentStr +  "_" + dt, entityList);
				} else if (numGroups == 1) {
					submissionGroups.put("Sess" + sessions.size() + "_" + keyStr + "_Pkg" + packages.size() + descriptiveSegmentStr + "_" + dt, entityList);
				} else {
					for (Integer[] gInfo : computeGroups(numGroups, entityList)) {
						submissionGroups.put("Sess" + sessions.size() + "_" + keyStr + "_Pkg" + packages.size() + "_group" + formatNumber(gInfo[0],numGroups) +
								descriptiveSegmentStr + "_" + dt, entityList.subList(gInfo[1], gInfo[2]));
					}
				}
			}
			
		} else if (submitBy.equals(SubmissionGrouping.SEPARATE)) {
			for (final PcpStatusEntity entity : entitiesForProcessing) {
				final String dataType = getDataTypeForSubgroup(entity.getProject(), entity.getSubGroup(), processingType);
				final List<PcpStatusEntity> newList = new ArrayList<>();
				newList.add(entity);
				submissionGroups.put(entity.getEntityLabel() + "_" + dataType + "_" + entity.getSubGroup() + "_" + dataType + descriptiveSegmentStr + "_" + dt, newList);
			}
		} else if (submitBy.equals(SubmissionGrouping.SESSION)) {
			final Map<String,List<PcpStatusEntity>> sessionGroups = new LinkedHashMap<>();
			for (final PcpStatusEntity entity : entitiesForProcessing) {
				final String dataType = getDataTypeForSubgroup(entity.getProject(), entity.getSubGroup(), processingType);
				final String label = entity.getEntityLabel();
				final String keyStr = label + "_" + dataType; 
				if (!sessionGroups.containsKey(keyStr)) {
					sessionGroups.put(keyStr, new ArrayList<PcpStatusEntity>());
				}
				sessionGroups.get(keyStr).add(entity);
			}
			for (final String keyStr : sessionGroups.keySet()) {
				final List<PcpStatusEntity> entityList = sessionGroups.get(keyStr);
				if (entityList.size()==1) {
					final PcpStatusEntity entity = entityList.get(0);
					submissionGroups.put(keyStr + "_" + entity.getSubGroup() + descriptiveSegmentStr +  "_" + dt, entityList);
				} else if (numGroups == 1) {
					submissionGroups.put(keyStr + "_Pkg" + entityList.size() + descriptiveSegmentStr +  "_" + dt, entityList);
				} else {
					for (Integer[] gInfo : computeGroups(numGroups, entityList)) {
						submissionGroups.put(keyStr + "_Pkg" + entityList.size() + "_group" + formatNumber(gInfo[0],numGroups) + 
								descriptiveSegmentStr +  "_" + dt, entityList.subList(gInfo[1], gInfo[2]));
					}
				}
			}
		} else if (submitBy.equals(SubmissionGrouping.PACKAGE)) {
			final Map<String,List<PcpStatusEntity>> packageGroups = new LinkedHashMap<>();
			for (final PcpStatusEntity entity : entitiesForProcessing) {
				final String subgroup = entity.getSubGroup();
				if (!packageGroups.containsKey(subgroup)) {
					packageGroups.put(subgroup, new ArrayList<PcpStatusEntity>());
				}
				packageGroups.get(subgroup).add(entity);
			}
			for (final String subgroup : packageGroups.keySet()) {
				final List<PcpStatusEntity> entityList = packageGroups.get(subgroup);
				final String dataType = getDataTypeForSubgroup(entityList.get(0).getProject(), subgroup, processingType);
				if (dataType == null) {
					log.warn("WARNING:  data type is null for subgroup {}.",subgroup);
				}
				if (entityList.size()==1) {
					final PcpStatusEntity entity = entityList.get(0);
					submissionGroups.put(entity.getEntityLabel() + "_" + dataType + "_" + entity.getSubGroup() + descriptiveSegmentStr + "_" + dt, entityList);
				} else if (numGroups == 1) {
					submissionGroups.put("Sess" + entityList.size() + "_" + dataType + "_" + subgroup + descriptiveSegmentStr + "_" + dt, entityList);
				} else {
					for (Integer[] gInfo : computeGroups(numGroups, entityList)) {
						submissionGroups.put("Sess" + entityList.size() + "_" + dataType + "_" + subgroup + "_group" + formatNumber(gInfo[0], numGroups) +
								descriptiveSegmentStr + "_" + dt, entityList.subList(gInfo[1], gInfo[2]));
					}
				}
			}
		}
		return submissionGroups;
	}

	private String formatNumber(Integer val, Integer numGroups) {
		String leading = "00000";
		if (val.toString().length()>=numGroups.toString().length() || val<0 || numGroups<0) {
			return val.toString();
		} else {
			int needed = numGroups.toString().length()-val.toString().length();
			return leading.substring(0,needed) + val;
		}
	}

	private List<Integer[]> computeGroups(Integer numGroups, List<PcpStatusEntity> entitiesForProcessing) {
		final List<Integer[]> returnList = new ArrayList<>();
		final int listSize = entitiesForProcessing.size();
		if (numGroups>listSize) {
			numGroups = listSize;
		}
		final int groupSize = (int)Math.ceil((float)listSize/(float)numGroups);
		for (int i = 0; i<numGroups; i++) {
			final Integer[] returnArr = new Integer[3];
			int from = i*groupSize;
			int to = from + groupSize;
			if (to>=listSize) {
				to = listSize;
			}
			returnArr[0]=i+1;
			returnArr[1]=from;
			returnArr[2]=to;
			returnList.add(returnArr);
		}
		return returnList;
	}

	private String getDataTypeForSubgroup(String project, String subGroup, ProcessingType processingType) {
		final DataPackageDefinition packageInfo = _configUtils.getDataPackageInfo(project, subGroup);
		if (!processingType.equals(ProcessingType.PACKAGE_SIZE_REPORT)) {
			return packageInfo.getDataType().getDisplayName();
		} else {
			return "EVERYTHING_COMBINED_FOR_PACKAGE_REPORT";
		}
	}

	private void doSubmission(Entry<String, List<PcpStatusEntity>> submission, final UserI user, Integer collectionId, boolean shouldRemoveTempDir, 
			final ProcessingType processingType, Integer totalSubmissions, final PipelineValidatorI validator, final Map<String, String> parameters, String additionalParameters) throws SubmissionException {
			
		final Map<PcpStatusEntity, PackageStats> entityPackageStats = new TreeMap<>();
		final Map<String, PackageStats> subgroupPackageStats = new TreeMap<>();
		long totalPackageSize = 0;
		long totalPackageFiles = 0;
		
		final PcpStatusEntity entity0 = submission.getValue().get(0);
		final String entityProject = entity0.getProject();
		final String entitySubGroup = entity0.getSubGroup();
		final String tempdirLocation = _projectTransferSettings.getTempdirLocation(entityProject);
		final File tempdirParent = (tempdirLocation != null && tempdirLocation.trim().length()>0) ? new File(tempdirLocation) : null;
		File tempDir;
		try {
			final String dt = _formatter.format(new Date());
			final String prefix = TEMPDIR_PREFIX + dt + "-";
			tempDir = (tempdirParent != null && tempdirParent.isDirectory() && tempdirParent.canWrite()) ?
					Files.createTempDirectory(Paths.get(tempdirLocation), prefix).toFile() : 
					Files.createTempDirectory(prefix).toFile();
		} catch (IOException e) {
			log.error("Exception thrown: ",e);
			throw new SubmissionException(e);
		}
		
		if (submission.getValue()==null || submission.getValue().size()<1) {
			return;
		}
		final Date submitTime = new Date();
		final String prepareInfoStr;
		if (processingType.equals(ProcessingType.CINAB_STRUCTURE) || processingType.equals(ProcessingType.CINAB_ARCHIVE)) {
				prepareInfoStr = "Preparing CinaB structure - No NDA submission (TIME=" + submitTime + ", NODE=" + 
						_nodeUtils.getXnatNode() + ", TEMPDIR=" + tempDir.getAbsolutePath() + ")";
		} else if (processingType.equals(ProcessingType.PACKAGE_SIZE_REPORT)) {
				prepareInfoStr = "Building package size report (TIME=" + submitTime + ", NODE=" + _nodeUtils.getXnatNode() + ")";
		} else {
				prepareInfoStr = "Preparing files and metadata for NDA submission (TIME=" + submitTime + ", NODE=" + 
						_nodeUtils.getXnatNode() + ", TEMPDIR=" + tempDir.getAbsolutePath() + ")"; 
		} 
		for (final PcpStatusEntity entity : submission.getValue()) {
			entity.setStatus(PcpConstants.PcpStatus.RUNNING.toString());
			
			entity.setStatusTime(submitTime);
			entity.setStatusInfo(prepareInfoStr);
			_statusEntityService.update(entity);
		}
		final String archivePath = ArcSpecManager.GetInstance().getGlobalArchivePath();
		final File archiveDir = new File(archivePath);
		
		if (processingType.equals(ProcessingType.PACKAGE_SIZE_REPORT)) {
			
			for (final PcpStatusEntity entity : submission.getValue()) {
				final DataPackageDefinition dataPackageDef = _configUtils.getDataPackageInfo(entity.getProject(), entity.getSubGroup());
				if (dataPackageDef instanceof ManifestPackageDefinition) {
					final ManifestPackageDefinition manifestPackageDef = (ManifestPackageDefinition) dataPackageDef;
					final PackageStats packageStats = manifestPackageDef.computePackageStats(entity, user);
					entityPackageStats.put(entity, packageStats);
					final String subgroup = entity.getSubGroup();
					final PackageStats subgroupValue = (subgroupPackageStats.containsKey(subgroup)) ? subgroupPackageStats.get(subgroup) : new PackageStats();
					subgroupValue.setPackageFiles(subgroupValue.getPackageFiles()+packageStats.getPackageFiles());
					subgroupValue.setPackageSize(subgroupValue.getPackageSize()+packageStats.getPackageSize());
					subgroupPackageStats.put(subgroup, subgroupValue);
					totalPackageSize = totalPackageSize + packageStats.getPackageSize();
					totalPackageFiles = totalPackageFiles + packageStats.getPackageFiles();
				}
			}
			final StringBuilder infoBuilder = new StringBuilder();
			infoBuilder.append("<h2>Totals</h2>\n");
			infoBuilder.append("Package Count: " + entityPackageStats.size() + "<br>\n");
			infoBuilder.append("Package Files: " + totalPackageFiles + "<br>\n");
			infoBuilder.append("Package Size: " + FileSizeUtils.humanReadableByteCountBin(totalPackageSize) + "<br>\n");
			infoBuilder.append("<br>\n");
			infoBuilder.append("<h2>Totals by Package</h2>\n");
			infoBuilder.append("<table class=\"data-table xnat-table\">\n");
			infoBuilder.append("<thead><tr><th>Package</th><th>PackageCount</th><th>NumFiles</th><th>Size</th><th>AvgFileCount</th><th>AvgSize</th></tr></thead>\n");
			infoBuilder.append("<tbody class\"table-body\">\n");
			for (final Entry<String, PackageStats> entry : subgroupPackageStats.entrySet()) {
				final Long packageCount = getPackageCount(entry.getKey(), entityPackageStats);
				final Long avgSize = (entry.getValue().getPackageSize())/packageCount;
				final Long avgCount = (entry.getValue().getPackageFiles())/packageCount;
				infoBuilder.append("<tr><td>" + entry.getKey() + "</td><td>" +
							getPackageCount(entry.getKey(), entityPackageStats) + "</td><td>" + 
							entry.getValue().getPackageFiles() + "</td><td>" + 
							FileSizeUtils.humanReadableByteCountBin(entry.getValue().getPackageSize()) + "</td><td>" + 
							avgCount + "</td><td>" +
							FileSizeUtils.humanReadableByteCountBin(avgSize) + "</td></tr>\n");
			}
			infoBuilder.append("</tbody>\n");
			infoBuilder.append("</table>\n");
			infoBuilder.append("<br>\n");
			infoBuilder.append("<br>\n");
			infoBuilder.append("<h2>Package Detail</h2>\n");
			infoBuilder.append("<table class=\"data-table xnat-table\">\n");
			infoBuilder.append("<thead><tr><th>Entity</th><th>Package</th><th>PackageFiles</th><th>PackageSize</th></tr></thead>\n");
			infoBuilder.append("<tbody class\"table-body\">\n");
			for (final Entry<PcpStatusEntity, PackageStats> entry : entityPackageStats.entrySet()) {
				infoBuilder.append("<tr><td>" + entry.getKey().getEntityLabel() + 
						"</td><td>" + entry.getKey().getSubGroup() + "</td><td>" + 
						entry.getValue().getPackageFiles() + "</td><td>" + 
						FileSizeUtils.humanReadableByteCountBin(entry.getValue().getPackageSize()) + "</td></tr>\n");
			}
			infoBuilder.append("</tbody>\n");
			infoBuilder.append("</table>\n");
			final String infoBuilderStr = infoBuilder.toString();
			int counter = 0;
			for (final PcpStatusEntity entity : submission.getValue()) {
				counter += 1;
				entity.setStatus(PcpConstants.PcpStatus.COMPLETE.toString());
				if (counter<=25) {
					entity.setStatusInfo(infoBuilderStr);
				} else {
					entity.setStatusInfo(
						"Package Size Report generation complete, but report is only listed for first 25 entities.  Please find report on a prior entity."
					);
				}
				// We'll do the validation here rather than have the validator do it, so we can request status just for this statusEntity rather than
				// potentially use a cached version of the SubmissionResource that predates the submission.
				_statusEntityService.update(entity);
			}
			return;
			
		}
		
		try {
			final File manifestDir = new File(tempDir,"manifest");
			manifestDir.mkdir();
			final File fileDir;
			if (processingType.equals(ProcessingType.CINAB_STRUCTURE)) {
				final String cinabDirectory = parameters.get("NdaTransferSubmitter-processing-type");
				File cinabDir = new File(cinabDirectory);
				boolean dirOk = false;
				if ((cinabDir.exists() && cinabDir.isDirectory())) {
					dirOk = true;
				} else {
					dirOk = cinabDir.mkdir();
				}
				fileDir = (dirOk) ? cinabDir : new File(tempDir,"files");
			} else if (processingType.equals(ProcessingType.CINAB_ARCHIVE)) {
				boolean dirOk = false;
				File cinabDir = new File(archivePath + File.separator + "CinaB", entityProject);
				if ((cinabDir.exists() && cinabDir.isDirectory())) {
					dirOk = true;
				} else if (archiveDir.exists() && archiveDir.isDirectory()) {
					dirOk = cinabDir.mkdirs();
				}
				fileDir = (dirOk) ? cinabDir : new File(tempDir,"files");
			} else {
				fileDir = new File(tempDir,"files");
			}
			manifestDir.mkdir();
			final String submissionTitle = submission.getKey();
			// For whatever reason, vtcmd through singularity quit liking the space in the description
			final String submissionDesc = "ccf-nda-transfer-plugin_submission";
			final NdaSubmissionEntity submissionEntity = new NdaSubmissionEntity();
			if (submission.getValue().size()>0) {
				submissionEntity.setProjectId(submission.getValue().get(0).getProject());
				submissionEntity.setCollectionId(collectionId);
				submissionEntity.setTempDirectory(tempDir.getCanonicalPath());
			}
			submissionEntity.setSubmissionDatasetTitle(submissionTitle);
			submissionEntity.setSubmissionDatasetDescription(submissionDesc);
			if (processingType.equals(ProcessingType.VALIDATE_AND_UPLOAD)) {
				_submissionEntityService.create(submissionEntity);
			}
			
			// Make sure we're working with fresh configuration.
			_configUtils.clearPackageDefinitionCache(entityProject);
			final DataPackageDefinition packageInfo0 = _configUtils.getDataPackageInfo(entityProject, entitySubGroup);
			final String dataTypeName = packageInfo0.getDataType().getDisplayName();
			final File metaCSV = new File(tempDir, dataTypeName + ".csv");
			metaCSV.createNewFile();
			
			FileUtils.write(metaCSV, packageInfo0.generateMetadataFileHeader(), true);
			String projectId = null;
			
			final ExecutorService es = Executors.newFixedThreadPool(_numThreads);
			
			log.info("Start metadata processing at:  " + new Date());
			
			for (final PcpStatusEntity entity : submission.getValue()) {
				
				if (projectId != null) {
					if (!projectId.equals(entity.getProject())) {
						throw new SubmissionException("ERROR:  All submission entities must be from the same project");
					}
				} else {
					projectId = entity.getProject();
				}
				
				es.execute(new Runnable() {

					@Override
					public void run() {
						
						final NdaSubmissionItem submissionItem = new NdaSubmissionItem();
						submissionItem.setEntityId(entity.getEntityLabel());
						submissionItem.setEntityType(entity.getEntityType());
						submissionItem.setItemId(entity.getSubGroup());
						submissionEntity.addNdaSubmissionItem(submissionItem);
						if (processingType.equals(ProcessingType.VALIDATE_AND_UPLOAD)) {
							_submissionItemService.create(submissionItem);
						}
						
						final DataPackageDefinition packageInfo = _configUtils.getDataPackageInfoClone(entity.getProject(), entity.getSubGroup());
						for (String metaRecord : packageInfo.generateMetadataFileRecords(entity, manifestDir, fileDir, processingType, parameters, user)) {
							if (!metaRecord.endsWith(System.lineSeparator())) {
								metaRecord = metaRecord + System.lineSeparator();
							}
							try {
								synchronized(this) {
									FileUtils.write(metaCSV, metaRecord, true);
								}
							} catch (IOException e) {
								log.error("ERROR:  Error thrown writing metadata CSV record");
							}
						}
						
						if (processingType.equals(ProcessingType.CINAB_STRUCTURE) || processingType.equals(ProcessingType.CINAB_ARCHIVE)) {
							entity.setStatus(PcpConstants.PcpStatus.COMPLETE.toString());
							entity.setStatusTime(submitTime);
							entity.setStatusInfo("CinaB structure created at:  " + fileDir.getAbsolutePath());
							validator.validate(entity, user);
							_statusEntityService.update(entity);
						}
						
					}
				
				});
			}
			
			es.shutdown();
			es.awaitTermination(Long.MAX_VALUE, TimeUnit.SECONDS);
			log.info("Metadata processing complete at " + new Date());
			
			if (processingType.equals(ProcessingType.CINAB_STRUCTURE) || processingType.equals(ProcessingType.CINAB_ARCHIVE)) {
				// Prune any empty subdirectories created in submitted entities and remove broken symlinks.
				for (final PcpStatusEntity entity : submission.getValue()) {
					final String entityLabel = entity.getEntityLabel();
					final File entityCinabDir = new File(fileDir, entityLabel);
					if (entityCinabDir.isDirectory()) {
						CcfFileUtils.pruneEmptySubdirectories(entityCinabDir);
						CcfFileUtils.removeBrokenSymlinks(entityCinabDir);
					}
				}
				return;
			}
			
			if (processingType.equals(ProcessingType.VALIDATE_AND_UPLOAD)) {
				_submissionEntityService.update(submissionEntity);
			}
			
			log.info("Creating upload bundle");
			
			final UploadBundle uploadBundle = new UploadBundle();
			if (projectId == null) {
				throw new SubmissionException("ProjectID is null");
			}
			uploadBundle.setProjectId(projectId);
			uploadBundle.setExportCsv(metaCSV.getCanonicalPath());
			uploadBundle.getManifestDirs().add(manifestDir.getCanonicalPath());
			uploadBundle.getDatafileDirs().add(fileDir.getCanonicalPath());
			uploadBundle.setSubmissionTitle(submissionTitle);
			uploadBundle.setSubmissionDesc(submissionDesc);
			uploadBundle.setCollectionId(collectionId);
			final String submitInfoStr = "Transfer bundle " + submissionTitle + " submitted to the NDA at " + submitTime 
					+ " (NODE=" + _nodeUtils.getXnatNode() + ", TEMPDIR=" + tempDir.getAbsolutePath() + ")";
			for (final PcpStatusEntity entity : submission.getValue()) {
				entity.setStatus(PcpConstants.PcpStatus.RUNNING.toString());
				
				entity.setStatusTime(submitTime);
				entity.setStatusInfo(submitInfoStr);
				_statusEntityService.update(entity);
			}
			log.info("Begin NDA submission");
			final ScriptResult submitResult = ndaSubmit(user, uploadBundle, tempDir, additionalParameters, processingType.equals(ProcessingType.VALIDATE_ONLY));
			log.info("Submission complete");
			log.info(submitResult.buildResultsHtml());
			final String infoAppend = submitResult.buildResultsHtml();
			final String submissionId = infoAppend.replaceFirst("^.* Submission ID:  *", "").replaceFirst("[ ,].*$","");
			SubmissionResource submissionResource = null;
			if (NumberUtils.toLong(submissionId)>0) {
				// Let's wait a bit before requesting the submission resource, to give it time to update it.  This likely
				// won't be enough to register upload completion, so we'll clear the cache so requesting an update will pull current
				// status.
				Integer sleepSec;
				if (totalSubmissions<10) {
					sleepSec = 60*5;
				} else if (totalSubmissions<25) {
					sleepSec = 60*2;
				} else {
					sleepSec = 15;
				}
				try {
					Thread.sleep(1000*sleepSec);
				} catch (InterruptedException ie) {
					// Do nothing
				}
				submissionResource = _submissionServiceClient.getSubmissionResource(submissionEntity.getProjectId(),submissionId);
				if (collectionId != null) {
					_submissionServiceClient.clearCollectionCache(Long.valueOf(collectionId));
				}
			}
			
			log.info("Updating status entities");
			
			for (final PcpStatusEntity entity : submission.getValue()) {
				if (submitResult.isSuccess()) {
					entity.setStatus(PcpConstants.PcpStatus.COMPLETE.toString());
				} else {
					entity.setStatus(PcpConstants.PcpStatus.ERROR.toString());
				}
				
				final StringBuilder infoBuilder = new StringBuilder();
				infoBuilder.append(entity.getStatusInfo()).append(infoAppend);
				entity.setStatusInfo(infoBuilder.toString());
				// We'll do the validation here rather than have the validator do it, so we can request status just for this statusEntity rather than
				// potentially use a cached version of the SubmissionResource that predates the submission.
				doOwnValidation(entity, submissionResource);
				_statusEntityService.update(entity);
			}
		//} catch (IOException | ValidateAndUploadException e) {
		} catch (Exception e) {
			log.error("Exception thrown: ",e);
			throw new SubmissionException(e);
		} finally {
			if (shouldRemoveTempDir) {
				try {
					FileUtils.deleteDirectory(tempDir);
				} catch (IOException e) {
					// Do nothing
				}
			}
		}
	}
	
	private Long getPackageCount(String key, Map<PcpStatusEntity, PackageStats> entityPackageStats) {
		long count = 0;
		for (PcpStatusEntity entity : entityPackageStats.keySet()) {
			if (entity.getSubGroup().equals(key)) {
				count += 1;
			}
		}
		return count;
	}

	private void resumePreviousSubmission(List<PcpStatusEntity> entitiesForProcessing, Integer collectionId, String additionalParametersStr, UserI user) {
		for (final PcpStatusEntity entity : entitiesForProcessing) {
			final NdaSubmissionItem submissionItem = _submissionItemService.getItemForStatusEntity(entity);
			final NdaSubmissionEntity submissionEntity = submissionItem.getNdaSubmissionEntity();
			if (submissionEntity.getSubmissionStatus() == null || 
					!submissionEntity.getSubmissionStatus().equalsIgnoreCase(SubmissionConstants.UPLOAD_COMPLETE)) {
				// Let's request updated status from the NDA to verify not complete
				boolean resumeWithoutSubmissionId = false;
				SubmissionResource submitResource;
				if (submissionEntity.getSubmissionId() != null) {
					submitResource = _submissionServiceClient.getSubmissionResource(entity.getProject(), submissionEntity.getSubmissionId().toString());
				} else {
					submitResource = _submissionServiceClient.getSubmissionResourceForSubmissionEntity(submissionEntity, true);
					if (submitResource==null || submitResource.getSubmission_status() == null || 
							!submitResource.getSubmission_status().equalsIgnoreCase(SubmissionConstants.UPLOAD_COMPLETE)) {
						submitResource = _submissionServiceClient.getSubmissionResourceForSubmissionEntity(submissionEntity, false);
						if (submitResource != null) {
							submissionEntity.setSubmissionId(Integer.valueOf(submitResource.getSubmission_id()));
						}
					}
				}
				if (submitResource != null && submitResource.getSubmission_status() != null && 
							submitResource.getSubmission_status().equalsIgnoreCase(SubmissionConstants.UPLOAD_COMPLETE)) {
					entity.setStatus(PcpConstants.PcpStatus.COMPLETE.toString());
					final Date statusTime = new Date();
					entity.setStatusTime(statusTime);
					entity.setStatusInfo("SubmissionResource reports that upload is complete at " + statusTime + ".");
					doOwnValidation(entity, submitResource);
					_statusEntityService.update(entity);
					continue;
				}
				else if (submitResource == null || submitResource.getSubmission_id() == null) {
					resumeWithoutSubmissionId = true;
				}
				/*
				else if (submitResource == null) {
					entity.setStatus(PcpConstants.PcpStatus.ERROR.toString());
					entity.setStatusInfo("No NDA submission record exists for this session, so it cannot be resubmitted.");
					_statusEntityService.update(entity);
					continue;
				} else if (submitResource.getSubmission_id() == null) {
					entity.setStatus(PcpConstants.PcpStatus.ERROR.toString());
					entity.setStatusInfo("This submission was not assigned a submission ID, so it was not far enough along in the " +
							"submission process to be eligible for resuming the submission.");
					_statusEntityService.update(entity);
					continue;
				}
				*/
				try {
					final Date submitTime = new Date();
					entity.setStatus(PcpConstants.PcpStatus.RUNNING.toString());
					entity.setStatusTime(submitTime);
					entity.setStatusInfo("Resuming submit for submission " + submissionEntity.getSubmissionId() + " at " + submitTime +
							":" 
							+ "<br><br>COMMAND=" + 
							_vtcmdClient.getResumeSubmitCommandStr(user, submissionEntity, additionalParametersStr, resumeWithoutSubmissionId)
							+ "<br>NODE=" + _nodeUtils.getXnatNode()
							);
					_statusEntityService.update(entity);
					final ScriptResult uploadResult = _vtcmdClient.resumeSubmission(user, submissionEntity, additionalParametersStr, resumeWithoutSubmissionId);
					log.info(uploadResult.buildResultsHtml());
					final Date statusTime = new Date();
					entity.setStatusTime(statusTime);
					if (uploadResult.isSuccess()) {
						entity.setStatus(PcpConstants.PcpStatus.COMPLETE.toString());
						entity.setStatusInfo("Resume submission process was successful<br><br>DETAILS:" + uploadResult.buildResultsHtml());
						// Let's allow a lot of time before validtion to give the NDA time to register upload completion.
						try {
							Thread.sleep(1000*60*5);
						} catch (InterruptedException ie) {
							// Do nothing
						}
						submitResource = _submissionServiceClient.getSubmissionResourceForSubmissionEntity(submissionEntity, false);
						doOwnValidation(entity, submitResource);
					} else {
						entity.setStatus(PcpConstants.PcpStatus.ERROR.toString());
						entity.setStatusInfo("Resume submission process was not successful<br><br>DETAILS:" + uploadResult.buildResultsHtml());
						
					}
				} catch (ResumeSubmissionException rse) {
					entity.setStatus(PcpConstants.PcpStatus.ERROR.toString());
					final Date statusTime = new Date();
					entity.setStatusTime(statusTime);
					entity.setStatusInfo("Resume submission process threw exception " + ExceptionUtils.getFullStackTrace(rse));
					_statusEntityService.update(entity);
					continue;
					
				}
				_statusEntityService.update(entity);
				continue;
			}
			entity.setStatus(PcpConstants.PcpStatus.COMPLETE.toString());
			final Date statusTime = new Date();
			entity.setStatusTime(statusTime);
			entity.setStatusInfo("Resume submission complete at " + statusTime + 
					".  StatusEntity reports that upload is complete.  See earlier submission entities for details.");
			_statusEntityService.update(entity);
		}
	}
	

	private ScriptResult ndaSubmit(UserI user, UploadBundle uploadBundle, File tempDir, String additionalParameters, boolean validateOnly) throws ValidateAndUploadException {
		log.debug("Submitting uploadBundle:  ({})", uploadBundle.toString());
		final ScriptResult uploadResult;
		if (!validateOnly) {
			uploadResult = _vtcmdClient.validateAndUpload(user, uploadBundle, tempDir, additionalParameters);
		} else {
			uploadResult = _vtcmdClient.validate(user, uploadBundle, tempDir, additionalParameters);
		}
		log.info(uploadResult.buildResultsHtml());
		return uploadResult;
	}

	
	private void doOwnValidation(PcpStatusEntity entity, SubmissionResource submissionResource) {
		final NdaSubmissionItem submissionItem = _submissionItemService.getItemForStatusEntity(entity);
		updateSubmissionItem(submissionItem,submissionResource);
		final NdaSubmissionEntity submissionEntity = (submissionItem != null) ? submissionItem.getNdaSubmissionEntity() : null;
		final String submissionStatus = (submissionEntity != null) ? submissionEntity.getSubmissionStatus() : null;
		
		Date ignoreDate = null;
		String ignoreDateStr = "";
		final DataPackageDefinition packageInfo = _configUtils.getDataPackageInfo(entity.getProject(), entity.getSubGroup());
		if (packageInfo == null) {
			entity.setValidated(false);
			entity.setValidatedTime(new Date());
			entity.setValidatedInfo("ERROR:  _configUtils.getDataPackageInfo(project, subgroup) unexpectedly returned a null value");
			return;
		}
		final Map<String,String> configurableMap = packageInfo.getConfigurables();
		if (configurableMap != null && configurableMap.containsKey(SimplePackageValidator.IGNORE_DATE_FIELD)) {
			try {
				ignoreDateStr = configurableMap.get(SimplePackageValidator.IGNORE_DATE_FIELD);
				ignoreDate = _ignoreDatef.parse(ignoreDateStr);
			} catch (ParseException e) {
				// Handle later
			}
		}
		if (ignoreDate == null) {
			ignoreDate = new Date(0);
		}
		if (submissionStatus != null && submissionStatus.equals(SubmissionConstants.UPLOAD_COMPLETE) &&
				!submissionEntity.getCreated().before(ignoreDate)) {
			final String infoString = SimplePackageValidator.buildInfoString(submissionItem, "Submission is complete");
			if (!entity.getValidated() || !entity.getValidatedInfo().equals(infoString)) {
				entity.setValidated(true);
				entity.setValidatedTime(new Date());
				entity.setValidatedInfo(infoString);
			}
		} else {
			final String infoString = 
					(submissionStatus != null && submissionStatus.equals(SubmissionConstants.UPLOAD_COMPLETE)) ?
						SimplePackageValidator.buildInfoString(submissionItem, 
								"A submission record exists for this session and is complete, however was submitted before " + ignoreDateStr ) :
						SimplePackageValidator.buildInfoString(submissionItem, 
								"A submission record exists for this session/package, however the submission is not indicated as being complete:");
			if (entity.getValidated() || !entity.getValidatedInfo().equals(infoString)) {
				entity.setValidated(false);
				entity.setValidatedTime(new Date());
				entity.setValidatedInfo(infoString);
			}
		}
	}

	private void updateSubmissionItem(NdaSubmissionItem submissionItem, SubmissionResource submissionResource) {
		if (submissionItem == null || submissionResource == null) {
			return;
		}
		final NdaSubmissionEntity submissionEntity = submissionItem.getNdaSubmissionEntity();
		if (submissionEntity == null) {
			return;
		}
		submissionEntity.setSubmissionId(Integer.valueOf(submissionResource.getSubmission_id()));
		submissionEntity.setSubmissionStatus(submissionResource.getSubmission_status());
		_submissionItemService.update(submissionItem);
	}

}
