package org.nrg.ccf.ndatransfer.components.entities;

import java.io.File;
import java.util.List;

import org.nrg.ccf.ndatransfer.abst.ManifestPackageDefinition;
import org.nrg.ccf.ndatransfer.abst.ValueInfo;
import org.nrg.ccf.ndatransfer.anno.NdaPackageDefinition;
import org.nrg.ccf.ndatransfer.constants.NdaManifestType;
import org.nrg.ccf.ndatransfer.constants.NdaModality;
import org.nrg.ccf.ndatransfer.constants.NdaScanType;
import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.xdat.om.XnatImagesessiondata;
import org.nrg.xdat.om.XnatSubjectdata;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

//@Slf4j
@Getter
@Setter
@NdaPackageDefinition(description="Standard imagingcollection01 package definition")
public class ImagingCollectionPackageDefinition extends ManifestPackageDefinition {
	
	@Setter(AccessLevel.NONE)
	@Getter(AccessLevel.NONE)
	private final String _className = this.getClass().getSimpleName();
	
	private String imageCollectionDesc = "";
	private String jobName = "";
	private String procTypes = "";
	private String pipeline = "";
	private String pipelineScript = "";
	private String pipelineTools = "";
	private String pipelineType = "";
	private String pipelineVersion = "";
	private NdaModality modality;
	private NdaScanType mainScanType;
	private Integer experimentId;
	private Integer originDatasetId;
	
	
	public ImagingCollectionPackageDefinition() {
		super();
		dataType = NdaManifestType.IMAGING_COLLECTION_01;
	}
	
	
	@Override
	public List<String> configurationYaml() {
		
		final List<String> returnList = super.configurationYaml();
		final StringBuilder sb = new StringBuilder();
		sb.append("ImageCollectionDesc:\n")
		.append("    id: imageCollectionDesc\n")
		.append("    name: imageCollectionDesc\n")
		.append("    kind: panel.input.text\n")
		.append("    validation: required\n")
		.append("    label: Image Collection Description:\n")
		.append("JobName:\n")
		.append("    id: jobName\n")
		.append("    name: jobName\n")
		.append("    kind: panel.input.text\n")
		.append("    size: 50\n")
		.append("    label: Job Name:\n")
		.append("ProcTypes:\n")
		.append("    id: procTypes\n")
		.append("    name: procTypes\n")
		.append("    kind: panel.input.text\n")
		.append("    size: 50\n")
		.append("    label: Proc Types:\n")
		.append("Pipeline:\n")
		.append("    id: pipeline\n")
		.append("    name: pipeline\n")
		.append("    kind: panel.input.text\n")
		.append("    size: 50\n")
		.append("    label: Pipeline:\n")
		.append("PipelineScript:\n")
		.append("    id: pipelineScript\n")
		.append("    name: pipelineScript\n")
		.append("    kind: panel.input.text\n")
		.append("    size: 50\n")
		.append("    label: PipelineScript:\n")
		.append("PipelineTools:\n")
		.append("    id: pipelineTools\n")
		.append("    name: pipelineTools\n")
		.append("    kind: panel.input.text\n")
		.append("    size: 50\n")
		.append("    label: PipelineTools:\n")
		.append("PipelineType:\n")
		.append("    id: pipelineType\n")
		.append("    name: pipelineType\n")
		.append("    kind: panel.input.text\n")
		.append("    size: 50\n")
		.append("    label: PipelineType:\n")
		.append("PipelineVersion:\n")
		.append("    id: pipelineVersion\n")
		.append("    name: pipelineVersion\n")
		.append("    kind: panel.input.text\n")
		.append("    size: 50\n")
		.append("    label: PipelineVersion:\n")
		.append("Modality:\n")
		.append("    id: modality\n")
		.append("    name: modality\n")
		.append("    kind: panel.select.single\n")
		.append("    label: Modality:\n")
		.append("    validation: required\n")
		.append("    options: \n");
		for (final NdaModality value : NdaModality.values()) {
			final ValueInfo valueInfo = value.getValueInfo();
			sb.append("        \"");
			sb.append(valueInfo.getValue());
			sb.append("\": \"");
			sb.append(valueInfo.getDisplayName());
			sb.append("\"\n");
		}
		sb.append("MainScanType:\n")
		.append("    id: mainScanType\n")
		.append("    name: mainScanType\n")
		.append("    kind: panel.select.single\n")
		.append("    label: Main Scan Type:\n")
		.append("    validation: required\n")
		.append("    options: \n");
		for (final NdaScanType value : NdaScanType.values()) {
			final ValueInfo valueInfo = value.getValueInfo();
			sb.append("        \"");
			sb.append(valueInfo.getValue());
			sb.append("\": \"");
			sb.append(valueInfo.getDisplayName());
			sb.append("\"\n");
		}
		sb.append("ExperimentId:\n")
		.append("    id: experimentId\n")
		.append("    name: experimentId\n")
		.append("    kind: panel.input.text\n")
		.append("    label: ExperimentId:\n")
		.append("    validation: integer\n")
		.append("    description: An integer experiment ID for the Experiment/settings/run (required for fMRI)\n");
		returnList.add(sb.toString());
		return returnList;
	}
	
	@Override
	public String generateMetadataFileHeader() {
		return "imagingcollection,1,,,,,,,,,,,,,,,," + System.lineSeparator() + 
				"subjectKey,src_subject_id,origin_dataset_id,interview_date,interview_age,gender,image_collection_desc,job_name,proc_types,"
				+ "pipeline,pipeline_script,pipeline_tools,pipeline_type,pipeline_version,image_modality,scan_type,image_manifest,experiment_id" 
				+ System.lineSeparator();
	}
	
	@Override
	protected String buildMetadataRecord(PcpStatusEntity entity, XnatImagesessiondata session, File manifestFile, Object recordInfo) {
		final StringBuilder metaSb = new StringBuilder();
		final String comma = ",";
		//final String empty = "";
		final XnatSubjectdata subj = session.getSubjectData();
		// subjectKey
		metaSb.append(_guidUtils.getGuidForSubject(subj));
		metaSb.append(comma);
		// src_subject_id
		metaSb.append(subj.getLabel());
		metaSb.append(comma);
		// origin_dataset_id
		metaSb.append((getOriginDatasetId()!=null) ? getOriginDatasetId() : "");
		metaSb.append(comma);
		// interview_date
		metaSb.append(_dateUtils.getInterviewDate(session));
		metaSb.append(comma);
		// interview_age
		metaSb.append(_ageUtils.getInterviewAge(session));
		metaSb.append(comma);
		// gender
		metaSb.append(_genderUtils.getInterviewGender(subj));
		metaSb.append(comma);
		// description
		safeAppend(metaSb, getImageCollectionDesc());
		metaSb.append(comma);
		// job_name
		safeAppend(metaSb, getJobName());
		metaSb.append(comma);
		// proc_types
		safeAppend(metaSb, getProcTypes());
		metaSb.append(comma);
		// pipeline
		safeAppend(metaSb, getPipeline());
		metaSb.append(comma);
		// pipeline_script
		safeAppend(metaSb, getPipelineScript());
		metaSb.append(comma);
		// pipline_tools
		safeAppend(metaSb, getPipelineTools());
		metaSb.append(comma);
		// pipeline_type
		safeAppend(metaSb, getPipelineType());
		metaSb.append(comma);
		// pipeline_version
		safeAppend(metaSb, getPipelineVersion());
		metaSb.append(comma);
		// modality
		safeAppend(metaSb, getModality().getDisplayName());
		metaSb.append(comma);
		// main_scan_type
		safeAppend(metaSb, getMainScanType().getDisplayName());
		metaSb.append(comma);
		// manifest
		safeAppend(metaSb, manifestFile.getName());
		metaSb.append(comma);
		// experiment_id
		safeAppend(metaSb, (getExperimentId()!=null) ? getExperimentId().toString() : "");
		metaSb.append(System.lineSeparator());
		return metaSb.toString();
	}
	
}
