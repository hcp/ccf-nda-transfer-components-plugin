package org.nrg.ccf.ndatransfer.components.pcp;

import org.nrg.ccf.ndatransfer.constants.PrimaryUsage;
import org.nrg.ccf.pcp.anno.PipelineSelector;
import org.nrg.ccf.pcp.inter.PipelineSelectorI;

@PipelineSelector
public class NdaTransferTransferOnlySelector extends NdaTransferSelector implements PipelineSelectorI {
	
	public NdaTransferTransferOnlySelector() {
		super();
		_primaryUsage = PrimaryUsage.TRANSFERS;
	}
	
}
