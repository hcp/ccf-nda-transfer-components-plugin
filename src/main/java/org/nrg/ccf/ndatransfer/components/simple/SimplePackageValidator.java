package org.nrg.ccf.ndatransfer.components.simple;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.nrg.ccf.ndatransfer.abst.DataPackageDefinition;
import org.nrg.ccf.ndatransfer.abst.NdaPackageValidatorI;
import org.nrg.ccf.ndatransfer.anno.NdaPackageValidator;
import org.nrg.ccf.ndatransfer.client.SubmissionServiceClient;
import org.nrg.ccf.ndatransfer.client.entities.SubmissionResource;
import org.nrg.ccf.ndatransfer.constants.SubmissionConstants;
import org.nrg.ccf.ndatransfer.entities.NdaSubmissionEntity;
import org.nrg.ccf.ndatransfer.entities.NdaSubmissionItem;
import org.nrg.ccf.ndatransfer.services.NdaSubmissionEntityService;
import org.nrg.ccf.ndatransfer.services.NdaSubmissionItemService;
import org.nrg.ccf.ndatransfer.utils.NdaTransferConfigUtils;
import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.ccf.pcp.inter.ConfigurableComponentI;
import org.nrg.xdat.XDAT;
import org.nrg.xft.security.UserI;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@NdaPackageValidator
public class SimplePackageValidator implements NdaPackageValidatorI, ConfigurableComponentI  {
	
	public static final String CLASS_NAME = SimplePackageValidator.class.getName().replace(".", "");
	public static final String IGNORE_DATE_FIELD = CLASS_NAME + "-ignore-date";
	public static final String IGNORE_DATE_FMT_STR = "yyyyMMdd";
	static final SimpleDateFormat DATE_FMT = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
	static final SimpleDateFormat IGNORE_DATE_FMT = new SimpleDateFormat(IGNORE_DATE_FMT_STR);
	final SimpleDateFormat _ignoreDateFmt = new SimpleDateFormat(IGNORE_DATE_FMT_STR);
	static final String NO_SUBMISSION_ITEM_STATUS = "There is no submission record for this session/package.";
	final SubmissionServiceClient _serviceClient = XDAT.getContextService().getBean(SubmissionServiceClient.class);
	final NdaSubmissionItemService _submissionItemService = XDAT.getContextService().getBean(NdaSubmissionItemService.class);
	final NdaSubmissionEntityService _submissionEntityService = XDAT.getContextService().getBean(NdaSubmissionEntityService.class);
	final NdaTransferConfigUtils _configUtils = XDAT.getContextService().getBean(NdaTransferConfigUtils.class);

	@Override
	public List<String> getConfigurationYaml() {
		final List<String> returnList = new ArrayList<>();
		final String ele = 
				"ignore-date:\n" +
				"    id: " + IGNORE_DATE_FIELD + "\n" +
				"    name: " + IGNORE_DATE_FIELD + "\n" +
				"    kind:  panel.input.text\n" + 
				"    label: Ignore Submissions Before:\n" +  
				"    placeholder:  YYYYMMDD\n" + 
				"    description:  (OPTIONAL)  Ignore any submissions before configured date in determining validation status  " +
				"                  Enter date in YYYYMMDD Format.  This is useful when data or formatting changes require resubmission " +
				"                  of data that has already been successfully sent";
		returnList.add(ele);
		return returnList;
	}

	@Override
	public void validate(PcpStatusEntity statusEntity, UserI user, DataPackageDefinition packageInfo) {
		final NdaSubmissionItem submissionItem = _submissionItemService.getItemForStatusEntity(statusEntity);
		final Map<String,String> configurableMap = packageInfo.getConfigurables();
		Date ignoreDate = null;
		String ignoreDateStr = "";
		if (configurableMap != null && configurableMap.containsKey(IGNORE_DATE_FIELD)) {
			try {
				ignoreDateStr = configurableMap.get(IGNORE_DATE_FIELD);
				ignoreDate = _ignoreDateFmt.parse(ignoreDateStr);
			} catch (ParseException e) {
				// Handle later
			}
		}
		if (ignoreDate == null) {
			ignoreDate = new Date(0);
		}
		if (submissionItem != null) {
			try {
				updateSubmissionItem(submissionItem);
			} catch (Exception e) {
				log.warn("Update of submission item threw exception", e);
			}
			final NdaSubmissionEntity submissionEntity = (submissionItem != null) ? submissionItem.getNdaSubmissionEntity() : null;
			final String submissionStatus = (submissionEntity != null) ? submissionEntity.getSubmissionStatus() : null;
			if (submissionStatus != null && submissionStatus.equals(SubmissionConstants.UPLOAD_COMPLETE) &&
					!submissionEntity.getCreated().before(ignoreDate)) {
				final String infoString = buildInfoString(submissionItem, "Submission is complete");
				if (!statusEntity.getValidated() || !statusEntity.getValidatedInfo().equals(infoString)) {
					statusEntity.setValidated(true);
					statusEntity.setValidatedTime(new Date());
					statusEntity.setValidatedInfo(infoString);
				}
			} else {
				final String infoString = 
						(submissionStatus != null && submissionStatus.equals(SubmissionConstants.UPLOAD_COMPLETE)) ?
							buildInfoString(submissionItem, 
									"A submission record exists for this session and is complete, however was submitted before " + ignoreDateStr ) :
							buildInfoString(submissionItem, 
									"A submission record exists for this session/package, however the submission is not indicated as being complete:");
				if (statusEntity.getValidated() || !statusEntity.getValidatedInfo().equals(infoString)) {
					statusEntity.setValidated(false);
					statusEntity.setValidatedTime(new Date());
					statusEntity.setValidatedInfo(infoString);
				}
			}
		} else {
			if (!statusEntity.getValidatedInfo().equals(NO_SUBMISSION_ITEM_STATUS)) {
				statusEntity.setValidated(false);
				statusEntity.setValidatedTime(new Date());
				statusEntity.setValidatedInfo(NO_SUBMISSION_ITEM_STATUS);
			}
		}
	}

	public synchronized static String buildInfoString(NdaSubmissionItem submissionItem, String status) {
		if (submissionItem == null || submissionItem.getNdaSubmissionEntity() == null) {
			return "No submission record exists for this session/package";
		}
		final NdaSubmissionEntity submissionEntity = submissionItem.getNdaSubmissionEntity();
		final Integer collectionID = submissionEntity.getCollectionId();
		final String submissionStatus = submissionEntity.getSubmissionStatus();
		final Integer submissionID = submissionEntity.getSubmissionId();
		final String submissionDatasetTitle = submissionEntity.getSubmissionDatasetTitle();
		final String submissionDatasetDescription = submissionEntity.getSubmissionDatasetDescription();
		final String tempDirectory = submissionEntity.getTempDirectory();
		final Date submissionDate = submissionItem.getNdaSubmissionEntity().getCreated();
		final String submissionDateStr = (submissionDate!=null) ? DATE_FMT.format(submissionDate) : "null";
		final StringBuilder sb = new StringBuilder(status);
		sb.append("<ul>");
		sb.append("<li>Submission Date:  ").append(submissionDateStr).append("</li>");
		sb.append("<li>collectionID:  ").append(collectionID).append("</li>");
		sb.append("<li>submissionID:  ").append(submissionID).append("</li>");
		sb.append("<li>submissionDatasetTitle:  ").append(submissionDatasetTitle).append("</li>");
		sb.append("<li>submissionDatasetDescription:  ").append(submissionDatasetDescription).append("</li>");
		sb.append("<li>submissionStatus:  ").append(submissionStatus).append("</li>");
		sb.append("<li>tempDirectory:  ").append(tempDirectory).append("</li>");
		sb.append("</ul>");
		return sb.toString();
	}

	private void updateSubmissionItem(NdaSubmissionItem submissionItem) {
		if (submissionItem == null) {
			return;
		}
		final NdaSubmissionEntity submissionEntity = submissionItem.getNdaSubmissionEntity();
		if (submissionEntity == null) {
			return;
		}
		final Integer collectionId = submissionEntity.getCollectionId();
		final String submissionDatasetTitle = submissionEntity.getSubmissionDatasetTitle();
		final List<SubmissionResource> submissionResources = _serviceClient.getSubmissionResources(submissionEntity.getProjectId(),Long.valueOf(collectionId),true);
		if (submissionResources == null) {
			log.error("ERROR: _serviceClient.getSubmissionResources(project,collectionId,true) unexpectedly returned null value.  (" + submissionItem.toString() + ")");
			return;
		}
		for (final SubmissionResource submissionResource : submissionResources) {
			if (submissionResource.getDataset_title().equals(submissionDatasetTitle)) {
				if (
					(submissionEntity.getSubmissionId() == null || submissionEntity.getSubmissionStatus() == null) ||
					!( 
						submissionEntity.getSubmissionId().equals(Integer.valueOf(submissionResource.getSubmission_id())) &&
						submissionEntity.getSubmissionStatus().equals(submissionResource.getSubmission_status())
					) ) {
					submissionEntity.setSubmissionId(Integer.valueOf(submissionResource.getSubmission_id()));
					submissionEntity.setSubmissionStatus(submissionResource.getSubmission_status());
					_submissionEntityService.update(submissionEntity);
				}
				return;
			}
		}
	}

}
