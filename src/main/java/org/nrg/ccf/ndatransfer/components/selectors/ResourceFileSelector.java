package org.nrg.ccf.ndatransfer.components.selectors;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.nrg.ccf.ndatransfer.abst.DataPackageDefinition;
import org.nrg.ccf.ndatransfer.anno.FileSelector;
import org.nrg.ccf.ndatransfer.components.abst.AbstractRegexResourceFileSelector;
import org.nrg.ccf.ndatransfer.constants.MatchOperator;
import org.nrg.ccf.ndatransfer.constants.ResourceType;
import org.nrg.ccf.ndatransfer.interfce.ExperimentFileSelectorI;
import org.nrg.ccf.ndatransfer.interfce.FileMatcherI;
import org.nrg.xdat.model.XnatAbstractresourceI;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.model.XnatImagesessiondataI;
import org.nrg.xdat.om.XnatResourcecatalog;
import org.nrg.xft.security.UserI;

import lombok.Getter;
import lombok.Setter;
//import lombok.extern.slf4j.Slf4j;
//import lombok.AccessLevel;

//@Slf4j
@Getter
@Setter
@FileSelector
public class ResourceFileSelector extends AbstractRegexResourceFileSelector implements ExperimentFileSelectorI {
	
	public ResourceFileSelector() {
		super();
	}
	
	public ResourceFileSelector(ResourceType resourceType, String matchRegex, String excludeRegex, MatchOperator matchOperator, Integer matchCount) {
		super(resourceType, matchRegex, excludeRegex, matchOperator, matchCount);
	}

	@Override
	public Map<String,List<File>> generateFileMap(DataPackageDefinition packageInfo, XnatImagesessiondataI exp, UserI user) {
		final Map<String, List<File>> returnMap = new LinkedHashMap<>();
		switch (resourceType) {
			case SESSION:
				final List<XnatAbstractresourceI> sessionResources = exp.getResources_resource();
				for (final XnatAbstractresourceI resource : findMatchingResources(sessionResources)) {
					if (!(resource instanceof XnatResourcecatalog)) {
						continue;
					}
					for (final FileMatcherI fileMatcher : getFileMatchers()) {
						final Map<String, List<File>> matches = findFileMatches((XnatResourcecatalog)resource,fileMatcher,user);
						for (Entry<String, List<File>> entry : matches.entrySet()) {
							if (!returnMap.containsKey(entry.getKey())) {
								returnMap.put(entry.getKey(), new ArrayList<File>());
							}
							returnMap.get(entry.getKey()).addAll(entry.getValue());
						}
					}
				}
				return returnMap;
			case SCAN:
				final List<XnatImagescandataI> sessionScans = getRelevantScans(exp); 
				for (final XnatAbstractresourceI resource : findMatchingScanResources(sessionScans)) {
					if (!(resource instanceof XnatResourcecatalog)) {
						continue;
					}
					for (final FileMatcherI fileMatcher : getFileMatchers()) {
						final Map<String, List<File>> matches = findFileMatches((XnatResourcecatalog)resource,fileMatcher,user);
						for (Entry<String, List<File>> entry : matches.entrySet()) {
							if (!returnMap.containsKey(entry.getKey())) {
								returnMap.put(entry.getKey(), new ArrayList<File>());
							}
							returnMap.get(entry.getKey()).addAll(entry.getValue());
						}
					}
				}
				return returnMap;
		}
		return returnMap;
	}
	
}
