package org.nrg.ccf.ndatransfer.components.entities;

import org.nrg.ccf.ndatransfer.anno.NdaPackageDefinition;
import org.nrg.ccf.ndatransfer.constants.NdaManifestType;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
//import lombok.experimental.Accessors;

@Getter
@Setter
@NdaPackageDefinition(description="Standard imagingcollection01 package definition (gender renamed)")
public class ImagingCollection01PackageDefinition_sx extends ImagingCollection01PackageDefinition {
	
	@Setter(AccessLevel.NONE)
	@Getter(AccessLevel.NONE)
	private final String _className = this.getClass().getSimpleName();
	
	public ImagingCollection01PackageDefinition_sx() {
		super();
		dataType = NdaManifestType.IMAGING_COLLECTION_01;
	}
	
	@Override
	public String generateMetadataFileHeader() {
		return "imagingcollection,01,,,,,,,,,,,,,,,,,,," + 
				System.lineSeparator() +
				"subjectkey,src_subject_id,interview_date,interview_age,sex,image_collection_desc,job_name,proc_types,pipeline," + 
				"pipeline_script,pipeline_tools,pipeline_type,pipeline_version,image_modality,scan_type,image_manifest,experiment_id," +
				"img03_id2,file_source2,img03_id,file_source" + 
				System.lineSeparator();
	}

}
