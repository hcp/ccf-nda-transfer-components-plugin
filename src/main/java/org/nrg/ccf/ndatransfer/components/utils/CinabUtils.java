package org.nrg.ccf.ndatransfer.components.utils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.nrg.ccf.ndatransfer.components.pcp.NdaTransferExecManager;
import org.nrg.ccf.pcp.dto.ComponentSet;
import org.nrg.ccf.pcp.dto.PcpConfigInfo;
import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.ccf.pcp.exception.PcpComponentSetException;
import org.nrg.ccf.pcp.inter.PcpCondensedStatusI;
import org.nrg.ccf.pcp.inter.PipelineExecManagerI;
import org.nrg.ccf.pcp.inter.PipelineSubmitterI;
import org.nrg.ccf.pcp.inter.PipelineValidatorI;
import org.nrg.ccf.pcp.services.PcpStatusEntityService;
import org.nrg.ccf.pcp.utils.PcpConfigUtils;
import org.nrg.ccf.pcp.utils.PcpUtils;
import org.nrg.xft.security.UserI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

@Lazy
@Component
public class CinabUtils {
	
	// TODO:  Do we want to autogenerate the pipeline or have configuration allow users to specify it? 
	// Otherwise users can name it anything and this wont be found.
	private static final String CINAB_PIPELINE_NAME = "CinabPipeline";
	
	private PcpConfigUtils _pcpConfigUtils;
	private PcpUtils _pcpUtils;
	private PcpStatusEntityService _entityService;
	final Map<String,String> _parameters = new HashMap<>();
	
	@Autowired
	public CinabUtils(PcpConfigUtils pcpConfigUtils, PcpUtils pcpUtils, PcpStatusEntityService entityService) {
		super();
		_pcpConfigUtils = pcpConfigUtils;
		_pcpUtils = pcpUtils;
		_entityService = entityService;
		_parameters.put("NdaTransferSubmitter-submit-by", "COMBINED");
		_parameters.put("NdaTransferSubmitter-collection-id", "9999");
		_parameters.put("NdaTransferSubmitter-resume-submission", "false");
		_parameters.put("NdaTransferSubmitter-remove-tempdir", "true");
		_parameters.put("NdaTransferSubmitter-processing-type", "CINAB_ARCHIVE");
	}

	public boolean generateCinabForEntity(String projectId, String entityLabel, UserI user) {
		final PcpConfigInfo config = _pcpConfigUtils.getProjectPcpConfigInfo(projectId, CINAB_PIPELINE_NAME);
		if (config == null) {
			return false;
		}
		final ComponentSet componentSet;
		try {
			componentSet = _pcpUtils.getComponentSet(projectId, CINAB_PIPELINE_NAME);
			if (componentSet == null) {
				return false;
			}
		} catch (PcpComponentSetException e) {
			return false;
		}
		final List<PcpCondensedStatusI> entityList = 
				_entityService.getCondensedStatusEntities(projectId, CINAB_PIPELINE_NAME, entityLabel);
		final PipelineExecManagerI execManager = componentSet.getExecManager();
		final PipelineValidatorI validator = componentSet.getValidator();
		final PipelineSubmitterI submitter = componentSet.getSubmitter();
		execManager.submitJobs(submitter, validator, entityList, _parameters, user);
		return false;
	}

}
