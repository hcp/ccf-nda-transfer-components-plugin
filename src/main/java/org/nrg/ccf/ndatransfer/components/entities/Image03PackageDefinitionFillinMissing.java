package org.nrg.ccf.ndatransfer.components.entities;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.nrg.ccf.common.utilities.constants.CommonConstants;
import org.nrg.ccf.common.utilities.utils.CcfNiftiUtils;
import org.nrg.ccf.ndatransfer.abst.ManifestPackageDefinition;
import org.nrg.ccf.ndatransfer.abst.ValueInfo;
import org.nrg.ccf.ndatransfer.anno.NdaPackageDefinition;
import org.nrg.ccf.ndatransfer.constants.NdaImageFileFormat;
import org.nrg.ccf.ndatransfer.constants.NdaImageOrientation;
import org.nrg.ccf.ndatransfer.constants.NdaImageUnit;
import org.nrg.ccf.ndatransfer.constants.NdaManifestType;
import org.nrg.ccf.ndatransfer.constants.NdaModality;
import org.nrg.ccf.ndatransfer.constants.NdaQcOutcome;
import org.nrg.ccf.ndatransfer.constants.NdaScanObject;
import org.nrg.ccf.ndatransfer.constants.NdaScanType;
import org.nrg.ccf.ndatransfer.constants.NdaYesNo;
import org.nrg.ccf.ndatransfer.constants.ProcessingType;
import org.nrg.ccf.ndatransfer.exception.SubmissionException;
import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.xdat.model.XnatAbstractresourceI;
import org.nrg.xdat.model.XnatAddfieldI;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.om.XnatImagescandata;
import org.nrg.xdat.om.XnatImagesessiondata;
import org.nrg.xdat.om.XnatMrscandata;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xdat.om.XnatResourcecatalog;
import org.nrg.xdat.om.XnatSubjectdata;
import org.nrg.xft.security.UserI;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xnat.nifti.NiftiHeader;

import com.google.common.collect.ImmutableList;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import edu.emory.mathcs.backport.java.util.concurrent.TimeUnit;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Getter
@Setter
@NdaPackageDefinition(description="Standard imagingcollection01 package definition")
public class Image03PackageDefinitionFillinMissing extends ManifestPackageDefinition {
	
	@Setter(AccessLevel.NONE)
	@Getter(AccessLevel.NONE)
	private final String _className = this.getClass().getSimpleName();
	@Setter(AccessLevel.NONE)
	@Getter(AccessLevel.NONE)
	private JAXBContext _niftiContext;
	@Setter(AccessLevel.NONE)
	@Getter(AccessLevel.NONE)
	private Unmarshaller _niftiUnmarshaller;
	@Setter(AccessLevel.NONE)
	@Getter(AccessLevel.NONE)
	private final Gson _gson = new Gson();
	@Setter(AccessLevel.NONE)
	@Getter(AccessLevel.NONE)
	final Type _floatType = new TypeToken<ArrayList<Float>>(){}.getType();  
	@Setter(AccessLevel.NONE)
	@Getter(AccessLevel.NONE)
	private NdaYesNo transformationPerformedOrig;
	
	@Setter(AccessLevel.NONE)
	@Getter(AccessLevel.NONE)
	private final String ECHO_TIME="EchoTime";
	
	private String pathToFslhd = "";
	private List<String> seriesDescriptions = new ArrayList<>();
	private String commentsMisc = "";
	private File imageFile;
	private File imageThumbnailFile;
	private String imageDescription = "";
	private Integer experimentId;
	private Map<String,Integer> experimentIdMapObj = new HashMap<>();
	private String experimentIdMap = "";
	private NdaScanType scanType;
	private NdaScanObject scanObject;
	private NdaImageFileFormat imageFileFormat;
	private File dataFile2;
	private String dataFile2Type = "";
	private NdaModality imageModality;
	private String scannerManufacturerPd;
	private String scannerTypePd = "";
	private String scannerSoftwareVersionsPd = "";
	private String magneticFieldStrength = "";
	private Float mriRepetitionTimePd;
	private String mriEchoTimePd = "";
	private String flipAngle = "";
	private String acquisitionMatrix = "";
	private String mriFieldOfViewPd = "";
	private String patientPosition = "";
	private String photometInterpret = "";
	private String receiveCoil = "";
	private String transmitCoil = "";
	private NdaYesNo transformationPerformed;
	private String transformationType = "";
	private List<String> facemaskingSeriesDescriptions = new ArrayList<>();
	private String facemaskingTransformationType = "";
	private String imageHistory = "";
	private Integer imageNumDimensions;
	private Integer imageExtent1;
	private Integer imageExtent2;
	private Integer imageExtent3;
	private Integer imageExtent4;
	private String extent4Type;
	private Integer imageExtent5;
	private String extent5Type;
	private NdaImageUnit imageUnit1;
	private NdaImageUnit imageUnit2;
	private NdaImageUnit imageUnit3;
	private NdaImageUnit imageUnit4;
	private NdaImageUnit imageUnit5;
	private Float imageResolution1;
	private Float imageResolution2;
	private Float imageResolution3;
	private Float imageResolution4;
	private Float imageResolution5;
	private Float imageSliceThickness;
	private NdaImageOrientation imageOrientation;
	private NdaQcOutcome qcOutcome;
	private String qcDescription = "";
	private String qcFailQuestReason = "";
	private String decayCorrection = "";
	private String frameEndTimes = "";
	private String frameEndUnit = "";
	private String frameStartTimes = "";
	private String frameStartUnit = "";
	private String petIsotope = "";
	private String petTracer = "";
	private Integer timeDiffInjectToImage;
	private String timeDiffUnits = "";
	private String pulseSeq = "";
	private Integer sliceAcquisition;
	private String softwarePreproc = "";
	private String study = "";
	private Float week;
	private String experimentDescription = "";
	private String visit = "";
	private String sliceTiming = "";
	private NdaYesNo bvecBvalFiles;
	private File bvecFile;
	private File bvalFile;
	private String deviceSerialNumber;
	private Date dataProcessedDate;
	private Float visnum;
	
	private void clearOutSomeValues() {
	
		imageNumDimensions = null;
		imageExtent1 = null;
		imageExtent2 = null;
		imageExtent3 = null;
		imageExtent4 = null;
		extent4Type = null;
		imageExtent5 = null;
		extent5Type = null;
		imageUnit1 = null;
		imageUnit2 = null;
		imageUnit3 = null;
		imageUnit4 = null;
		imageUnit5 = null;
		imageResolution1 = null;
		imageResolution2 = null;
		imageResolution3 = null;
		imageResolution4 = null;
		imageResolution5 = null;
		imageSliceThickness = null;
		imageOrientation = null;
		
	}

	private final String NOT_EXTRACTED = "NotExtracted";
	
	public Image03PackageDefinitionFillinMissing() {
		super();
		dataType = NdaManifestType.IMAGE_03;
		try {
			_niftiContext = JAXBContext.newInstance(NiftiImageHdr.class);
			_niftiUnmarshaller = _niftiContext.createUnmarshaller();
		} catch (JAXBException e) {
			log.warn("WARNING:  Could not instantiate NiftiImage JAXBContext",e);
		}
	}
	
	@Override
	public List<String> configurationYaml() {
		
		final List<String> returnList = super.configurationYaml();
		final StringBuilder sb = new StringBuilder();
		sb.append("PathToFslhd:\n")
		.append("    id: pathToFslhd\n")
		.append("    name: pathToFslhd\n")
		.append("    kind: panel.input.text\n")
		.append("    label: Path to fslhd\n")
		.append("    size: 50\n")
		.append("    description: Fslhd is used to pull NIFTI headers from which some values are set.\n");
		sb.append("SeriesDescriptions:\n")
		.append("    id: seriesDescriptions\n")
		.append("    name: seriesDescriptions\n")
		.append("    kind: panel.select.multiple\n")
		.append("    label: Series Descriptions\n")
		.append("    description: Series Description(s) of the scan image to be used for this record.\n")
		.append("    validation: required\n");
		sb.append("CommentsMisc:\n")
		.append("    id: commentsMisc\n")
		.append("    name: commentsMisc\n")
		.append("    kind: panel.textarea\n")
		.append("    label: Comments Misc:\n")
		.append("    description: NOTE:  You may use the following replacement values for this field to insert values from the scan:  $SCANID, $SCANTYPE, $SERIESDESC\n")
		.append("ImageDescription:\n")
		.append("    id: imageDescription\n")
		.append("    name: imageDescription\n")
		.append("    kind: panel.input.text\n")
		.append("    description: If blank, the series description will be used here.\n")
		.append("    size: 50\n")
		.append("    label: Image Description\n");
		sb.append("ExperimentIdMap:\n")
		.append("    id: experimentIdMap\n")
		.append("    name: experimentIdMap\n")
		.append("    label: ExperimentId Map:\n")
		.append("    kind: panel.textarea\n")
		.append("    description: Enter 1 row for each series description that will be sent (format=[SeriesDescription]:[experimentId])\n");
		sb.append("ScanType:\n")
		.append("    id: scanType\n")
		.append("    name: scanType\n")
		.append("    kind: panel.select.single\n")
		.append("    label: Scan Type:\n")
		.append("    size: 50\n")
		.append("    description: If blank, will attempt to assign based on series description\n")
		.append("    options: \n");
		sb.append("        \"\": \"\"\n");
		for (final NdaScanType value : NdaScanType.values()) {
			final ValueInfo valueInfo = value.getValueInfo();
			sb.append("        \"");
			sb.append(valueInfo.getValue());
			sb.append("\": \"");
			sb.append(valueInfo.getDisplayName());
			sb.append("\"\n");
		}
		sb.append("ScanObject:\n")
		.append("    id: scanObject\n")
		.append("    name: scanObject\n")
		.append("    kind: panel.select.single\n")
		.append("    label: Scan Object:\n")
		.append("    validation: required\n")
		.append("    value: LIVE\n")
		.append("    options: \n");
		for (final NdaScanObject value : NdaScanObject.values()) {
			final ValueInfo valueInfo = value.getValueInfo();
			sb.append("        \"");
			sb.append(valueInfo.getValue());
			sb.append("\": \"");
			sb.append(valueInfo.getDisplayName());
			sb.append("\"\n");
		}
		sb.append("ImageFileFormat:\n")
		.append("    id: imageFileFormat\n")
		.append("    name: imageFileFormat\n")
		.append("    kind: panel.select.single\n")
		.append("    label: Image File Format:\n")
		.append("    validation: required\n")
		.append("    value: NIFTI\n")
		.append("    options: \n");
		for (final NdaImageFileFormat value : NdaImageFileFormat.values()) {
			final ValueInfo valueInfo = value.getValueInfo();
			sb.append("        \"");
			sb.append(valueInfo.getValue());
			sb.append("\": \"");
			sb.append(valueInfo.getDisplayName());
			sb.append("\"\n");
		}
		sb.append("ImageModality:\n")
		.append("    id: imageModality\n")
		.append("    name: imageModality\n")
		.append("    kind: panel.select.single\n")
		.append("    label: Image Modality:\n")
		.append("    validation: required\n")
		.append("    value: MRI\n")
		.append("    options: \n");
		for (final NdaModality value : NdaModality.values()) {
			final ValueInfo valueInfo = value.getValueInfo();
			sb.append("        \"");
			sb.append(valueInfo.getValue());
			sb.append("\": \"");
			sb.append(valueInfo.getDisplayName());
			sb.append("\"\n");
		}
		sb.append("TransformationPerformed:\n")
		.append("    id: transformationPerformed\n")
		.append("    name: transformationPerformed\n")
		.append("    kind: panel.select.single\n")
		.append("    label: Transformation Performed:\n")
		.append("    value: NO\n")
		.append("    options: \n");
		for (final NdaYesNo value : NdaYesNo.values()) {
			final ValueInfo valueInfo = value.getValueInfo();
			sb.append("        \"");
			sb.append(valueInfo.getValue());
			sb.append("\": \"");
			sb.append(valueInfo.getDisplayName());
			sb.append("\"\n");
		}
		sb.append("TransformationType:\n")
		.append("    id: transformationType\n")
		.append("    name: transformationType\n")
		.append("    kind: panel.input.text\n")
		.append("    size: 50\n")
		.append("    label: Transformation Type:\n");
		sb.append("FacemaskingSeriesDescriptions:\n")
		.append("    id: facemaskingSeriesDescriptions\n")
		.append("    name: facemaskingSeriesDescriptions\n")
		.append("    kind: panel.select.multiple\n")
		.append("    label: Facemasking Series Descriptions\n")
		.append("    description: Series Description(s) of scan(s) that are facemasked.  Will populate TransformationPerformed field.\n");
		sb.append("FacemaskingTransformationType:\n")
		.append("    id: facemaskingTransformationType\n")
		.append("    name: facemaskingTransformationType\n")
		.append("    kind: panel.input.text\n")
		.append("    size: 50\n")
		.append("    label: Facemasking Transformation Type:\n")
		.append("    value: Image FaceMasked\n")
		.append("    description: Will be appended to TransformationType field if scan is facemasked\n");
		sb.append("ImageHistory:\n")
		.append("    id: imageHistory\n")
		.append("    name: imageHistory\n")
		.append("    kind: panel.textarea\n")
		.append("    label: Image History:\n");
		sb.append("QcOutcome:\n")
		.append("    id: qcOutcome\n")
		.append("    name: qcOutcome\n")
		.append("    kind: panel.select.single\n")
		.append("    label: QC Outcome:\n")
		//.append("    value: PASS\n")
		.append("    options: \n")
		.append("        \"\": \"\"\n");
		for (final NdaQcOutcome value : NdaQcOutcome.values()) {
			final ValueInfo valueInfo = value.getValueInfo();
			sb.append("        \"");
			sb.append(valueInfo.getValue());
			sb.append("\": \"");
			sb.append(valueInfo.getDisplayName());
			sb.append("\"\n");
		}
		sb.append("QcDescription:\n")
		.append("    id: qcDescription\n")
		.append("    name: qcDescription\n")
		.append("    kind: panel.input.text\n")
		.append("    size: 50\n")
		.append("    label: QC Description:\n");
		sb.append("QcFailQuestReason:\n")
		.append("    id: qcFailQuestReason\n")
		.append("    name: qcFailQuestReason\n")
		.append("    kind: panel.input.text\n")
		.append("    size: 50\n")
		.append("    label: QC Failure Reason:\n");
		sb.append("DecayCorrection:\n")
		.append("    id: decayCorrection\n")
		.append("    name: decayCorrection\n")
		.append("    kind: panel.input.text\n")
		.append("    label: Decay Correction:\n");
		sb.append("PetIsotope:\n")
		.append("    id: petIsotope\n")
		.append("    name: petIsotope\n")
		.append("    kind: panel.input.text\n")
		.append("    size: 50\n")
		.append("    label: PET Isotope:\n");
		sb.append("PetTracer:\n")
		.append("    id: petTracer\n")
		.append("    name: petTracer\n")
		.append("    kind: panel.input.text\n")
		.append("    size: 50\n")
		.append("    label: PET Tracer:\n");
		sb.append("SoftwarePreproc:\n")
		.append("    id: softwarePreproc\n")
		.append("    name: softwarePreproc\n")
		.append("    kind: panel.input.text\n")
		.append("    size: 50\n")
		.append("    label: Software Preproc:\n");
		sb.append("Study:\n")
		.append("    id: study\n")
		.append("    name: study\n")
		.append("    kind: panel.input.text\n")
		.append("    label: Study:\n");
		sb.append("ExperimentDescription:\n")
		.append("    id: experimentDesc\n")
		.append("    name: experimentDesc\n")
		.append("    kind: panel.textarea\n")
		.append("    label: Experiment Description:\n");
		sb.append("Visit:\n")
		.append("    id: visit\n")
		.append("    name: visit\n")
		.append("    kind: panel.input.text\n")
		.append("    label: Visit Name:\n")
		.append("    description: If blank, will attempt to pull visit name from the experiment name.\n");
		sb.append("Visnum:\n")
		.append("    id: visnum\n")
		.append("    name: visnum\n")
		.append("    kind: panel.input.text\n")
		.append("    validation: integer\n")
		.append("    label: Visit Number:\n");
		sb.append("InlineScript:\n")
		.append("    tag: script\n")
		.append("    content: CCF.ndatransferconfig.populateSeriesDescriptionOptions();\n");
		returnList.add(sb.toString());
		return returnList;
	}
	
	@Override
	public String generateMetadataFileHeader() {
		return "image,03,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,," + System.lineSeparator() +
		  "subjectkey,src_subject_id,interview_date,interview_age,sex,comments_misc,image_file,image_thumbnail_file,image_description,experiment_id," +
		  "scan_type,scan_object,image_file_format,data_file2,data_file2_type,image_modality,scanner_manufacturer_pd,scanner_type_pd," +
		  "scanner_software_versions_pd,magnetic_field_strength,mri_repetition_time_pd,mri_echo_time_pd,flip_angle,acquisition_matrix,mri_field_of_view_pd," +
		  "patient_position,photomet_interpret,receive_coil,transmit_coil,transformation_performed,transformation_type,image_history,image_num_dimensions," +
		  "image_extent1,image_extent2,image_extent3,image_extent4,extent4_type,image_extent5,extent5_type,image_unit1,image_unit2,image_unit3,image_unit4," +
		  "image_unit5,image_resolution1,image_resolution2,image_resolution3,image_resolution4,image_resolution5,image_slice_thickness,image_orientation," +
		  "qc_outcome,qc_description,qc_fail_quest_reason,decay_correction,frame_end_times,frame_end_unit,frame_start_times,frame_start_unit,pet_isotope," +
		  "pet_tracer,time_diff_inject_to_image,time_diff_units,pulse_seq,slice_acquisition,software_preproc,study,week,experiment_description,visit," +
		  "slice_timing,bvek_bval_files,bvecfile,bvalfile,deviceserialnumber,procdate,visnum,manifest" + System.lineSeparator()
		;
	}
	
	@Override
	public List<String> generateMetadataFileRecords(PcpStatusEntity entity, File manifestDir, File fileDir,
			ProcessingType processingType, Map<String, String> parameters, UserI user) {
		final XnatImagesessiondata session = getSessionWithRetry(entity.getEntityId(), user, false);
		if (session == null) {
			log.error("ERROR:  Couldn't retrieve session:  {}", entity.getEntityLabel());
			return ImmutableList.of();
		}
		boolean useSeriesDescForImgDesc = getImageDescription() == null || getImageDescription().trim().length()<1;
		boolean setScanTypeFromSeriesDesc = getScanType() == null || getScanType().toString().trim().length()<1;
		transformationPerformedOrig = transformationPerformed;
		try {
			final List<String> returnList = new ArrayList<>();
			for (final String seriesDesc : getSeriesDescriptions()) {
				for (final XnatImagescandataI scanI : session.getScans_scan()) {
					if (!(scanI instanceof XnatImagescandata)) {
						continue;
					}
					XnatImagescandata scan = (XnatImagescandata) scanI;
					if (scan.getSeriesDescription().equals(seriesDesc)) {
						final File manifestFile = this.generateManifestFileAndPopulateFileDirectory(entity, session, scan, manifestDir,
								fileDir, processingType, parameters, user);
						populateImage03Fields(session, scan, useSeriesDescForImgDesc, setScanTypeFromSeriesDesc, user);
						returnList.add(buildMetadataRecord(entity, session, manifestFile, seriesDesc));
						break;
					}
				}
			}
			return returnList;
		} catch (IOException | SubmissionException e) {
			log.error("Exception thrown populating manifest or generating metadata record:  ",e);
			return ImmutableList.of();
		}
	}
	
	private void initExperimentMapIfNecessary() {
		if (experimentIdMapObj.isEmpty() && experimentIdMap != null && experimentIdMap.contains(":")) {
			String[] lines = experimentIdMap.split(System.lineSeparator());
			for (final String line : Arrays.asList(lines)) {
				if (!line.contains(":")) {
					continue;
				}
				final String seriesDesc = line.substring(0,line.lastIndexOf(":"));
				final String str2 = line.substring(line.indexOf(":")+1);
				Integer expId;
				try {
					expId = Integer.valueOf(str2);
					experimentIdMapObj.put(seriesDesc,expId);
				} catch (Exception e) {
					continue;
				}
			}
		}
	}
	
	private synchronized void populateImage03Fields(XnatImagesessiondata session, XnatImagescandata scan, boolean useSeriesDescForImgDesc, boolean setScanTypeFromSeriesDesc, UserI user) {
		if (!(session instanceof XnatMrsessiondata)) {
			log.error("ERROR:  Currently only MR sessions are supported for image03 data transfers");
			return;
		} 
		clearOutSomeValues();
		final XnatMrscandata mrScan = (scan instanceof XnatMrscandata) ? (XnatMrscandata)scan : null;
		final List<XnatAddfieldI> addParams = (mrScan != null) ? mrScan.getParameters_addparam() : new ArrayList<XnatAddfieldI>(); 
		@SuppressWarnings("rawtypes")
		final Map bidsSidecarMap =(mrScan != null) ? getBidsSidecarMap(mrScan, user) : new HashMap<>();
		initExperimentMapIfNecessary();
		// comments_misc
		String commentsMiscDisp = getCommentsMisc();
		commentsMiscDisp = commentsMiscDisp.replaceAll("$SCANID", scan.getId()).replaceAll("$SCANTYPE", scan.getType())
				.replaceAll("$SERIESDESC", scan.getSeriesDescription());
		setCommentsMisc(commentsMiscDisp);
		// image_description
		final String seriesDesc = scan.getSeriesDescription();
		if (useSeriesDescForImgDesc) {
			setImageDescription(seriesDesc);
		}
		// experiment_id
		if (experimentIdMapObj.containsKey(seriesDesc)) {
			this.experimentId = experimentIdMapObj.get(seriesDesc);
		} else {
			this.experimentId = null;
		}
		// scan_type
		if (setScanTypeFromSeriesDesc) {
			final String ucSD = seriesDesc.toUpperCase();
			if (ucSD.contains("FMRI")) {
				this.setScanType(NdaScanType.FMRI);
			} else if (ucSD.contains("BOLD")) {
				this.setScanType(NdaScanType.FMRI);
			} else if (ucSD.contains("DMRI") || (ucSD.contains("DWI_"))) {
				this.setScanType(NdaScanType.MR_DIFFUSION);
			} else if (ucSD.contains("T1W") && !ucSD.contains("SETTER")) {
				this.setScanType(NdaScanType.MR_STRUCTURAL_T1);
			} else if (ucSD.contains("T1_MPR") && !ucSD.contains("SETTER")) {
				this.setScanType(NdaScanType.MR_STRUCTURAL_T1);
			} else if (ucSD.contains("T2W") && !ucSD.contains("SETTER")) {
				this.setScanType(NdaScanType.MR_STRUCTURAL_T2);
			} else if (ucSD.contains("T2_MPR") && !ucSD.contains("SETTER")) {
				this.setScanType(NdaScanType.MR_STRUCTURAL_T2);
			} else if (ucSD.contains("PCASL")) {
				this.setScanType(NdaScanType.PCASL_ASL);
			} else if (ucSD.contains("TSE_HI") || ucSD.contains("TSE_NORM") || ucSD.contains("_180FLIP")) {
				this.setScanType(NdaScanType.MR_STRUCTURAL_TSE);
			} else if (ucSD.contains("FIELDMAP")) {
				this.setScanType(NdaScanType.FIELD_MAP);
			} else {
				log.error("ERROR:  Couldn't set scan type from series description (UpperCaseSeriesDesc=" + ucSD + ").");
				this.setScanType(null);
			}
		}
		// bvec_bval_files
		if (getScanType() != null && getScanType().equals(NdaScanType.MR_DIFFUSION)) {
			setBvecBvalFiles(NdaYesNo.YES);
		} else {
			setBvecBvalFiles(null);
		}
		// scanner_manufacturer_pd
		String scannerManufacturerPd = scan.getScanner_manufacturer();
		if (scannerManufacturerPd == null || scannerManufacturerPd.trim().isEmpty()) {
			scannerManufacturerPd = (bidsSidecarMap != null && bidsSidecarMap.containsKey("Manufacturer")) ?
				bidsSidecarMap.get("Manufacturer").toString() : "NotExtracted";
		}
		setScannerManufacturerPd(scannerManufacturerPd);
		// scanner_type_pd
		String scannerTypePd = scan.getScanner_model();
		if (scannerTypePd == null || scannerTypePd.trim().isEmpty()) {
			scannerTypePd = (bidsSidecarMap != null && bidsSidecarMap.containsKey("ManufacturersModelName")) ?
				bidsSidecarMap.get("ManufacturersModelName").toString() : "NotExtracted";
		}
		setScannerTypePd(scannerTypePd);
		// scanner_software_versions_pd
		String scannerSoftwareVersionsPd = scan.getScanner_softwareversion();
		if (scannerSoftwareVersionsPd == null || scannerSoftwareVersionsPd.trim().isEmpty()) {
			scannerSoftwareVersionsPd = (bidsSidecarMap != null && bidsSidecarMap.containsKey("ConversionSoftware") && 
				bidsSidecarMap.containsKey("ConversionSoftwareVersion")) ?
				String.format("%s %s",bidsSidecarMap.get("ConversionSoftware"),bidsSidecarMap.get("ConversionSoftwareVersion")) : "NotExtracted";
		}
		setScannerSoftwareVersionsPd(scannerSoftwareVersionsPd);
		// magnetic_field_strength
		setMagneticFieldStrength((mrScan != null && mrScan.getFieldstrength() != null) ? mrScan.getFieldstrength() : "");
		// mri_repetition_time_pd
		setMriRepetitionTimePd((mrScan != null && mrScan.getParameters_tr() != null) ? millisecondsToSeconds(mrScan.getParameters_tr().floatValue()) : null);
		// mri_echo_time_pd
		setMriEchoTimePd(getEchoTimingFromSidecarFiles(((mrScan != null && mrScan.getSeriesDescription() != null && mrScan.getSeriesDescription().contains("_RMS")) ? getNonRmsScan(session, mrScan) : mrScan), user));
		// flip_angle
		String flipAngle = 
				((mrScan != null && mrScan.getParameters_diffusion_refocusflipangle()!=null &&
						!mrScan.getType().toUpperCase().contains("T1W")) ? "[" : "") +  
				((mrScan != null && mrScan.getParameters_flip() != null) ? mrScan.getParameters_flip().toString() + 
				((mrScan.getParameters_diffusion_refocusflipangle()!=null && !mrScan.getType().toUpperCase().contains("T1W")) ? 
						", " + mrScan.getParameters_diffusion_refocusflipangle().toString().replaceFirst("[.]0$", "") + "]" : "") : ""
							);
		if (flipAngle == null || flipAngle.trim().isEmpty()) {
			// In case dicom extraction hasn't run
			if (bidsSidecarMap != null && bidsSidecarMap.containsKey("FlipAngle")) {
				flipAngle = bidsSidecarMap.get("FlipAngle").toString();
			}
		}
		setFlipAngle(flipAngle);
		// NOTE:  We'd have to get this from the DICOM
		setAcquisitionMatrix(NOT_EXTRACTED);
		// mri_field_of_view_pd
		// Per Mike Harms, set this to NotExtracted.  The one in the scan is not what people would think of as the FOV
		setMriFieldOfViewPd(NOT_EXTRACTED);
		
		//setMriFieldOfViewPd((mrScan != null && mrScan.getParameters_fov_x() != null && mrScan.getParameters_fov_y() != null) ? 
		//		mrScan.getParameters_fov_x() + " x " + mrScan.getParameters_fov_y() : "");
		// patient_position
		String patientPosition =(mrScan != null && mrScan.getParameters_subjectposition() != null) ? mrScan.getParameters_subjectposition() : "";
		if (patientPosition == null || patientPosition.trim().isEmpty()) {
			patientPosition = (bidsSidecarMap != null && bidsSidecarMap.containsKey("PatientPosition")) ? 
				bidsSidecarMap.get("PatientPosition").toString(): "NotExtracted";
		}
		setPatientPosition(patientPosition);
		if (getScanType().getDisplayName().equals("fMRI")) {
				if (mrScan.getParameters_subjectposition()==null || mrScan.getParameters_subjectposition().trim().isEmpty()) {
					setPatientPosition("NotExtracted");
				}
		} 
		// photomet_interpret
		setPhotometInterpret(NOT_EXTRACTED);
		// receive_coil
		String receiveCoil = (mrScan != null && mrScan.getCoil() != null) ? mrScan.getCoil() : "";
		if (receiveCoil.isEmpty()) {
			// Dicom not setting in scan for some projects/scanners.  Get from bids sidecar if necessary
			if (bidsSidecarMap != null && bidsSidecarMap.containsKey("ReceiveCoilName")) {
				receiveCoil = bidsSidecarMap.get("ReceiveCoilName").toString();
			}
		}
		setReceiveCoil(receiveCoil);
		// transmit_coil
		setTransmitCoil("");
		// transformation_performed
		final NdaYesNo wasTransformed =  
		 ((transformationPerformedOrig != null && transformationPerformedOrig.equals(NdaYesNo.YES)) || 
				facemaskingSeriesDescriptions != null && facemaskingSeriesDescriptions.contains(scan.getSeriesDescription())) ?
						NdaYesNo.YES : NdaYesNo.NO;
		setTransformationPerformed(wasTransformed);
		// transformation_type
		final StringBuilder transTypeSb = (transformationType != null) ?  new StringBuilder(transformationType) : new  StringBuilder("");
		if (facemaskingSeriesDescriptions != null && facemaskingSeriesDescriptions.contains(scan.getSeriesDescription()) && 
				!transTypeSb.toString().contains(facemaskingTransformationType)) {
			if (transTypeSb.toString().length()>0) {
				transTypeSb.append(", ");
			}
			transTypeSb.append(facemaskingTransformationType);
		}
		if (getTransformationPerformed().equals(NdaYesNo.YES)) {
			setTransformationType(transTypeSb.toString());
		} else {
			setTransformationType("");
		}
		// For the next set, we need to pull values from NIFTI files.
		NiftiHeader niftiHdr = null;
		try {
			niftiHdr = getNiftiHeader(mrScan, user);
		} catch (InterruptedException e) {
			// Do nothing
		}
		if (niftiHdr != null) {
			// image_num_dimensions 
			try {
				if (niftiHdr.dim!=null && niftiHdr.dim.length>0) {
					log.debug("NIFTI header dimensions found (SCAN={})",mrScan.getSeriesDescription());
					NdaImageUnit xyzUnits = xyzUnitsTransform(niftiHdr.xyzt_units);
					Integer nDim = Integer.valueOf(niftiHdr.dim[0]);
					//
					setImageNumDimensions(nDim);
					//
					setImageExtent1(Integer.valueOf(niftiHdr.dim[1]));
					if (xyzUnits != null) {
						setImageUnit1(xyzUnitsTransform(niftiHdr.xyzt_units));
					}
					setImageResolution1(niftiHdr.pixdim[1]);
					if (nDim>=2) {
						setImageExtent2(Integer.valueOf(niftiHdr.dim[2]));
						if (xyzUnits != null) {
							setImageUnit2(xyzUnitsTransform(niftiHdr.xyzt_units));
						}
						setImageResolution2(niftiHdr.pixdim[2]);
					}
					if (nDim>=3) {
						setImageExtent3(Integer.valueOf(niftiHdr.dim[3]));
						if (xyzUnits != null) {
							setImageUnit3(xyzUnitsTransform(niftiHdr.xyzt_units));
						}
						setImageResolution3(niftiHdr.pixdim[3]);
					}
					if (nDim>=4) {
						setImageExtent4(Integer.valueOf(niftiHdr.dim[4]));
						setExtent4Type("Time");
						if (xyzUnits != null) {
							if (!scan.getType().toUpperCase().contains("DMRI")) {
								setImageUnit4(NdaImageUnit.FRAME_NUMBER);
							} else {
								setImageUnit4(NdaImageUnit.DIFFUSION_GRADIENT);
							}
						}
						setImageResolution4(niftiHdr.pixdim[4]);
					}
				}
				
			} catch (Exception e) {
				log.error("ERROR:  Exception thrown setting NIFTI header dimensions (SCAN={}): {}",mrScan.getSeriesDescription(), e.toString());
				log.debug(ExceptionUtils.getFullStackTrace(e));
			}
		} else {
			log.error("ERROR:  Failed to return NIFTI header information for (SESSION={}, SCAN={})",session.getLabel(),mrScan.getSeriesDescription());
		}
		// For the next set, we need to pull values from NIFTI files.
		// image_slice_thickness
		String sliceThicknessStr = getAddParam(addParams,"Siemens sSliceArray.asSlice[0].dThickness");
		if (sliceThicknessStr == null || sliceThicknessStr.isEmpty()) {
			// Dicom not setting in scan for some projects/scanners.  Get from bids sidecar if necessary
			if (bidsSidecarMap != null && bidsSidecarMap.containsKey("SliceThickness")) {
				sliceThicknessStr = bidsSidecarMap.get("SliceThickness").toString();
			}
		}
		setImageSliceThickness((sliceThicknessStr != null && sliceThicknessStr.length()>0) ? NumberUtils.createFloat(sliceThicknessStr) : null);
		// image_orientation
		final String oStr = (mrScan != null) ? mrScan.getParameters_orientation().toUpperCase() : "";
		NdaImageOrientation orientation = null;
		if (oStr.contains("TRA") || oStr.contains("AX")) {
			orientation = NdaImageOrientation.AXIAL;
		} else if (oStr.contains("SAG")) {
			orientation = NdaImageOrientation.SAGITTAL;
		} else if (oStr.contains("COR")) {
			orientation = NdaImageOrientation.CORONAL;
		}
		setImageOrientation(orientation);
		// qc_outcome
		//setQcOutcome(NdaQcOutcome.PASS);
		// qc_description
		setQcDescription("");
		// qc_fail_quest_reason
		setQcFailQuestReason("");
		// decay_correction
		setDecayCorrection("");
		// frame_end_times
		setFrameEndTimes("");
		// frame_end_units
		setFrameEndUnit("");
		// frame_start_times
		setFrameStartTimes("");
		// frame_start_units
		setFrameStartUnit("");
		// pet_isotope
		setPetIsotope("");
		// pet_tracer
		setPetTracer("");
		// time_diff_inject_to_image
		setTimeDiffInjectToImage(null);
		// time_diff_units
		setTimeDiffUnits("");
		// pulse_sequence
		setPulseSeq((bidsSidecarMap != null && bidsSidecarMap.containsKey("PulseSequenceDetails")) ? bidsSidecarMap.get("PulseSequenceDetails").toString() : "");
		// slice_acquisition
		String sliceTimingArrayStr = null;
		if (bidsSidecarMap != null) {
			if (bidsSidecarMap.containsKey("SliceTiming")) {
				sliceTimingArrayStr = bidsSidecarMap.get("SliceTiming").toString();
				//// Per Mike Harms, 2019/09/11, this wouldn't be accurate.  We're leaving it blank.
				//setSliceAcquisition(computeSliceAcquisitionValue(sliceTimingArrayStr));
			}
		}
		// software_preproc
		setSoftwarePreproc((bidsSidecarMap != null && bidsSidecarMap.containsKey("ConversionSoftware") && 
				bidsSidecarMap.containsKey("ConversionSoftwareVersion")) ?
				String.format("%s %s",bidsSidecarMap.get("ConversionSoftware"),bidsSidecarMap.get("ConversionSoftwareVersion")) : ""
		);
		// slice_timing

		if (getScanType().getDisplayName().equals("fMRI")) {
			setSliceTiming((sliceTimingArrayStr != null) ? sliceTimingArrayStr : "[ ]");
		} else {
			setSliceTiming((sliceTimingArrayStr != null) ? sliceTimingArrayStr : "");
		}


		// deviceserialnumber
		setDeviceSerialNumber(getAddParam(addParams, "Device Serial Number"));
	}

	private Float millisecondsToSeconds(Float val) {
		if (val != null) {
			return val/1000;
		}
		return null;
	}

	@SuppressWarnings("unused")
	private Integer computeSliceAcquisitionValue(String sliceTimingArrayStr) {
		try {
			if (sliceTimingArrayStr == null || sliceTimingArrayStr.length()<1) {
				return null;
			}
			final List<Float> sliceTiming = _gson.fromJson(sliceTimingArrayStr, _floatType);
			if (sliceTiming.size()<3) {
				return null;
			}
			// If all values are equal, then return null
			float prevFloat = sliceTiming.get(0);
			boolean allEqual = true;
			for (Float t : sliceTiming) {
				if (t != prevFloat) {
					allEqual = false;
					break;
				}
			}
			if (allEqual) {
				return null;
			}
			float val0 = sliceTiming.get(0);
			float val1 = sliceTiming.get(1);
			float val2 = sliceTiming.get(2);
			if (val0<=val1 && val1<=val2) {
				// Ascending
				return 1;
			} else if (val0>=val1 && val1>=val2) {
				// Descending
				return 2;
			} else if (val0<=val1 && val1>=val2) {
				// interleaved-even first
				return 3;
			} else if (val0>=val1 && val1<=val2) {
				// interleaved-odd first
				return 4;
			}
			return null;
		} catch (Exception e) {
			return null;
		}
	}

	private String getAddParam(List<XnatAddfieldI> addParams, String fieldName) {
		for (XnatAddfieldI addField : addParams) {
			if (addField.getName().equals(fieldName)) {
				return addField.getAddfield();
			}
		}
		return "";
	}
	
	private NdaImageUnit xyzUnitsTransform(Byte xyz_units_name) {
		if (xyz_units_name == null) {
			return NdaImageUnit.UNKNOWN;
		}
		switch(xyz_units_name) {
			case((byte)10):
				return NdaImageUnit.MILLIMETERS;
		}
		return NdaImageUnit.UNKNOWN;
	}

	@SuppressWarnings("rawtypes")
	private Map getBidsSidecarMap(final XnatMrscandata scan, final UserI user) {
		for (final XnatAbstractresourceI resource : scan.getFile()) {
			if (!(resource instanceof XnatResourcecatalog)) {
				continue;
			}
			final String resLabel = resource.getLabel();
			if (!resLabel.equals(CommonConstants.NIFTI_RESOURCE)) {
				continue;
			}
			final XnatResourcecatalog resourceCat = (XnatResourcecatalog)resource;
			final ArrayList<ResourceFile> fileResources = resourceCat.getFileResources(CommonConstants.ROOT_PATH);
			for (ResourceFile fileResource : fileResources) {
				if (fileResource.getAbsolutePath().endsWith(".json")) {
					try {
						final String fileStr = FileUtils.readFileToString(new File(fileResource.getAbsolutePath()));
						return _gson.fromJson(fileStr, Map.class);
					} catch (IOException e) {
						log.debug(e.toString());
					}
				}
			}
		}
		return null;
	}

	private XnatMrscandata getNonRmsScan(final XnatImagesessiondata session, final XnatMrscandata mrScan) {
		final String targetDesc = mrScan.getSeriesDescription().replace("_RMS","");
		for (final XnatImagescandataI scan : session.getScans_scan()) {
			if (scan.getSeriesDescription().equals(targetDesc) && scan instanceof XnatMrscandata) {
				return (XnatMrscandata)scan;
			}
		}
		return mrScan;
	}
	
	@SuppressWarnings("rawtypes")
	private String getEchoTimingFromSidecarFiles(final XnatMrscandata scan, final UserI user) {
		if (scan == null) {
			return null;
		}
		final ArrayList<String> timingList = new ArrayList<>();
		for (final XnatAbstractresourceI resource : scan.getFile()) {
			if (!(resource instanceof XnatResourcecatalog)) {
				continue;
			}
			final String resLabel = resource.getLabel();
			if (!resLabel.equals("NIFTI")) {
				continue;
			}
			final XnatResourcecatalog resourceCat = (XnatResourcecatalog)resource;
			final ArrayList<ResourceFile> fileResources = resourceCat.getFileResources(CommonConstants.ROOT_PATH);
			Collections.sort(fileResources, new Comparator<ResourceFile>() {
				public int compare(ResourceFile r1, ResourceFile r2) {
					return r1.getAbsolutePath().compareTo(r2.getAbsolutePath());
				}
			});
			for (final ResourceFile fileResource : fileResources) {
				if (fileResource.getAbsolutePath().endsWith(".json")) {
					try {
						final String fileStr = FileUtils.readFileToString(new File(fileResource.getAbsolutePath()));
						 Map sidecarMap = _gson.fromJson(fileStr, Map.class);
						 if (sidecarMap.containsKey(ECHO_TIME)) {
							 timingList.add(sidecarMap.get(ECHO_TIME).toString());
						 }
					} catch (IOException e) {
						log.debug(e.toString());
					}
				}
			}
		}
		if (timingList.isEmpty()) {
			return "NA";
		} else if (timingList.size()==1) {
			return timingList.get(0);
		} else {
			final StringBuilder sb = new StringBuilder("[ ");
			for (String timing : timingList) {
				if (sb.length()>2) {
					sb.append(", ");
				}
				sb.append(timing);
			}
			sb.append(" ]");
			return sb.toString();
		}
	}
	
	private XnatImagesessiondata getSessionWithRetry(String entityId, UserI user, boolean b) {
		XnatImagesessiondata session;
		// Very occasionally, system issues interfere with this call.  Let's try multiple times with sleep.
		for (int i = 0; i < 10; i++) {
			try {
				session = XnatImagesessiondata.getXnatImagesessiondatasById(entityId, user, false);
				if (session != null) return session;
				TimeUnit.SECONDS.sleep(30);
			} catch (Throwable t) {
				try {
					TimeUnit.SECONDS.sleep(30);
				} catch (InterruptedException e) {
					// Do nothing
				}
			}
		}
		return null;
	}

	private NiftiHeader getNiftiHeader(XnatMrscandata scan, UserI user) throws InterruptedException {
		NiftiHeader header = null;
		// Very occasionally, system issues interfere with this call.  Let's try multiple times with sleep
		for (int i = 0; i < 10; i++) {
			try {
				header = readNiftiHeader(scan,user);
				if (header != null) return header;
				TimeUnit.SECONDS.sleep(30);
			} catch (Throwable t) {
				TimeUnit.SECONDS.sleep(30);
			}
		}
		return null;
	}

	private NiftiHeader readNiftiHeader(XnatMrscandata scan, UserI user) {
		// TODO:  This needs to be written to use available JAVA tools (e.g. PlexiViewer or nifti_io) rather 
		// than shelling out to FSL.  See the looping to catch failures.
		if (scan == null) {
			return null;
		}
		if (pathToFslhd != null && pathToFslhd.contains("/fslhd")) {
			for (final XnatAbstractresourceI resource : scan.getFile()) {
				if (!(resource instanceof XnatResourcecatalog)) {
					continue;
				}
				final String resLabel = resource.getLabel();
				if (!resLabel.equals("NIFTI")) {
					continue;
				}
				final XnatResourcecatalog resourceCat = (XnatResourcecatalog)resource;
				final ArrayList<ResourceFile> fileResources = resourceCat.getFileResources(CommonConstants.ROOT_PATH);
				for (ResourceFile fileResource : fileResources) {
					final String filePath = fileResource.getAbsolutePath();
					if ((fileResource.getAbsolutePath().endsWith(".nii.gz") || filePath.endsWith(".nii")) &&
							!filePath.toUpperCase().contains("INITIALFRAMES")) {
						return CcfNiftiUtils.getNiftiHeader(filePath);
					}
				}
			}
		}
		return null;
	}

	@Override
	protected String buildMetadataRecord(PcpStatusEntity entity, XnatImagesessiondata session, File manifestFile, Object recordInfo) {
		final StringBuilder metaSb = new StringBuilder();
		final String comma = ",";
		//final String empty = "";
		final XnatSubjectdata subj = session.getSubjectData();
		// subjectKey
		metaSb.append(_guidUtils.getGuidForSubject(subj));
		metaSb.append(comma);
		// src_subject_id
		metaSb.append(_srcSubjectIdUtils.getSrcSubjectId(subj));
		metaSb.append(comma);
		// interview_date
		metaSb.append(_dateUtils.getInterviewDate(session));
		metaSb.append(comma);
		// interview_age
		metaSb.append(_ageUtils.getInterviewAge(session));
		metaSb.append(comma);
		// gender
		metaSb.append(_genderUtils.getInterviewGender(subj));
		metaSb.append(comma);
		// comments_misc
		safeAppend(metaSb, getCommentsMisc());
		metaSb.append(comma);
		// image_file
		metaSb.append(comma);
		// image_thumbnail_fille
		metaSb.append(comma);
		// image_description
		safeAppend(metaSb, getImageDescription());
		metaSb.append(comma);
		// experiment_id
		safeAppend(metaSb, (getExperimentId()!=null) ? getExperimentId().toString() : "");
		metaSb.append(comma);
		//  scan_type
		safeAppend(metaSb, (getScanType()!=null) ? getScanType().getDisplayName() : "");
		metaSb.append(comma);
		//  scan_object
		safeAppend(metaSb, (getScanObject()!=null) ? getScanObject().getDisplayName() : "");
		metaSb.append(comma);
		//  image_file_format 
		safeAppend(metaSb, (getImageFileFormat()!=null) ? getImageFileFormat().getDisplayName() : "");
		metaSb.append(comma);
		// data_file2
		metaSb.append(comma);
		// data_file2_type
		safeAppend(metaSb, getDataFile2Type());
		metaSb.append(comma);
		//  image_modality
		safeAppend(metaSb, (getImageModality()!=null) ? getImageModality().getDisplayName() : "");
		metaSb.append(comma);
		// scanner_manufacturer_pd
		safeAppend(metaSb, getScannerManufacturerPd());
		metaSb.append(comma);
		// scanner_type_pd
		safeAppend(metaSb, getScannerTypePd());
		metaSb.append(comma);
		// scanner_software_versions_pd
		safeAppend(metaSb, getScannerSoftwareVersionsPd());
		metaSb.append(comma);
		// magnetic_field_strength
		safeAppend(metaSb, getMagneticFieldStrength());
		metaSb.append(comma);
		// mri_repetition_time_pd
		safeAppend(metaSb, (getMriRepetitionTimePd() != null) ? getMriRepetitionTimePd().toString() : "");
		metaSb.append(comma);
		// mri_echo_time_pd
		safeAppend(metaSb, getMriEchoTimePd());
		metaSb.append(comma);
		// flip_angle
		safeAppend(metaSb, getFlipAngle());
		metaSb.append(comma);
		// acquisition_matrix
		safeAppend(metaSb, getAcquisitionMatrix());
		metaSb.append(comma);
		// mri_field_of_view_pd
		safeAppend(metaSb, getMriFieldOfViewPd());
		metaSb.append(comma);
		// patient_position
		safeAppend(metaSb, getPatientPosition());
		metaSb.append(comma);
		// photomet_interpret
		safeAppend(metaSb, getPhotometInterpret());
		metaSb.append(comma);
		// receive_coil
		safeAppend(metaSb, getReceiveCoil());
		metaSb.append(comma);
		// transmit_coil
		safeAppend(metaSb, getTransmitCoil());
		metaSb.append(comma);
		// transformation_performed
		safeAppend(metaSb, (getTransformationPerformed() != null) ? getTransformationPerformed().getDisplayName() : "");
		metaSb.append(comma);
		// transformation_type
		safeAppend(metaSb, getTransformationType());
		metaSb.append(comma);
		// image_history
		safeAppend(metaSb, getImageHistory());
		metaSb.append(comma);
		// image_num_dimensions
		safeAppend(metaSb, (getImageNumDimensions() != null) ? getImageNumDimensions().toString() : "");
		metaSb.append(comma);
		// image_extent1
		safeAppend(metaSb, (getImageExtent1() != null) ? getImageExtent1().toString() : "");
		metaSb.append(comma);
		// image_extent2
		safeAppend(metaSb, (getImageExtent2() != null) ? getImageExtent2().toString() : "");
		metaSb.append(comma);
		// image_extent3
		safeAppend(metaSb, (getImageExtent3() != null) ? getImageExtent3().toString() : "");
		metaSb.append(comma);
		// image_extent4
		safeAppend(metaSb, (getImageExtent4() != null) ? getImageExtent4().toString() : "");
		metaSb.append(comma);
		// extent4_type
		safeAppend(metaSb, (getExtent4Type() != null) ? getExtent4Type().toString() : "");
		metaSb.append(comma);
		// image_extent5
		safeAppend(metaSb, (getImageExtent5() != null) ? getImageExtent5().toString() : "");
		metaSb.append(comma);
		// extent5_type
		safeAppend(metaSb, (getExtent5Type() != null) ? getExtent5Type().toString() : "");
		metaSb.append(comma);
		// image_unit1
		safeAppend(metaSb, (getImageUnit1() != null) ? getImageUnit1().getDisplayName() : "");
		metaSb.append(comma);
		// image_unit2
		safeAppend(metaSb, (getImageUnit2() != null) ? getImageUnit2().getDisplayName() : "");
		metaSb.append(comma);
		// image_unit3
		safeAppend(metaSb, (getImageUnit3() != null) ? getImageUnit3().getDisplayName() : "");
		metaSb.append(comma);
		// image_unit4
		safeAppend(metaSb, (getImageUnit4() != null) ? getImageUnit4().getDisplayName() : "");
		metaSb.append(comma);
		// image_unit5
		safeAppend(metaSb, (getImageUnit5() != null) ? getImageUnit5().getDisplayName() : "");
		metaSb.append(comma);
		// image_resolution1
		safeAppend(metaSb, (getImageResolution1() != null) ? getImageResolution1().toString() : "");
		metaSb.append(comma);
		// image_resolution2
		safeAppend(metaSb, (getImageResolution2() != null) ? getImageResolution2().toString() : "");
		metaSb.append(comma);
		// image_resolution3
		safeAppend(metaSb, (getImageResolution3() != null) ? getImageResolution3().toString() : "");
		metaSb.append(comma);
		// image_resolution4
		safeAppend(metaSb, (getImageResolution4() != null) ? getImageResolution4().toString() : "");
		metaSb.append(comma);
		// image_resolution5
		safeAppend(metaSb, (getImageResolution5() != null) ? getImageResolution5().toString() : "");
		metaSb.append(comma);
		// image_slice_thickness
		safeAppend(metaSb, (getImageSliceThickness() != null) ? getImageSliceThickness().toString() : "");
		metaSb.append(comma);
		// image_orientation
		safeAppend(metaSb, (getImageOrientation() != null) ? getImageOrientation().getDisplayName() : "");
		metaSb.append(comma);
		// qc_outcome
		safeAppend(metaSb, (getQcOutcome() != null) ? getQcOutcome().getDisplayName() : "");
		metaSb.append(comma);
		// qc_description
		safeAppend(metaSb, getQcDescription());
		metaSb.append(comma);
		// qc_fail_quest_reason
		safeAppend(metaSb, getQcFailQuestReason());
		metaSb.append(comma);
		// decay_correction
		safeAppend(metaSb, getDecayCorrection());
		metaSb.append(comma);
		// frame_end_times
		safeAppend(metaSb, getFrameEndTimes());
		metaSb.append(comma);
		// frame_end_unit
		safeAppend(metaSb, getFrameEndUnit());
		metaSb.append(comma);
		// frame_start_times
		safeAppend(metaSb, getFrameStartTimes());
		metaSb.append(comma);
		// frame_start_unit
		safeAppend(metaSb, getFrameStartUnit());
		metaSb.append(comma);
		// pet_isotope
		safeAppend(metaSb, getPetIsotope());
		metaSb.append(comma);
		// pet_tracer
		safeAppend(metaSb, getPetTracer());
		metaSb.append(comma);
		// time_diff_inject_to_image
		safeAppend(metaSb, (getTimeDiffInjectToImage() != null) ? getTimeDiffInjectToImage().toString() : "");
		metaSb.append(comma);
		// time_diff_units
		safeAppend(metaSb, getTimeDiffUnits());
		metaSb.append(comma);
		// pulse_seq
		safeAppend(metaSb, getPulseSeq());
		metaSb.append(comma);
		// slice_acquisition
		safeAppend(metaSb, (getSliceAcquisition() != null) ? getSliceAcquisition().toString() : "");
		metaSb.append(comma);
		// software_preproc
		safeAppend(metaSb, (getSoftwarePreproc() != null) ? getSoftwarePreproc().toString() : "");
		metaSb.append(comma);
		// study
		safeAppend(metaSb, getStudy());
		metaSb.append(comma);
		// week
		final String visitName = computeVisitName(subj, session);
		safeAppend(metaSb, (getWeek() != null && getWeek().toString().trim().length()>0) ? 
				getWeek().toString() : (visitName.equals("V1")) ? "0" : "");
		metaSb.append(comma);
		// experiment_description
		safeAppend(metaSb, getExperimentDescription());
		metaSb.append(comma);
		// visit
		safeAppend(metaSb, visitName);
		metaSb.append(comma);
		// slice_timing
		safeAppend(metaSb, getSliceTiming());
		metaSb.append(comma);
		// bvec_bval_files
		safeAppend(metaSb, (getBvecBvalFiles() != null) ? getBvecBvalFiles().getDisplayName() : "");
		metaSb.append(comma);
		// bvec_file
		metaSb.append(comma);
		// bval_file
		metaSb.append(comma);
		// device_serial_number
		safeAppend(metaSb, getDeviceSerialNumber());
		metaSb.append(comma);
		// data_processed_date
		safeAppend(metaSb, (getDataProcessedDate() != null) ? getDataProcessedDate().toString() : "");
		metaSb.append(comma);
		// visnum
		safeAppend(metaSb, (getVisnum() != null) ? getVisnum().toString() : "");
		metaSb.append(comma);
		// manifest
		safeAppend(metaSb, manifestFile.getName());
		//metaSb.append(comma);
		metaSb.append(System.lineSeparator());
		return metaSb.toString();
	}

	private String computeVisitName(XnatSubjectdata subj, XnatImagesessiondata session) {
		String visit = getVisit();
		if (visit!=null && visit.trim().length()>0) {
			return visit;
		}
		visit = session.getLabel().replace(subj.getLabel() + "_", "").replace("_MR","");
		return visit;
	}
	
}
