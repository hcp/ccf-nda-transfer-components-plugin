package org.nrg.ccf.ndatransfer.components.entities;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

/*
 * NOTE:  This class is for mapping the results from fslhd.  We are no longer using it.  We're now using Plexiviewer-based
 * classes to retrieve the NIFTI header rather than shelling out to fslhd for it.
 */

@XmlRootElement(name = "nifti_image")
@XmlAccessorType(XmlAccessType.PUBLIC_MEMBER)
public class NiftiImageHdr {
	
	private String nifti_type;
	private String header_filename;
	private String image_filename;
	private String image_offset;
	private String ndim;
	private String nx;
	private String ny;
	private String nz;
	private String nt;
	private String dx;
	private String dy;
	private String dz;
	private String dt;
	private String datatype;
	private String datatype_name;
	private String nvox;
	private String nbyper;
	private String byteorder;
	private String scl_slope;
	private String scl_inter;
	private String xyz_units;
	private String xyz_units_name;
	private String time_units;
	private String time_units_name;
	private String descrip;
	private String aux_file;
	private String qform_code;
	private String qform_code_name;
	private String qto_xyz_matrix;
	private String qto_ijk_matrix;
	private String quatern_b;
	private String quatern_c;
	private String quatern_d;
	private String qoffset_x;
	private String qoffset_y;
	private String qoffset_z;
	private String qfac;
	private String qform_i_orientation;
	private String qform_j_orientation;
	private String qform_k_orientation;
	private String sform_code;
	private String sform_code_name;
	private String sto_xyz_matrix;
	private String sto_ijk;
	private String sform_i_orientation;
	private String sform_j_orientation;
	private String sform_k_orientation;
	private String num_ext;
	
	@XmlAttribute(name = "nifti_type")
	public String getNifti_type() {
		return nifti_type;
	}
	
	public void setNifti_type(String nifti_type) {
		this.nifti_type = nifti_type;
	}
	
	@XmlAttribute(name = "header_filename")
	public String getHeader_filename() {
		return header_filename;
	}
	
	public void setHeader_filename(String header_filename) {
		this.header_filename = header_filename;
	}
	
	@XmlAttribute(name = "image_filename")
	public String getImage_filename() {
		return image_filename;
	}
	
	public void setImage_filename(String image_filename) {
		this.image_filename = image_filename;
	}
	
	@XmlAttribute(name = "image_offset")
	public String getImage_offset() {
		return image_offset;
	}
	
	public void setImage_offset(String image_offset) {
		this.image_offset = image_offset;
	}
	
	@XmlAttribute(name = "ndim")
	public String getNdim() {
		return ndim;
	}
	
	public void setNdim(String ndim) {
		this.ndim = ndim;
	}
	
	@XmlAttribute(name = "nx")
	public String getNx() {
		return nx;
	}
	
	public void setNx(String nx) {
		this.nx = nx;
	}
	
	@XmlAttribute(name = "ny")
	public String getNy() {
		return ny;
	}
	
	public void setNy(String ny) {
		this.ny = ny;
	}
	
	@XmlAttribute(name = "nz")
	public String getNz() {
		return nz;
	}
	
	public void setNz(String nz) {
		this.nz = nz;
	}
	
	@XmlAttribute(name = "nt")
	public String getNt() {
		return nt;
	}
	
	public void setNt(String nt) {
		this.nt = nt;
	}
	
	@XmlAttribute(name = "dx")
	public String getDx() {
		return dx;
	}
	
	public void setDx(String dx) {
		this.dx = dx;
	}
	
	@XmlAttribute(name = "dy")
	public String getDy() {
		return dy;
	}
	
	public void setDy(String dy) {
		this.dy = dy;
	}
	
	@XmlAttribute(name = "dz")
	public String getDz() {
		return dz;
	}
	
	public void setDz(String dz) {
		this.dz = dz;
	}
	
	@XmlAttribute(name = "dt")
	public String getDt() {
		return dt;
	}
	
	public void setDt(String dt) {
		this.dt = dt;
	}
	
	@XmlAttribute(name = "datatype")
	public String getDatatype() {
		return datatype;
	}
	
	public void setDatatype(String datatype) {
		this.datatype = datatype;
	}
	
	@XmlAttribute(name = "datatype_name")
	public String getDatatype_name() {
		return datatype_name;
	}
	
	public void setDatatype_name(String datatype_name) {
		this.datatype_name = datatype_name;
	}
	
	@XmlAttribute(name = "nvox")
	public String getNvox() {
		return nvox;
	}
	
	public void setNvox(String nvox) {
		this.nvox = nvox;
	}
	
	@XmlAttribute(name = "nbyper")
	public String getNbyper() {
		return nbyper;
	}
	
	public void setNbyper(String nbyper) {
		this.nbyper = nbyper;
	}
	
	@XmlAttribute(name = "byteorder")
	public String getByteorder() {
		return byteorder;
	}
	
	public void setByteorder(String byteorder) {
		this.byteorder = byteorder;
	}
	
	@XmlAttribute(name = "scl_slope")
	public String getScl_slope() {
		return scl_slope;
	}
	
	public void setScl_slope(String scl_slope) {
		this.scl_slope = scl_slope;
	}
	
	@XmlAttribute(name = "scl_inter")
	public String getScl_inter() {
		return scl_inter;
	}
	
	public void setScl_inter(String scl_inter) {
		this.scl_inter = scl_inter;
	}
	
	@XmlAttribute(name = "xyz_units")
	public String getXyz_units() {
		return xyz_units;
	}
	
	public void setXyz_units(String xyz_units) {
		this.xyz_units = xyz_units;
	}
	
	@XmlAttribute(name = "xyz_units_name")
	public String getXyz_units_name() {
		if (xyz_units_name == null && xyz_units != null) {
			return unitsNameFromUnits(xyz_units);
		}
		return xyz_units_name;
	}

	public void setXyz_units_name(String xyz_units_name) {
		this.xyz_units_name = xyz_units_name;
	}
	
	@XmlAttribute(name = "time_units")
	public String getTime_units() {
		return time_units;
	}
	
	public void setTime_units(String time_units) {
		this.time_units = time_units;
	}
	
	@XmlAttribute(name = "time_units_name")
	public String getTime_units_name() {
		if (time_units_name == null && time_units != null) {
			return unitsNameFromUnits(time_units);
		}
		return time_units_name;
	}
	
	public void setTime_units_name(String time_units_name) {
		this.time_units_name = time_units_name;
	}
	
	@XmlAttribute(name = "descrip")
	public String getDescrip() {
		return descrip;
	}
	
	public void setDescrip(String descrip) {
		this.descrip = descrip;
	}
	
	@XmlAttribute(name = "aux_file")
	public String getAux_file() {
		return aux_file;
	}
	
	public void setAux_file(String aux_file) {
		this.aux_file = aux_file;
	}
	
	@XmlAttribute(name = "qform_code")
	public String getQform_code() {
		return qform_code;
	}
	
	public void setQform_code(String qform_code) {
		this.qform_code = qform_code;
	}
	
	@XmlAttribute(name = "qform_code_name")
	public String getQform_code_name() {
		return qform_code_name;
	}
	
	public void setQform_code_name(String qform_code_name) {
		this.qform_code_name = qform_code_name;
	}
	
	@XmlAttribute(name = "qto_xyz_matrix")
	public String getQto_xyz_matrix() {
		return qto_xyz_matrix;
	}
	
	public void setQto_xyz_matrix(String qto_xyz_matrix) {
		this.qto_xyz_matrix = qto_xyz_matrix;
	}
	
	@XmlAttribute(name = "qto_ijk_matrix")
	public String getQto_ijk_matrix() {
		return qto_ijk_matrix;
	}
	
	public void setQto_ijk_matrix(String qto_ijk_matrix) {
		this.qto_ijk_matrix = qto_ijk_matrix;
	}
	
	@XmlAttribute(name = "quatern_b")
	public String getQuatern_b() {
		return quatern_b;
	}
	
	public void setQuatern_b(String quatern_b) {
		this.quatern_b = quatern_b;
	}
	
	@XmlAttribute(name = "quatern_c")
	public String getQuatern_c() {
		return quatern_c;
	}
	
	public void setQuatern_c(String quatern_c) {
		this.quatern_c = quatern_c;
	}
	
	@XmlAttribute(name = "quatern_d")
	public String getQuatern_d() {
		return quatern_d;
	}
	
	public void setQuatern_d(String quatern_d) {
		this.quatern_d = quatern_d;
	}
	
	@XmlAttribute(name = "qoffset_x")
	public String getQoffset_x() {
		return qoffset_x;
	}
	
	public void setQoffset_x(String qoffset_x) {
		this.qoffset_x = qoffset_x;
	}
	
	@XmlAttribute(name = "qoffset_y")
	public String getQoffset_y() {
		return qoffset_y;
	}
	
	public void setQoffset_y(String qoffset_y) {
		this.qoffset_y = qoffset_y;
	}
	
	@XmlAttribute(name = "qoffset_z")
	public String getQoffset_z() {
		return qoffset_z;
	}
	
	public void setQoffset_z(String qoffset_z) {
		this.qoffset_z = qoffset_z;
	}
	
	@XmlAttribute(name = "qfac")
	public String getQfac() {
		return qfac;
	}
	
	public void setQfac(String qfac) {
		this.qfac = qfac;
	}
	
	@XmlAttribute(name = "qform_i_orientation")
	public String getQform_i_orientation() {
		return qform_i_orientation;
	}
	
	public void setQform_i_orientation(String qform_i_orientation) {
		this.qform_i_orientation = qform_i_orientation;
	}
	
	@XmlAttribute(name = "qform_j_orientation")
	public String getQform_j_orientation() {
		return qform_j_orientation;
	}
	
	public void setQform_j_orientation(String qform_j_orientation) {
		this.qform_j_orientation = qform_j_orientation;
	}
	
	@XmlAttribute(name = "qform_k_orientation")
	public String getQform_k_orientation() {
		return qform_k_orientation;
	}
	
	public void setQform_k_orientation(String qform_k_orientation) {
		this.qform_k_orientation = qform_k_orientation;
	}
	
	@XmlAttribute(name = "sform_code")
	public String getSform_code() {
		return sform_code;
	}
	
	public void setSform_code(String sform_code) {
		this.sform_code = sform_code;
	}
	
	@XmlAttribute(name = "sform_code_name")
	public String getSform_code_name() {
		return sform_code_name;
	}
	
	public void setSform_code_name(String sform_code_name) {
		this.sform_code_name = sform_code_name;
	}
	
	@XmlAttribute(name = "sto_xyz_matrix")
	public String getSto_xyz_matrix() {
		return sto_xyz_matrix;
	}
	
	public void setSto_xyz_matrix(String sto_xyz_matrix) {
		this.sto_xyz_matrix = sto_xyz_matrix;
	}
	
	@XmlAttribute(name = "sto_ijk")
	public String getSto_ijk() {
		return sto_ijk;
	}
	
	public void setSto_ijk(String sto_ijk) {
		this.sto_ijk = sto_ijk;
	}
	
	@XmlAttribute(name = "sform_i_orientation")
	public String getSform_i_orientation() {
		return sform_i_orientation;
	}
	
	public void setSform_i_orientation(String sform_i_orientation) {
		this.sform_i_orientation = sform_i_orientation;
	}
	
	@XmlAttribute(name = "sform_j_orientation")
	public String getSform_j_orientation() {
		return sform_j_orientation;
	}
	
	public void setSform_j_orientation(String sform_j_orientation) {
		this.sform_j_orientation = sform_j_orientation;
	}
	
	@XmlAttribute(name = "sform_k_orientation")
	public String getSform_k_orientation() {
		return sform_k_orientation;
	}
	
	public void setSform_k_orientation(String sform_k_orientation) {
		this.sform_k_orientation = sform_k_orientation;
	}
	
	@XmlAttribute(name = "num_ext")
	public String getNum_ext() {
		return num_ext;
	}
	
	public void setNum_ext(String num_ext) {
		this.num_ext = num_ext;
	}
	
	// Some versions of FSL (e.g. 6.0.1), don't return this.
	private String unitsNameFromUnits(String units) {
			switch(units) {
			case "0":
				return "";
			case "1":
				return "m";
			case "2":
				return "mm";
			case "3":
				return "um";
			case "8":
				return "s";
			case "16":
				return "ms";
			case "24":
				return "us";
			case "32":
				return "Hz";
			case "40":
				return "ppm";
			case "48":
				return "rad/s";
			}
			return null;
	}
	
}
