package org.nrg.ccf.ndatransfer.components.pcp;

import java.io.InvalidClassException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.nrg.ccf.ndatransfer.abst.DataPackageDefinition;
import org.nrg.ccf.ndatransfer.abst.NdaPackagePrereqCheckerI;
import org.nrg.ccf.ndatransfer.utils.NdaTransferConfigUtils;
import org.nrg.ccf.pcp.anno.PipelinePrereqChecker;
import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.ccf.pcp.inter.ConfigurableComponentI;
import org.nrg.ccf.pcp.inter.PipelinePrereqCheckerI;
import org.nrg.ccf.pcpcomponents.exception.PcpAutomationScriptExecutionException;
import org.nrg.ccf.pcpcomponents.utils.PcpComponentUtils;
import org.nrg.framework.exceptions.NotFoundException;
import org.nrg.xdat.XDAT;
import org.nrg.xft.security.UserI;

import com.google.common.collect.ImmutableList;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@PipelinePrereqChecker
public class NdaTransferPrereqChecker implements PipelinePrereqCheckerI, ConfigurableComponentI {
	
	final NdaTransferConfigUtils _configUtils = XDAT.getContextService().getBean(NdaTransferConfigUtils.class);
	final Map<String,NdaPackagePrereqCheckerI> _cache = new HashMap<>();
	private final Map<String,List<String>> _configCache = new HashMap<>();
	private List<String> _configYaml = null;
	protected final String _className = this.getClass().getName().replace(".", "");
	
	@Override
	public List<String> getConfigurationYaml() {
		if (_configYaml != null) {
			return _configYaml;
		}
		_configYaml = new ArrayList<>();
		final StringBuilder sb = new StringBuilder();
		sb.append(_className)
		.append("ExcludedEntities:\n")
		.append("    id: " + _className + "-excluded-entities\n")
		.append("    name: " + _className + "-excluded-entities\n")
		.append("    kind: panel.textarea\n")
		.append("    label: Excluded entities:\n")
		.append("    description: List of entities to be exluded from transfers.  These can be listed as [subgroup,subgroup2]:[entity] " + 
				"if entities should only be excluded for certain subgroups.\n");
		_configYaml.add(sb.toString());
		return _configYaml;
	}
	
	protected List<String> getConfiguredExclusions(String project, String pipeline) throws PcpAutomationScriptExecutionException {
		final String configValue = PcpComponentUtils.getConfigValue(project, pipeline, this.getClass().getName(),
				"-excluded-entities", false).toString();
		if (!_configCache.containsKey(project)) {
			final List<String> returnLst = new ArrayList<>();
			final String configArr[] = configValue.split("\\r?\\n");
			for (String configV : configArr) {
				if (configV == null) {
					continue;
				}
				configV = configV.trim();
				if (configV.length()>0 && !returnLst.contains(configV)) {
					returnLst.add(configV);
				}
			}
			_configCache.put(project, returnLst);
		}
		return _configCache.get(project);
	}

	@Override
	public void checkPrereqs(PcpStatusEntity statusEntity, UserI user) {
		try {
			// Let's work with a clone here in case the prereq checker sets any values.
			final DataPackageDefinition dataPackageInfo = 
					_configUtils.getDataPackageInfoClone(statusEntity.getProject(),statusEntity.getSubGroup());
			final NdaPackagePrereqCheckerI checker = getChecker(dataPackageInfo);
			final List<String> exclusions = getExcludedEntities(statusEntity, user);
			exclusions.addAll(getExcludedEntities(statusEntity, dataPackageInfo));
			if (!exclusions.contains(statusEntity.getEntityLabel())) {
				checker.checkPrereqs(statusEntity, user, dataPackageInfo);
			} else {
				final String infoStr = "Entity has been excluded by configuration"; 
				if (statusEntity.getPrereqs() || !statusEntity.getPrereqsInfo().equals(infoStr.toString())) {
					statusEntity.setPrereqs(false);
					statusEntity.setPrereqsTime(new Date());
					statusEntity.setPrereqsInfo(infoStr.toString());
				} 
			}
		} catch (Exception e) {
			log.debug(e.toString());
		}
	}

	private List<String> getExcludedEntities(PcpStatusEntity statusEntity, UserI user) {
		try {
			return populateReturnList(statusEntity, getConfiguredExclusions(statusEntity.getProject(), statusEntity.getPipeline()));
		} catch (PcpAutomationScriptExecutionException e) {
			log.error("Exception thrown retrieving configured exclusions:", e);
			return new ArrayList<>();
		}
	}

	protected List<String> getExcludedEntities(PcpStatusEntity statusEntity, DataPackageDefinition dataPackageInfo) 
			throws NotFoundException, ClassNotFoundException, InstantiationException, IllegalAccessException, InvalidClassException {
		final String excludedEntityStr = dataPackageInfo.getExcludedEntities();
		if (excludedEntityStr == null || excludedEntityStr.length()<1) {
			return ImmutableList.of();
		}
		final String configArr[] = excludedEntityStr.split("\\r?\\n");
		return populateReturnList(statusEntity, Arrays.asList(configArr));
	}

	private List<String> populateReturnList(PcpStatusEntity statusEntity, List<String> exclusions) {
		final List<String> returnLst = new ArrayList<>();
		for (final String configV : exclusions) {
			if (configV == null) {
				continue;
			}
			if (configV.contains(":")) {
				final String valA[] = configV.split(":");
				String subG = valA[0];
				String subV = valA[1];
				if (subG == null || subV == null) {
					continue;
				}
				subG = subG.trim();
				subV = subV.trim();
				for (final String subGG : subG.split(",")) {
					if ((subGG).equals(statusEntity.getSubGroup())) {
						if (!returnLst.contains(subV)) {
							returnLst.add(subV);
						}
					}
				}
			} else {
				if (configV.trim().length()>0 && !returnLst.contains(configV)) {
					returnLst.add(configV);
				}
			}
		}
		return returnLst;
	}

	protected NdaPackagePrereqCheckerI getChecker(DataPackageDefinition dataPackageInfo) 
			throws NotFoundException, ClassNotFoundException, InstantiationException, IllegalAccessException, InvalidClassException {
		if (dataPackageInfo == null) {
			throw new NotFoundException("NdaPackagePrereqChecker not found (DataPackageInfo is null).");
		}
		final String pClass = dataPackageInfo.getPackagePrereqChecker();
		if (_cache.containsKey(pClass)) {
			return _cache.get(pClass);
		}
		if (pClass == null) {
			throw new NotFoundException("NdaPackagePrereqChecker not found (PackagePrereqChecker class value is null).");
		}
		final Object pClassObj = Class.forName(pClass).newInstance();
		if (pClassObj instanceof NdaPackagePrereqCheckerI) {
			final NdaPackagePrereqCheckerI checker = (NdaPackagePrereqCheckerI)pClassObj;
			_cache.put(pClass, checker);
			return checker;
		}
		throw new InvalidClassException("NdaPackagePrereqChecker is not an instance of NdaPackagePrereqCheckerI (class=" +  pClass + ")");
	}

}
