package org.nrg.ccf.ndatransfer.components.entities;

import org.nrg.ccf.ndatransfer.anno.NdaPackageDefinition;
import lombok.Getter;
import lombok.Setter;

//@Slf4j
@Getter
@Setter
@NdaPackageDefinition(description="Standard imagingcollection01 package definition (gender rename)")
public class ImagingCollectionPackageDefinition_sx extends ImagingCollectionPackageDefinition {
	
	@Override
	public String generateMetadataFileHeader() {
		return "imagingcollection,1,,,,,,,,,,,,,,,," + System.lineSeparator() + 
				"subjectKey,src_subject_id,origin_dataset_id,interview_date,interview_age,sex,image_collection_desc,job_name,proc_types,"
				+ "pipeline,pipeline_script,pipeline_tools,pipeline_type,pipeline_version,image_modality,scan_type,image_manifest,experiment_id" 
				+ System.lineSeparator();
	}
	
}
