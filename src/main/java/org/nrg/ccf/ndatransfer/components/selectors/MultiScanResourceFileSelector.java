package org.nrg.ccf.ndatransfer.components.selectors;

import java.util.ArrayList;
import java.util.List;

import org.nrg.ccf.ndatransfer.anno.FileSelector;
import org.nrg.ccf.ndatransfer.constants.MatchOperator;
import org.nrg.ccf.ndatransfer.constants.ResourceType;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.model.XnatImagesessiondataI;
import org.python.google.common.collect.ImmutableList;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@FileSelector
public class MultiScanResourceFileSelector extends ResourceFileSelector {
	
	public static final String MATCH_ALL = "^.*$";
	private String scanTypeRegex = MATCH_ALL;
	private String seriesDescRegex = MATCH_ALL;
	
	@Override
	public List<String> configurationYaml() {
		return ImmutableList.of();
	}
	
	public MultiScanResourceFileSelector(ResourceType resourceType, String matchRegex, String excludeRegex, MatchOperator matchOperator,
			Integer matchCount) {
		super(ResourceType.SCAN, matchRegex, excludeRegex, matchOperator, matchCount);
	}
	
	public MultiScanResourceFileSelector(ResourceType resourceType, String matchRegex, String excludeRegex, MatchOperator matchOperator,
			Integer matchCount, String scanTypeRegex, String seriesDescRegex) {
		super(ResourceType.SCAN, matchRegex, excludeRegex, matchOperator, matchCount);
		this.scanTypeRegex = scanTypeRegex;
		this.seriesDescRegex = seriesDescRegex;
	}
	
	@Override
	protected List<XnatImagescandataI> getRelevantScans(XnatImagesessiondataI exp) {
		
		final List<XnatImagescandataI> initialList = exp.getScans_scan();
		final List<XnatImagescandataI> returnList = new ArrayList<>();
		for (final XnatImagescandataI scan : initialList) {
			if (scanTypeRegex != null && scanTypeRegex.length()>0) {
				if (!scan.getType().matches(scanTypeRegex)) {
					continue;
				}
			}
			if (seriesDescRegex != null && seriesDescRegex.length()>0) {
				if (!scan.getSeriesDescription().matches(seriesDescRegex)) {
					continue;
				}
			}
			returnList.add(scan);
		}
		
		return returnList;
		
	}
	
}
