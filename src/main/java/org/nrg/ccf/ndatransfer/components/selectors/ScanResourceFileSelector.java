package org.nrg.ccf.ndatransfer.components.selectors;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.nrg.ccf.ndatransfer.abst.DataPackageDefinition;
import org.nrg.ccf.ndatransfer.abst.ValueInfo;
import org.nrg.ccf.ndatransfer.anno.FileSelector;
import org.nrg.ccf.ndatransfer.components.abst.AbstractRegexResourceFileSelector;
import org.nrg.ccf.ndatransfer.constants.MatchOperator;
import org.nrg.ccf.ndatransfer.constants.ResourceType;
import org.nrg.ccf.ndatransfer.interfce.FileMatcherI;
import org.nrg.ccf.ndatransfer.interfce.SingleScanFileSelectorI;
import org.nrg.xdat.model.XnatAbstractresourceI;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.model.XnatImagesessiondataI;
import org.nrg.xdat.om.XnatResourcecatalog;
import org.nrg.xft.security.UserI;
import org.python.google.common.collect.ImmutableList;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@FileSelector
public class ScanResourceFileSelector extends AbstractRegexResourceFileSelector implements SingleScanFileSelectorI {
	
	public ScanResourceFileSelector() {
		super();
	}
	
	@Override
	public List<String> configurationYaml() {
		
		final StringBuilder sb = new StringBuilder();
		sb.append("ResourceType:\n")
		.append("    id: resourceType\n")
		.append("    name: resourceType\n")
		.append("    kind: panel.select.single\n")
		.append("    label: Resource Type:\n")
		.append("    validation: required\n")
		.append("    value: " + ResourceType.SCAN.toString() + "\n");
		if (resourceType != null) {
			sb.append("    value: ")
			.append(resourceType)
			.append("\n")
			.append("    element:\n")
			.append("        disabled: true:\n")
			;
		}
		sb.append("    options:\n");
		for (final ResourceType value : ResourceType.values()) {
			final ValueInfo valueInfo = value.getValueInfo();
			if (!valueInfo.getValue().equalsIgnoreCase("SCAN")) {
				continue;
			}
			sb.append("        \"");
			sb.append(valueInfo.getValue());
			sb.append("\": \"");
			sb.append(valueInfo.getDisplayName());
			sb.append("\"\n");
		}
		sb.append(regexConfigurationYaml())
		.append("FileMatchers:\n")
		.append("    id: fileMatchers\n")
		.append("    name: fileMatchers\n")
		.append("    kind: panel.display\n")
		.append("    label: FileMatchers:\n");
		
		return ImmutableList.of(sb.toString());
		
	}
	
	public ScanResourceFileSelector(ResourceType resourceType, String matchRegex, String excludeRegex, MatchOperator matchOperator,
			Integer matchCount) {
		super(ResourceType.SCAN, matchRegex, excludeRegex, matchOperator, matchCount);
	}
	
	@Override
	public Map<String, List<File>> generateFileMap(DataPackageDefinition packageInfo, XnatImagesessiondataI session, XnatImagescandataI scan, UserI user) {
		final Map<String, List<File>> returnMap = new LinkedHashMap<>();
		final List<XnatImagescandataI> scanList = ImmutableList.of(scan); 
		for (final XnatAbstractresourceI resource : findMatchingScanResources(scanList)) {
			if (!(resource instanceof XnatResourcecatalog)) {
				continue;
			}
			for (final FileMatcherI fileMatcher : getFileMatchers()) {
				final Map<String, List<File>> matches = findFileMatches((XnatResourcecatalog)resource,fileMatcher,user);
				for (Entry<String, List<File>> entry : matches.entrySet()) {
					if (!returnMap.containsKey(entry.getKey())) {
						returnMap.put(entry.getKey(), new ArrayList<File>());
					}
					returnMap.get(entry.getKey()).addAll(entry.getValue());
				}
			}
		}
		return returnMap;
	}
	
}
