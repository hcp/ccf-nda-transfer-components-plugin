package org.nrg.ccf.ndatransfer.components.pcp;

import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.nrg.ccf.ndatransfer.constants.PrimaryUsage;
import org.nrg.ccf.ndatransfer.utils.NdaTransferConfigUtils;
import org.nrg.ccf.pcp.anno.PipelineSelector;
import org.nrg.ccf.pcp.constants.PcpConfigConstants;
import org.nrg.ccf.pcp.constants.PcpConstants;
import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.ccf.pcp.inter.PipelineSelectorI;
import org.nrg.ccf.pcp.services.PcpStatusEntityService;
import org.nrg.ccf.pcp.services.PcpStatusUpdateService;
import org.nrg.xdat.XDAT;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xft.search.CriteriaCollection;
import org.nrg.xft.security.UserI;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@PipelineSelector
public class NdaTransferSelector implements PipelineSelectorI {
	
	final protected NdaTransferConfigUtils _configUtils = XDAT.getContextService().getBean(NdaTransferConfigUtils.class);
	final protected RuntimeMXBean _mxBean = ManagementFactory.getRuntimeMXBean();
	final protected PcpStatusUpdateService _statusUpdateService = XDAT.getContextService().getBean(PcpStatusUpdateService.class);
	protected PrimaryUsage _primaryUsage = PrimaryUsage.BOTH;
			
	@Override
	public List<PcpStatusEntity> getStatusEntities(PcpStatusEntityService _statusEntityService, String projectId, String pipelineId, UserI user) {
		
		final List<PcpStatusEntity> returnList = new ArrayList<>();
		final List<PcpStatusEntity> unmatchedEntities;
		try {
			unmatchedEntities = _statusEntityService.getProjectPipelineStatus(projectId, pipelineId);
			Collections.sort(unmatchedEntities);
		} catch (Throwable t) {
			log.error("Exception thrown accessing status entity service", t);
			return null;
		}
        final CriteriaCollection cc = new CriteriaCollection("OR");
        cc.addClause(XnatMrsessiondata.SCHEMA_ELEMENT_NAME + "/project", projectId);
        cc.addClause(XnatMrsessiondata.SCHEMA_ELEMENT_NAME + "/sharing/share/project", projectId);
		final List<XnatMrsessiondata> exps=XnatMrsessiondata.getXnatMrsessiondatasByField(cc, user, false);
		final List<String> groupList = _configUtils.getSubgroupList(projectId, _primaryUsage);
		Collections.sort(groupList);
		for (final XnatMrsessiondata exp : exps) {
			for (final String GROUP : groupList) {
				boolean foundGroupMatch = false;
				final Iterator<PcpStatusEntity> i = unmatchedEntities.iterator();
				while (i.hasNext()) {
					final PcpStatusEntity entity = i.next();
					if (entity.getEntityId().equals(exp.getId()) || (entity.getEntityLabel().equals(exp.getLabel()))) {
						if (entity.getSubGroup().equals(GROUP)) {
							returnList.add(entity);
							i.remove();
							foundGroupMatch = true;
							if (entity.getEntityId().equals(exp.getId())) {
								if (!entity.getEntityLabel().equals(exp.getLabel())) {
									log.warn("WARNING:  Experiment Label has changed for session {} - updating StatusEntityID (OLDLABEL={}, NEWLABEL={}",
											exp.getId(), entity.getEntityLabel(), exp.getLabel());
									entity.setEntityLabel(exp.getLabel());
									_statusEntityService.update(entity);
								}
								if (entity.getStatus().equals(PcpConstants.PcpStatus.REMOVED.toString())) {
									entity.setStatus(PcpConstants.PcpStatus.UNKNOWN.toString());
									entity.setStatusTime(new Date());
									_statusEntityService.update(entity);
								}
							} else {
								log.warn("WARNING:  Experiment ID has changed for session {} - updating StatusEntityID (OLDID={}, NEWID={})",
										exp.getLabel(), entity.getId(), exp.getId());
								entity.setEntityId(exp.getId());
								if (entity.getStatus().equals(PcpConstants.PcpStatus.REMOVED.toString())) {
									entity.setStatus(PcpConstants.PcpStatus.UNKNOWN.toString());
									entity.setStatusTime(new Date());
								}
								_statusEntityService.update(entity);
							}
							break;
						}
					}
				}
				if (foundGroupMatch) {
					continue;
				}
				try {
					final PcpStatusEntity newEntity = new PcpStatusEntity();
					newEntity.setProject(projectId);
					newEntity.setPipeline(pipelineId);
					newEntity.setEntityType(XnatMrsessiondata.SCHEMA_ELEMENT_NAME);
					newEntity.setEntityId(exp.getId());
					newEntity.setEntityLabel(exp.getIdentifier(projectId));
					newEntity.setSubGroup(GROUP);
					newEntity.setStatus(PcpConstants.PcpStatus.NOT_SUBMITTED.toString());
					newEntity.setStatusTime(new Date());
					final PcpStatusEntity createdEntity = _statusEntityService.create(newEntity);
					returnList.add(createdEntity);
					_statusUpdateService.refreshStatusEntityCheckAndValidate(createdEntity, user, false);
				} catch (Exception e) {
					// Could be an overlooked constraint violation or something.  We need to continue and try other entities.
					log.error("ERROR:  Exception thrown adding status entity", e);
				}
			}
		}
		//
		// Update status for sessions that no longer exist.
		//
		// CCF-326:  For some reason, this code was often executing at Tomcat startup.  It seems like this check 
		// may be executing before the XnatMrsessiondatas.getXnatMrsessiondatasByField method is able to 
		// return values.  Let's just skip this code near Tomcat startup to prevent it messing up status when 
		// the run is unable to access the session list.  NOTE:  This fix is probably redundant, because 
		// there's a similar check in PcpStatusUpdate (the scheduled task), but just in case....
		final Long upTime = (_mxBean!=null) ? _mxBean.getUptime() : Long.MAX_VALUE;
		if (upTime>PcpConfigConstants.UP_TIME_WAIT &&  exps.size()>0) {
			for (PcpStatusEntity statusEntity : unmatchedEntities) {
				if (statusEntity.getStatus() != null && statusEntity.getStatus().equals(PcpConstants.PcpStatus.REMOVED.toString())) {
					continue;
				}
				statusEntity.setStatus(PcpConstants.PcpStatus.REMOVED.toString());
				statusEntity.setStatusTime(new Date());
				_statusEntityService.update(statusEntity);
			}
		} else {
			log.debug("Skipping step to set status to REMOVED due to insufficient Tomcat uptime or empty experiment list.  (UPTIME={} ms, UNMATCHED_ENTITIES={})",
					upTime, unmatchedEntities.size());
		}
		return returnList;
	}
		
		
}
