package org.nrg.ccf.ndatransfer.components.pcp;

import java.io.InvalidClassException;
import java.util.HashMap;
import java.util.Map;

import org.nrg.ccf.ndatransfer.abst.DataPackageDefinition;
import org.nrg.ccf.ndatransfer.abst.NdaPackageValidatorI;
import org.nrg.ccf.ndatransfer.utils.NdaTransferConfigUtils;
import org.nrg.ccf.pcp.anno.PipelineValidator;
import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.ccf.pcp.inter.PipelineValidatorI;
import org.nrg.framework.exceptions.NotFoundException;
import org.nrg.xdat.XDAT;
import org.nrg.xft.security.UserI;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@PipelineValidator
public class NdaTransferValidator implements PipelineValidatorI {
	
	final NdaTransferConfigUtils _configUtils = XDAT.getContextService().getBean(NdaTransferConfigUtils.class);
	final static Map<String,NdaPackageValidatorI> _cache = new HashMap<>();

	@Override
	public void validate(PcpStatusEntity statusEntity, UserI user) {
		try {
			// Let's work with a clone here in case the validation method changes the object
			final DataPackageDefinition dataPackageInfo = _configUtils.getDataPackageInfoClone(statusEntity.getProject(),statusEntity.getSubGroup());
			final NdaPackageValidatorI validator = getValidator(dataPackageInfo);
			validator.validate(statusEntity, user, dataPackageInfo);
		} catch (Exception e) {
			log.error(e.toString());
		}
	}
	

	protected static NdaPackageValidatorI getValidator(DataPackageDefinition dataPackageInfo) 
			throws NotFoundException, ClassNotFoundException, InstantiationException, IllegalAccessException, InvalidClassException {
		if (dataPackageInfo == null) {
			throw new NotFoundException("NdaPackageValidator not found (DataPackageInfo is null).");
		}
		final String pClass = dataPackageInfo.getPackageValidator();
		if (_cache.containsKey(pClass)) {
			return _cache.get(pClass);
		}
		if (pClass == null) {
			throw new NotFoundException("NdaPackageValidator not found (PackageValidator class value is null).");
		}
		final Object pClassObj = Class.forName(pClass).newInstance();
		if (pClassObj instanceof NdaPackageValidatorI) {
			final NdaPackageValidatorI validator = (NdaPackageValidatorI)pClassObj;
			_cache.put(pClass, validator);
			return validator;
		}
		throw new InvalidClassException("NdaPackageValidator is not an instance of NdaPackageValidatorI (class=" +  pClass + ")");
	}

}
