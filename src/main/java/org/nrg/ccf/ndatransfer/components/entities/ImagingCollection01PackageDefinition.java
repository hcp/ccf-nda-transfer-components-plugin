package org.nrg.ccf.ndatransfer.components.entities;

import java.io.File;
import java.util.List;

import org.nrg.ccf.common.utilities.constants.CommonConstants;
import org.nrg.ccf.common.utilities.utils.ExperimentUtils;
import org.nrg.ccf.ndatransfer.abst.ManifestPackageDefinition;
import org.nrg.ccf.ndatransfer.abst.ValueInfo;
import org.nrg.ccf.ndatransfer.anno.NdaPackageDefinition;
import org.nrg.ccf.ndatransfer.constants.NdaManifestType;
import org.nrg.ccf.ndatransfer.constants.NdaModality;
import org.nrg.ccf.ndatransfer.constants.NdaQcOutcome;
import org.nrg.ccf.ndatransfer.constants.NdaScanType;
import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.xdat.om.XnatImagesessiondata;
import org.nrg.xdat.om.XnatSubjectdata;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
//import lombok.experimental.Accessors;

@Getter
@Setter
@NdaPackageDefinition(description="Standard imagingcollection01 package definition")
public class ImagingCollection01PackageDefinition extends ManifestPackageDefinition {
	
	@Setter(AccessLevel.NONE)
	@Getter(AccessLevel.NONE)
	private final String _className = this.getClass().getSimpleName();
	
	private String imageCollectionDesc = "";
	private String jobName = "";
	private String procTypes = "";
	private String pipeline = "";
	private String pipelineScript = "";
	private String pipelineTools = "";
	private String pipelineType = "";
	private String pipelineVersion = "";
	private NdaModality modality;
	private NdaScanType mainScanType;
	private Integer experimentId;
	private Integer img03Id;
	private Integer fileSource;
	private Integer img03Id2;
	private Integer fileSource2;
	private String imageCollectionName = "";
	private String qcFailQuestReason = "";
	private NdaQcOutcome qcOutcome;
	
	
	public ImagingCollection01PackageDefinition() {
		super();
		dataType = NdaManifestType.IMAGING_COLLECTION_01;
	}
	
	@Override
	public List<String> configurationYaml() {
		
		final List<String> returnList = super.configurationYaml();
		final StringBuilder sb = new StringBuilder();
		sb.append("ImageCollectionName:\n")
		.append("    id: imageCollectionName\n")
		.append("    name: imageCollectionName\n")
		.append("    kind: panel.input.text\n")
		.append("    validation: required\n")
		.append("    label: Image Collection Name:\n");
		sb.append("ImageCollectionDesc:\n")
		.append("    id: imageCollectionDesc\n")
		.append("    name: imageCollectionDesc\n")
		.append("    kind: panel.textarea\n")
		.append("    validation: required\n")
		.append("    label: Image Collection Description:\n")
		.append("JobName:\n")
		.append("    id: jobName\n")
		.append("    name: jobName\n")
		.append("    kind: panel.input.text\n")
		.append("    size: 50\n")
		.append("    label: Job Name:\n")
		.append("ProcTypes:\n")
		.append("    id: procTypes\n")
		.append("    name: procTypes\n")
		.append("    kind: panel.input.text\n")
		.append("    size: 50\n")
		.append("    label: Proc Types:\n")
		.append("Pipeline:\n")
		.append("    id: pipeline\n")
		.append("    name: pipeline\n")
		.append("    kind: panel.input.text\n")
		.append("    size: 50\n")
		.append("    label: Pipeline:\n")
		.append("PipelineScript:\n")
		.append("    id: pipelineScript\n")
		.append("    name: pipelineScript\n")
		.append("    kind: panel.input.text\n")
		.append("    size: 50\n")
		.append("    label: PipelineScript:\n")
		.append("PipelineTools:\n")
		.append("    id: pipelineTools\n")
		.append("    name: pipelineTools\n")
		.append("    kind: panel.input.text\n")
		.append("    size: 50\n")
		.append("    label: PipelineTools:\n")
		.append("PipelineType:\n")
		.append("    id: pipelineType\n")
		.append("    name: pipelineType\n")
		.append("    kind: panel.input.text\n")
		.append("    size: 50\n")
		.append("    label: PipelineType:\n")
		.append("PipelineVersion:\n")
		.append("    id: pipelineVersion\n")
		.append("    name: pipelineVersion\n")
		.append("    kind: panel.input.text\n")
		.append("    size: 50\n")
		.append("    label: PipelineVersion:\n")
		.append("Modality:\n")
		.append("    id: modality\n")
		.append("    name: modality\n")
		.append("    kind: panel.select.single\n")
		.append("    label: Modality:\n")
		.append("    validation: required\n")
		.append("    options: \n");
		for (final NdaModality value : NdaModality.values()) {
			final ValueInfo valueInfo = value.getValueInfo();
			sb.append("        \"");
			sb.append(valueInfo.getValue());
			sb.append("\": \"");
			sb.append(valueInfo.getDisplayName());
			sb.append("\"\n");
		}
		sb.append("MainScanType:\n")
		.append("    id: mainScanType\n")
		.append("    name: mainScanType\n")
		.append("    kind: panel.select.single\n")
		.append("    label: Main Scan Type:\n")
		.append("    validation: required\n")
		.append("    options: \n");
		for (final NdaScanType value : NdaScanType.values()) {
			final ValueInfo valueInfo = value.getValueInfo();
			sb.append("        \"");
			sb.append(valueInfo.getValue());
			sb.append("\": \"");
			sb.append(valueInfo.getDisplayName());
			sb.append("\"\n");
		}
		sb.append("ExperimentId:\n")
		.append("    id: experimentId\n")
		.append("    name: experimentId\n")
		.append("    kind: panel.input.text\n")
		.append("    label: ExperimentId:\n")
		.append("    validation: integer\n")
		.append("    description: An integer experiment ID for the Experiment/settings/run (required for fMRI)\n");
		sb.append("QcFailQuestReason:\n")
		.append("    id: qcFailQuestReason\n")
		.append("    name: qcFailQuestReason\n")
		.append("    kind: panel.input.text\n")
		.append("    size: 50\n")
		.append("    label: QcFailQuestReason:\n")
		.append("QcOutcome:\n")
		.append("    id: qcOutcome\n")
		.append("    name: qcOutcome\n")
		.append("    kind: panel.select.single\n")
		.append("    label: QcOutcome:\n")
		.append("    value: UNDETERMINED\n")
		.append("    options: \n");
		for (final NdaQcOutcome value : NdaQcOutcome.values()) {
			final ValueInfo valueInfo = value.getValueInfo();
			sb.append("        \"");
			sb.append(valueInfo.getValue());
			sb.append("\": \"");
			sb.append(valueInfo.getDisplayName());
			sb.append("\"\n");
		}
		returnList.add(sb.toString());
		return returnList;
	}
	
	@Override
	public String generateMetadataFileHeader() {
		return "imagingcollection,01,,,,,,,,,,,,,,,,,,," + 
				System.lineSeparator() +
				"subjectkey,src_subject_id,interview_date,interview_age,sex,image_collection_desc,job_name,proc_types,pipeline," + 
				"pipeline_script,pipeline_tools,pipeline_type,pipeline_version,image_modality,scan_type,image_manifest,experiment_id," +
				"img03_id2,file_source2,img03_id,file_source,image_collection_name,qc_outcome,qc_fail_quest_reason" + 
				System.lineSeparator();
	}

	@Override
	protected String buildMetadataRecord(PcpStatusEntity entity, XnatImagesessiondata session, File manifestFile, Object recordInfo) {
		final StringBuilder metaSb = new StringBuilder();
		final String comma = ",";
		//final String empty = "";
		final XnatSubjectdata subj = session.getSubjectData();
		// subjectKey
		metaSb.append(_guidUtils.getGuidForSubject(subj));
		metaSb.append(comma);
		// src_subject_id
		metaSb.append(_srcSubjectIdUtils.getSrcSubjectId(subj));
		metaSb.append(comma);
		// interview_date
		metaSb.append(_dateUtils.getInterviewDate(session));
		metaSb.append(comma);
		// interview_age
		metaSb.append(_ageUtils.getInterviewAge(session));
		metaSb.append(comma);
		// gender
		metaSb.append(_genderUtils.getInterviewGender(subj));
		metaSb.append(comma);
		// description
		safeAppend(metaSb, getImageCollectionDesc());
		metaSb.append(comma);
		// job_name
		safeAppend(metaSb, getJobName());
		metaSb.append(comma);
		// proc_types
		safeAppend(metaSb, getProcTypes());
		metaSb.append(comma);
		// pipeline
		safeAppend(metaSb, getPipeline());
		metaSb.append(comma);
		// pipeline_script
		safeAppend(metaSb, getPipelineScript());
		metaSb.append(comma);
		// pipline_tools
		safeAppend(metaSb, getPipelineTools());
		metaSb.append(comma);
		// pipeline_type
		safeAppend(metaSb, getPipelineType());
		metaSb.append(comma);
		// pipeline_version
		safeAppend(metaSb, getPipelineVersion());
		metaSb.append(comma);
		// modality
		safeAppend(metaSb, getModality().getDisplayName());
		metaSb.append(comma);
		// main_scan_type
		safeAppend(metaSb, getMainScanType().getDisplayName());
		metaSb.append(comma);
		// manifest
		safeAppend(metaSb, manifestFile.getName());
		metaSb.append(comma);
		// experiment_id
		safeAppend(metaSb, (getExperimentId()!=null) ? getExperimentId().toString() : "");
		metaSb.append(comma);
		// img03_id2
		safeAppend(metaSb, (getImg03Id2()!=null) ? getImg03Id2().toString() : "");
		metaSb.append(comma);
		// file_source2
		safeAppend(metaSb, (getFileSource2()!=null) ? getFileSource2().toString() : "");
		metaSb.append(comma);
		// img03_id
		safeAppend(metaSb, (getImg03Id()!=null) ? getImg03Id().toString() : "");
		metaSb.append(comma);
		// file_source
		safeAppend(metaSb, (getFileSource()!=null) ? getFileSource().toString() : "");
		metaSb.append(comma);
		// description
		safeAppend(metaSb, getImageCollectionName());
		metaSb.append(comma);
		// qc_outcome
		safeAppend(metaSb, getQcOutcome(session).getDisplayName());
		metaSb.append(comma);
		// qc_fail_quest_reason
		safeAppend(metaSb, getQcFailQuestReason(session));
		metaSb.append(System.lineSeparator());
		return metaSb.toString();
	}
	
	private String getQcFailQuestReason(XnatImagesessiondata session) {
		if (isRulesOverrideRequired(session)) {
			return "Required release rules override to build release session.";
		}
		return null;
	}

	private NdaQcOutcome getQcOutcome(XnatImagesessiondata session) {
		final NdaQcOutcome defaultOutcome = (getQcOutcome() != null) ? getQcOutcome() : NdaQcOutcome.UNDETERMINED;
		return (isRulesOverrideRequired(session)) ? NdaQcOutcome.QUESTIONABLE : defaultOutcome;
	}

	private boolean isRulesOverrideRequired(XnatImagesessiondata session) {
		final String overrideValue = ExperimentUtils.getFieldValue(session, CommonConstants.RULES_OVERRIDE_REQUIRED_FIELD);
		final String overrideAsWarning = ExperimentUtils.getFieldValue(session, CommonConstants.OVERRIDE_AS_WARNING_FIELD);
		return ((overrideValue != null && overrideValue.toLowerCase().contains("true")) &&
		        !(overrideAsWarning != null && overrideAsWarning.toLowerCase().contains("true")));
	}
	
}
