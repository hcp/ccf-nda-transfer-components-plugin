package org.nrg.ccf.ndatransfer.components.entities;

import java.io.File;
import java.util.List;

import org.nrg.ccf.ndatransfer.abst.ManifestPackageDefinition;
import org.nrg.ccf.ndatransfer.abst.ValueInfo;
import org.nrg.ccf.ndatransfer.anno.NdaPackageDefinition;
import org.nrg.ccf.ndatransfer.constants.NdaManifestType;
import org.nrg.ccf.ndatransfer.constants.NdaQcOutcome;
import org.nrg.ccf.ndatransfer.constants.NdaScanType;
import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.xdat.om.XnatImagesessiondata;
import org.nrg.xdat.om.XnatSubjectdata;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
//import lombok.experimental.Accessors;

@Getter
@Setter
@NdaPackageDefinition(description="Standard fmriresults01 package definition")
public class FmriResultsPackageDefinition extends ManifestPackageDefinition {
	
	@Setter(AccessLevel.NONE)
	@Getter(AccessLevel.NONE)
	private final String _className = this.getClass().getSimpleName();
	
	private Integer originDatasetId;
	private Integer experimentId;
	private String inputs;
	private Integer img03Id;
	private String fileSource = "";
	private String jobName = "";
	private String procTypes = "";
	private File metricFiles;
	private String pipeline = "";
	private String pipelineScript = "";
	private String pipelineTools = "";
	private String pipelineType = "";
	private String pipelineVersion = "";
	private String qcFailQuestReason = "";
	private NdaQcOutcome qcOutcome;
	private File derivedFiles;
	private NdaScanType scanType;
	private Integer img03Id2;
	private String fileSource2 = "";
	private String sessionDetails = "";
	private String imageHistory = "";
	
	public FmriResultsPackageDefinition() {
		super();
		dataType = NdaManifestType.FMRI_RESULTS_01;
	}
	
	@Override
	public List<String> configurationYaml() {
		
		final List<String> returnList = super.configurationYaml();
		final StringBuilder sb = new StringBuilder();
		sb.append("ExperimentId:\n")
		.append("    id: experimentId\n")
		.append("    name: experimentId\n")
		.append("    kind: panel.input.text\n")
		.append("    label: ExperimentId:\n")
		.append("    validation: integer\n")
		.append("    description: An integer experiment ID for the Experiment/settings/run (required for fMRI)\n");
		sb.append("Inputs:\n")
		.append("    id: inputs\n")
		.append("    name: inputs\n")
		.append("    kind: panel.textarea\n")
		.append("    label: Inputs:\n")
		.append("JobName:\n")
		.append("    id: jobName\n")
		.append("    name: jobName\n")
		.append("    kind: panel.input.text\n")
		.append("    size: 50\n")
		.append("    label: Job Name:\n")
		.append("ProcTypes:\n")
		.append("    id: procTypes\n")
		.append("    name: procTypes\n")
		.append("    kind: panel.input.text\n")
		.append("    size: 50\n")
		.append("    label: Proc Types:\n")
		.append("Pipeline:\n")
		.append("    id: pipeline\n")
		.append("    name: pipeline\n")
		.append("    kind: panel.input.text\n")
		.append("    validation: required\n")
		.append("    size: 50\n")
		.append("    label: Pipeline:\n")
		.append("PipelineScript:\n")
		.append("    id: pipelineScript\n")
		.append("    name: pipelineScript\n")
		.append("    kind: panel.input.text\n")
		.append("    validation: required\n")
		.append("    size: 50\n")
		.append("    label: PipelineScript:\n")
		.append("PipelineTools:\n")
		.append("    id: pipelineTools\n")
		.append("    name: pipelineTools\n")
		.append("    kind: panel.input.text\n")
		.append("    validation: required\n")
		.append("    size: 50\n")
		.append("    label: PipelineTools:\n")
		.append("PipelineType:\n")
		.append("    id: pipelineType\n")
		.append("    name: pipelineType\n")
		.append("    kind: panel.input.text\n")
		.append("    validation: required\n")
		.append("    size: 50\n")
		.append("    label: PipelineType:\n")
		.append("PipelineVersion:\n")
		.append("    id: pipelineVersion\n")
		.append("    name: pipelineVersion\n")
		.append("    kind: panel.input.text\n")
		.append("    size: 50\n")
		.append("    validation: required\n")
		.append("    label: PipelineVersion:\n")
		.append("QcFailQuestReason:\n")
		.append("    id: qcFailQuestReason\n")
		.append("    name: qcFailQuestReason\n")
		.append("    kind: panel.input.text\n")
		.append("    validation: required\n")
		.append("    size: 50\n")
		.append("    label: QcFailQuestReason:\n")
		.append("QcOutcome:\n")
		.append("    id: qcOutcome\n")
		.append("    name: qcOutcome\n")
		.append("    kind: panel.select.single\n")
		.append("    label: QcOutcome:\n")
		.append("    validation: required\n")
		.append("    value: PASS\n")
		.append("    options: \n");
		for (final NdaQcOutcome value : NdaQcOutcome.values()) {
			final ValueInfo valueInfo = value.getValueInfo();
			sb.append("        \"");
			sb.append(valueInfo.getValue());
			sb.append("\": \"");
			sb.append(valueInfo.getDisplayName());
			sb.append("\"\n");
		}
		sb.append("ScanType:\n")
		.append("    id: scanType\n")
		.append("    name: scanType\n")
		.append("    kind: panel.select.single\n")
		.append("    label: Scan Type:\n")
		.append("    validation: required\n")
		.append("    options: \n");
		for (final NdaScanType value : NdaScanType.values()) {
			final ValueInfo valueInfo = value.getValueInfo();
			sb.append("        \"");
			sb.append(valueInfo.getValue());
			sb.append("\": \"");
			sb.append(valueInfo.getDisplayName());
			sb.append("\"\n");
		}
		sb.append("SessionDetails:\n")
		.append("    id: sessionDetails\n")
		.append("    name: sessionDetails\n")
		.append("    kind: panel.input.text\n")
		.append("    size: 50\n")
		.append("    label: SessionDetails:\n");
		sb.append("ImageHistory:\n")
		.append("    id: imageHistory\n")
		.append("    name: imageHistory\n")
		.append("    kind: panel.textarea\n")
		.append("    size: 50\n")
		.append("    label: ImageHistory:\n");
		returnList.add(sb.toString());
		return returnList;
	}
	
	@Override
	public String generateMetadataFileHeader() {
		return "fmriresults,01,,,,,,,,,,,,,,,,,,,,,,,," + System.lineSeparator() +
		"subjectkey,src_subject_id,origin_dataset_id,interview_date,interview_age,gender,experiment_id,inputs,img03_id," + 
		"file_source,job_name,proc_types,metric_files,pipeline,pipeline_script,pipeline_tools,pipeline_type,pipeline_version," +
		"qc_fail_quest_reason,qc_outcome,derived_files,scan_type,img03_id2,file_source2,session_det,image_history" + System.lineSeparator()		
		;
	}

	protected String buildMetadataRecord(PcpStatusEntity entity, XnatImagesessiondata session, File manifestFile, Object recordInfo) {
		final StringBuilder metaSb = new StringBuilder();
		final String comma = ",";
		//final String empty = "";
		final XnatSubjectdata subj = session.getSubjectData();
		// subjectKey
		metaSb.append(_guidUtils.getGuidForSubject(subj));
		metaSb.append(comma);
		// src_subject_id
		metaSb.append(subj.getLabel());
		metaSb.append(comma);
		// origin_dataset_id
		metaSb.append((getOriginDatasetId()!=null) ? getOriginDatasetId() : "");
		metaSb.append(comma);
		// interview_date
		metaSb.append(_dateUtils.getInterviewDate(session));
		metaSb.append(comma);
		// interview_age
		metaSb.append(_ageUtils.getInterviewAge(session));
		metaSb.append(comma);
		// gender
		metaSb.append(_genderUtils.getInterviewGender(subj));
		metaSb.append(comma);
		// experiment_id
		safeAppend(metaSb, (getExperimentId()!=null) ? getExperimentId().toString() : "");
		metaSb.append(System.lineSeparator());
		// inputs
		safeAppend(metaSb, getInputs());
		metaSb.append(comma);
		// img03_id
		safeAppend(metaSb, (getImg03Id() != null) ? getImg03Id().toString() : "");
		metaSb.append(comma);
		// file_source
		safeAppend(metaSb, getFileSource());
		metaSb.append(comma);
		// job_name
		safeAppend(metaSb, getJobName());
		metaSb.append(comma);
		// proc_types
		safeAppend(metaSb, getProcTypes());
		metaSb.append(comma);
		// metric_files (not used for now)
		metaSb.append(comma);
		// pipeline
		safeAppend(metaSb, getPipeline());
		metaSb.append(comma);
		// pipeline_script
		safeAppend(metaSb, getPipelineScript());
		metaSb.append(comma);
		// pipline_tools
		safeAppend(metaSb, getPipelineTools());
		metaSb.append(comma);
		// pipeline_type
		safeAppend(metaSb, getPipelineType());
		metaSb.append(comma);
		// pipeline_version
		safeAppend(metaSb, getPipelineVersion());
		metaSb.append(comma);
		// qc_fail_quest_reason
		safeAppend(metaSb, getQcFailQuestReason());
		metaSb.append(comma);
		// qc_outcome
		safeAppend(metaSb, getQcOutcome().getDisplayName());
		metaSb.append(comma);
		// derived_files (not used for now)
		metaSb.append(comma);
		// scan_type
		safeAppend(metaSb, getScanType().getDisplayName());
		metaSb.append(comma);
		// img03_id2
		safeAppend(metaSb, (getImg03Id2() != null) ? getImg03Id2().toString() : "");
		metaSb.append(comma);
		// file_source2
		safeAppend(metaSb, getFileSource2());
		metaSb.append(comma);
		// session_det
		safeAppend(metaSb, getSessionDetails());
		metaSb.append(comma);
		// image_history
		safeAppend(metaSb, getImageHistory());
		//metaSb.append(comma);
		metaSb.append(System.lineSeparator());
		// manifest
		//metaSb.append(manifestFile.getName());
		//metaSb.append(comma);
		return metaSb.toString();
	}
	
	
}
