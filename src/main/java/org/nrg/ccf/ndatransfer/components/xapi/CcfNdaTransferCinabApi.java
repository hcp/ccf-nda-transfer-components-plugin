package org.nrg.ccf.ndatransfer.components.xapi;


import org.nrg.ccf.ndatransfer.components.utils.CinabUtils;
import org.nrg.framework.annotations.XapiRestController;
import org.nrg.framework.exceptions.NrgServiceException;
import org.nrg.xapi.rest.AbstractXapiRestController;
import org.nrg.xapi.rest.ProjectId;
import org.nrg.xapi.rest.XapiRequestMapping;
import org.nrg.xdat.security.helpers.AccessLevel;
import org.nrg.xdat.security.services.RoleHolder;
import org.nrg.xdat.security.services.UserManagementServiceI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
//import lombok.extern.slf4j.Slf4j;

//@Slf4j
@Lazy
@XapiRestController
@Api(description = "Archive CinaB generation API")
public class CcfNdaTransferCinabApi extends AbstractXapiRestController {
	
	private CinabUtils _cinabUtils;

	@Autowired
	@Lazy
	public CcfNdaTransferCinabApi(UserManagementServiceI userManagementService, RoleHolder roleHolder,
			CinabUtils cinabUtils) {
		super(userManagementService, roleHolder);
		_cinabUtils = cinabUtils;
	}
	
	@ApiOperation(value = "Generate CinaB links in archive from CinaB packages", 
			notes = "Generates CinaB links in archive from CinaB packages",
			response = Void.class)
    @ApiResponses({@ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = {"/ccfNdaTransfer/{projectId}/entity/{entityLabel}/generateArchiveCinaB"}, restrictTo=AccessLevel.Member, method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<Void> setConfig(@PathVariable("projectId") @ProjectId final String projectId,
    		@PathVariable("entityLabel") @ProjectId final String entityLabel) throws NrgServiceException {
		
		_cinabUtils.generateCinabForEntity(projectId, entityLabel, getSessionUser());
		return new ResponseEntity<Void>(HttpStatus.OK);
	
    }
	
}
