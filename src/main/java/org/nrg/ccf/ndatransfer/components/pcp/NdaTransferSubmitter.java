package org.nrg.ccf.ndatransfer.components.pcp;

import java.util.List;
import java.util.Map;

import org.nrg.ccf.ndatransfer.abst.ValueInfo;
import org.nrg.ccf.ndatransfer.client.entities.ProjectTransferSettings;
import org.nrg.ccf.ndatransfer.constants.SubmissionGrouping;
import org.nrg.ccf.pcp.anno.PipelineSubmitter;
import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.ccf.pcp.exception.PcpSubmitException;
import org.nrg.ccf.pcp.inter.PipelineSubmitterI;
import org.nrg.ccf.pcp.inter.PipelineValidatorI;
import org.nrg.ccf.pcp.services.PcpStatusEntityService;
import org.nrg.xdat.XDAT;
import org.nrg.xft.security.UserI;

import com.google.common.collect.ImmutableList;

@PipelineSubmitter
public class NdaTransferSubmitter implements PipelineSubmitterI {
	
	final String _className = this.getClass().getSimpleName();
	final ProjectTransferSettings _projectTransferSettings = XDAT.getContextService().getBean(ProjectTransferSettings.class);

	@Override
	public List<String> getParametersYaml(final String projectId, final String pipelineId) {
		StringBuilder sb = new StringBuilder();
		sb.append(_className)
		.append("SubmitBy:\n")
		.append("    id: " + _className + "-submit-by\n")
		.append("    name: " + _className + "-submit-by\n")
		.append("    kind: panel.select.single\n")
		.append("    value: PACKAGE\n")
		.append("    label: 'Batch submissions by:  '\n")
		.append("    description: 'NOTE:  Packages for different data types (e.g. image03 vs fmriresults01) will always be sent separately. ")
			.append("This setting will be used to group submissions within the same data type.'\n")
		.append("    options:\n");
		for (final SubmissionGrouping group : SubmissionGrouping.values()) {
			final ValueInfo valueInfo = group.getValueInfo();
			sb.append("        '");
			sb.append(valueInfo.getValue());
			sb.append("': '");
			sb.append(valueInfo.getDisplayName());
			sb.append("'\n");
		}
		sb.append("numGroups:\n")
		.append("    id: " + _className + "-num-groups\n")
		.append("    name: " + _className + "-num-groups\n")
		.append("    kind: panel.input.text\n")
		.append("    label: 'Number of Groups:  '\n")
		.append("    validation: integer\n")
		.append("    description: (OPTIONAL) Break each <em>submit-by</em> package into equal size submission groups. ")
			.append("NDA submission can sometimes fail for very large submissions.\n");
		sb.append("CollectionId:\n")
		.append("    id: " + _className + "-collection-id\n")
		.append("    name: " + _className + "-collection-id\n")
		.append("    kind: panel.input.text\n")
		.append("    value: " + _projectTransferSettings.getDefaultCollectionId(projectId) +"\n")
		.append("    label: 'NDA Collection ID:  '\n");
		sb.append("ResumeSubmission:\n")
		.append("    id: " + _className + "-resume-submission\n")
		.append("    name: " + _className + "-resume-submission\n")
		.append("    kind: panel.input.switchbox\n")
		.append("    value: false \n")
		.append("    label: 'Resume failed submission:  '\n")
		.append("    description: 'Resume a submission that has failed to complete, rather than doing a new submission.  ")
			.append("This is valid only for submissions that made it to the point of completing CSV metadata generation and ")
			.append("creating a submission record.'\n");
		sb.append("AdditionalFlags:\n")
		.append("    id: " + _className + "-additional-parameters\n")
		.append("    name: " + _className + "-additional-parameters\n")
		.append("    kind: panel.input.text\n")
		.append("    label: 'Additional vtcmd parameters:  '\n")
		.append("    value: '' \n")
		.append("    placeholder: '--skipLocalAssocFileCheck' \n")
		.append("    description: 'Additional parameters to add to vtcmd call'\n");
		sb.append("NumberThreads:\n")
		.append("    id: " + _className + "-num-threads\n")
		.append("    name: " + _className + "-num-threads\n")
		.append("    kind: panel.input.text\n")
		.append("    label: 'Number of concurrent threads:  '\n")
		.append("    value: '4' \n")
		.append("    description: 'Number of concurrent threads to use for metadata processing'\n");
		sb.append("RemoveTemp:\n")
		.append("    id: " + _className + "-remove-tempdir\n")
		.append("    name: " + _className + "-remove-tempdir\n")
		.append("    kind: panel.input.switchbox\n")
		.append("    value: false \n")
		.append("    label: 'Remove TempDir after processing:  '\n")
		.append("    description: '(RECOMMENDED ONLY FOR SMALL SUBMISSIONS AND CINAB GENERATION)  You will not be able to resume failed submissions ")
			.append("if the temp directory has been removed.'\n");
		sb.append("RegenerateCinabDuplicates:\n")
		.append("    id: " + _className + "-regenerate-duplicates\n")
		.append("    name: " + _className + "-regenerate-duplicates\n")
		.append("    kind: panel.input.switchbox\n")
		.append("    value: false \n")
		.append("    label: 'Regenerate symlinks to duplicated files:  '\n")
		.append("    description: '(NORMALLY NOT NECESSARY)  Remove symlinks to files that are duplicated in the symlink structure ")
			.append("so duplicated files are handled according to the latest algorithm.'\n");
		sb.append("ProcessingType:\n")
		.append("    id: " + _className + "-processing-type\n")
		.append("    name: " + _className + "-processing-type\n")
		.append("    kind: panel.select\n")
		.append("    value: VALIDATE_AND_UPLOAD \n")
		.append("    label: 'Processing Type:  '\n")
		.append("    after:\n")
		.append("        script:\n")
		.append("            tag: 'script'\n")
		.append("            content: >\n")
		.append("                 $('#" + _className + "-processing-type').change(function() {\n")
		.append("                 	var processingType = $('#" + _className + "-processing-type').val();\n")
		.append("                 	if (processingType == 'VALIDATE_AND_UPLOAD') {\n")
		.append("                 		$('[data-name=" + _className + "-cinab-directory]').hide();\n")
		.append("                 		$('[data-name=" + _className + "-num-groups]').show();\n")
		.append("                 		$('[data-name=" + _className + "-collection-id]').show();\n")
		.append("                 		$('[data-name=" + _className + "-resume-submission]').show();\n")
		.append("                 		$('[data-name=" + _className + "-additional-parameters]').show();\n")
		.append("                 		$('[data-name=" + _className + "-remove-tempdir]').show();\n")
		.append("                 		$('[data-name=" + _className + "-descriptive-segment]').show();\n")
		.append("                 		$('[data-name=" + _className + "-regenerate-duplicates]').hide();\n")
		.append("                 		$('#" + _className + "-submit-by').val('COMBINED');\n")
		.append("                 	} else if (processingType == 'VALIDATE_ONLY') {\n")
		.append("                 		$('[data-name=" + _className + "-num-groups]').show();\n")
		.append("                 		$('[data-name=" + _className + "-collection-id]').hide();\n")
		.append("                 		$('[data-name=" + _className + "-cinab-directory]').hide();\n")
		.append("                 		$('[data-name=" + _className + "-resume-submission]').hide();\n")
		.append("                 		$('[data-name=" + _className + "-additional-parameters]').hide();\n")
		.append("                 		$('[data-name=" + _className + "-remove-tempdir]').show();\n")
		.append("                 		$('[data-name=" + _className + "-descriptive-segment]').hide();\n")
		.append("                 		$('[data-name=" + _className + "-regenerate-duplicates]').hide();\n")
		.append("                 		$('#" + _className + "-submit-by').val('COMBINED');\n")
		.append("                 	} else if (processingType == 'PACKAGE_SIZE_REPORT') {\n")
		.append("                 		$('[data-name=" + _className + "-num-groups]').hide();\n")
		.append("                 		$('[data-name=" + _className + "-collection-id]').hide();\n")
		.append("                 		$('[data-name=" + _className + "-cinab-directory]').hide();\n")
		.append("                 		$('[data-name=" + _className + "-resume-submission]').hide();\n")
		.append("                 		$('[data-name=" + _className + "-additional-parameters]').hide();\n")
		.append("                 		$('[data-name=" + _className + "-remove-tempdir]').hide();\n")
		.append("                 		$('[data-name=" + _className + "-descriptive-segment]').hide();\n")
		.append("                 		$('[data-name=" + _className + "-regenerate-duplicates]').hide();\n")
		.append("                 		$('#" + _className + "-submit-by').val('COMBINED');\n")
		.append("                 	} else if (processingType == 'CINAB_STRUCTURE') {\n")
		.append("                 		$('[data-name=" + _className + "-cinab-directory]').show();\n")
		.append("                 		$('[data-name=" + _className + "-num-groups]').hide();\n")
		.append("                 		$('[data-name=" + _className + "-collection-id]').hide();\n")
		.append("                 		$('[data-name=" + _className + "-resume-submission]').hide();\n")
		.append("                 		$('[data-name=" + _className + "-additional-parameters]').hide();\n")
		.append("                 		$('[data-name=" + _className + "-remove-tempdir]').hide();\n")
		.append("                 		$('[data-name=" + _className + "-descriptive-segment]').hide();\n")
		.append("                 		$('[data-name=" + _className + "-regenerate-duplicates]').show();\n")
		.append("                 		$('#" + _className + "-submit-by').val('COMBINED');\n")
		.append("                 	} else if (processingType == 'CINAB_ARCHIVE') {\n")
		.append("                 		$('[data-name=" + _className + "-cinab-directory]').hide();\n")
		.append("                 		$('[data-name=" + _className + "-num-groups]').hide();\n")
		.append("                 		$('[data-name=" + _className + "-collection-id]').hide();\n")
		.append("                 		$('[data-name=" + _className + "-resume-submission]').hide();\n")
		.append("                 		$('[data-name=" + _className + "-additional-parameters]').hide();\n")
		.append("                 		$('[data-name=" + _className + "-remove-tempdir]').show();\n")
		.append("                 		$('[data-name=" + _className + "-descriptive-segment]').hide();\n")
		.append("                 		$('[data-name=" + _className + "-regenerate-duplicates]').show();\n")
		.append("                 		$('#" + _className + "-submit-by').val('COMBINED');\n")
		.append("                 	}\n")
		.append("                 });\n") 
		.append("                 $('#" + _className + "-processing-type').val('VALIDATE_ONLY');\n")
		.append("                 $('#" + _className + "-processing-type').change();\n")
		.append("    options:\n")
		.append("        'VALIDATE_AND_UPLOAD': 'Validate And Upload'\n")
		.append("        'VALIDATE_ONLY': 'Validate Only'\n")
		.append("        'PACKAGE_SIZE_REPORT': 'Package Size Report'\n")
		.append("        'CINAB_STRUCTURE': 'Create CinaB Structure'\n")
		.append("        'CINAB_ARCHIVE': 'Create Archive CinaB Structure'\n");
		sb.append("CinabDirectory:\n")
		.append("    id: " + _className + "-cinab-directory\n")
		.append("    name: " + _className + "-cinab-directory\n")
		.append("    kind: panel.input.text\n")
		.append("    label: 'CinaB Directory:  '\n")
		.append("    value: '' \n")
		.append("    description: '(OPTIONAL) Directory for CinaB structure (Create CinaB Structure processing type only)'\n");
		sb.append("DescriptiveSegment:\n")
		.append("    id: " + _className + "-descriptive-segment\n")
		.append("    name: " + _className + "-descriptive-segment\n")
		.append("    kind: panel.input.text\n")
		.append("    label: 'Descriptive Segment:  '\n")
		.append("    value: '' \n")
		.append("    description: '(OPTIONAL) A short descriptive segment (letters and numbers only) to inserted into the NDA submission ")
			.append( "title to provide additional information.'\n");
		return ImmutableList.of(sb.toString());
	}

	@Override
	public boolean submitJob(final PcpStatusEntity entity, final PcpStatusEntityService entityService, final PipelineValidatorI validator,
			final Map<String, String> parameters, final UserI user) throws PcpSubmitException {
		// The exec manager will do the work of submission
		return true;
	}

}
